<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Восстановление пароля</title>
    <style type="text/css">
        @font-face {
            font-family: "formular";
            src: url(/fonts/formular-extralight.otf?107f8b5326f16c367ea26c1c861a6387);
            font-weight: 100;
            font-style: normal;
        }

        @font-face {
            font-family: "formular";
            src: url(/fonts/formular-extralightitalic.otf?bedc3f88ca0f5cdfe6a1e6cee451b436);
            font-weight: 100;
            font-style: italic;
        }

        @font-face {
            font-family: "formular";
            src: url(/fonts/formular-light.otf?1033fad155a595b4fdb7408781e3ecd4);
            font-weight: 200;
            font-style: normal;
        }

        @font-face {
            font-family: "formular";
            src: url(/fonts/formular-lightitalic.otf?d4ccfa010400cdee3afa5912d146b3f6);
            font-weight: 200;
            font-style: italic;
        }

        @font-face {
            font-family: "formular";
            src: url(/fonts/formular-regular.otf?638688cf5643ab792bd88b62b254d16a);
            font-weight: 400;
            font-style: normal;
        }

        @font-face {
            font-family: "formular";
            src: url(/fonts/formular-regularitalic.otf?68272d92a0d983cb7bf80884ab629f18);
            font-weight: 400;
            font-style: italic;
        }

        @font-face {
            font-family: "formular";
            src: url(/fonts/formular-medium.otf?f7f790441358540518ec9b2f6d109f27);
            font-weight: 500;
            font-style: normal;
        }

        @font-face {
            font-family: "formular";
            src: url(/fonts/formular-mediumitalic.otf?9b08916df170924f5f6b57b6370496e1);
            font-weight: 500;
            font-style: italic;
        }

        @font-face {
            font-family: "formular";
            src: url(/fonts/formular-bold.otf?b4065b8cd46de2cb33836858d5adba57);
            font-weight: 700;
            font-style: normal;
        }

        @font-face {
            font-family: "formular";
            src: url(/fonts/formular-bolditalic.otf?1213d7111c42fc8aeceb0559390a48f2);
            font-weight: 700;
            font-style: italic;
        }

        @font-face {
            font-family: "formular";
            src: url(/fonts/formular-black.otf?556a5c00c692a689fece4cc47345e92c);
            font-weight: 900;
            font-style: normal;
        }

        @font-face {
            font-family: "formular";
            src: url(/fonts/formular-blackitalic.otf?4a8ee0fe529cd0385b6f9a7d844e125f);
            font-weight: 900;
            font-style: italic;
        }

        body {
            margin: 0;
            padding: 0;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
            background-color: #000;
        }

        table {
            border-spacing: 0;
        }

        table td {
            border-collapse: collapse;
            box-sizing: border-box;
        }
    </style>
</head>
<body style="margin: 0; padding: 50px 0 0; background: #000000;font-family:'formular', sans-serif; font-weight: 500;">

<table border="0" cellpadding="0" cellspacing="0" style=" width: 100% !important; margin:0; padding:0 !important; background-color: #000000; color: #ffffff; ">
    <tbody>
    <tr style="width: 100% !important; border: none; padding: 0;">
        <td style="padding: 0; width: 100% !important; max-width: 100%;">

            <table align="center" cellspacing="0" cellpadding="0" border="0"
                   style="width: 100%;
                           max-width: 670px;">
                <tbody>

                <tr class="logo-top">
                    <td align="center" width="100%"
                        style="padding: 30px 15px 25px 15px;">

                        <img src="{{ asset('images/email/logo-top.png') }} " alt="alt"
                             style="display: block;
                                                 width: 518px;">

                    </td>
                </tr>

                <tr class="main-title">
                    <td align="center" width="100%"
                        style="padding: 15px 15px 0px 15px;">
                        <p style="font-size: 30px; font-family:'formular', sans-serif; font-weight: 500;margin: 0;color: #fff;">
                            Восстановление пароля
                        </p>

                    </td>
                </tr>

                <tr class="main-photo">
                    <td width="100%"
                        style="">
                        <img src="	{{ asset('images/email/logo.jpg') }}" alt="alt"
                             style="display: block;
                                         object-fit: cover;

                                         width: 500px;margin: 72px auto 43px;">
                    </td>
                </tr>

                <tr class="content">
                    <td align="left" width="100%" style="padding: 0 15px">

                        <table class="content__text" align="center" width="100%" cellpadding="0" cellspacing="0"
                               style="border-spacing:0; max-width: 600px; margin-top: 15px;margin-bottom: 16px;">
                            <tbody>

                            <tr>
                                <td align="left" width="100%"
                                    style="text-align: left;
                                                        color: #fff;
                                                        font-size: 17px;
                                                        font-weight: 400;
                                                        line-height: 22px;
                                                        max-width: 600px;
                                                        width: 100%;
                                                        margin: auto;
                                                        box-sizing: border-box;">
                                    {!! $content !!}
                                </td>

                            </tr>

                            </tbody>

                        </table>

                    </td>
                </tr>

                <tr style="background: #141414;">
                    <td>
                        <table style="display: block;
                                                  max-width: 600px;
                                                  margin: auto;
                                                  padding: 35px;
                                                  box-sizing: border-box;">
                            <tbody>
                            <tr>
                                <td>
                                    <p style="box-sizing:border-box;outline:0px;margin:0px;padding:0px;line-height:normal;float:none;font-family: 'formular', sans-serif;font-size: 26px;color: #fff;">
                                        Хотите быть в курсе всех новостей
                                        проекта? Подпишитесь на нас.
                                    </p>
                                    <p style="box-sizing:border-box;outline:0px;margin:0px;padding:0px;line-height:inherit;float:none;">
                                        <br></p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="https://www.facebook.com/superludi.official/?epa=SEARCH_BOX" target="_blank"  style="display:inline-block;margin-right: 10px;">
                                        <img src=" {{ asset('images/email/fb.png') }}" alt="alt" style="width: 50px;">
                                    </a>
                                    <a href="https://t.me/sl_media" target="_blank" style="display:inline-block;">
                                        <img src=" {{ asset('images/email/tl.png') }}" alt="alt" style="width: 50px;">
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>

                </tbody>

            </table>
        </td>
    </tr>
    </tbody>

</table>

</body>
</html>