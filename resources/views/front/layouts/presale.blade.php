<!doctype html>
<html lang="{{ app()->getLocale() }}" class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta property="og:title" content="Премьера первого сезона." />
    <meta property="og:image"  content="{{ asset('images/open-g.jpg') }}" />
    <meta property="og:description"
          content="Премьера первого образовательного сезона медиаплатформы Суперлюди." />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {!! SEO::generate(true) !!}

    <link rel="stylesheet" href="{{ mix('/css/front-presale.css') }}">

</head>

<body>

@include('front.parts.presale.header')

@yield('content')

@include('front.modals.presale.thanks')

@include('front.modals.presale.error')

<script src="{{ mix('/js/app-front-presale.js') }}"></script>

</body>

</html>
