<!doctype html>
<html lang="{{ app()->getLocale() }}" class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @if (!empty($refresh_token))
        <meta name="jwt-token" content="{{ $refresh_token }}">
    @endif

    {!! SEO::generate(true) !!}

    <link rel="stylesheet" href="{{ mix('/css/front.css') }}">

    <!-- Google Tag Manager -->
    <script>
        (function(w,d,s,l,i) {
            w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});
            var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';
            j.async=true;
            j.src= 'https://www.googletagmanager.com/gtm.js?id='+i+dl;
            f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MXMRLM7');
    </script>
    <!-- End Google Tag Manager -->

</head>

<body>

<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MXMRLM7" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

@yield('body')

@include('front.modals')

<script src="//fast.wistia.net/assets/external/E-v1.js" async></script>
<script src="{{ mix('/js/app-front.js') }}"></script>

</body>

</html>
