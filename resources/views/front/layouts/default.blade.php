@extends('front.layouts.base')

@section('body')

    @include('front.parts.header')

    @yield('content')

    @include('front.parts.footer')

    @include('front.parts.home.piracy')

@stop
