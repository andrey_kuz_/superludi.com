@extends('front.layouts.default')

@section('content')

    <section class="course-list sections">

        <div class="row">

            <div class="course-list__title">
                <h2>@lang('front.course-list.title')</h2>
            </div>

            <div class="course-list__sub">
                <div class="course-list__sub-left">
                    <p>@lang('front.course-list.description.title')</p>
                </div>
                <div class="course-list__sub-right">
                    <p>@lang('front.course-list.description.item_1')</p>

                    <p>@lang('front.course-list.description.item_2')</p>
                </div>

                <div class="course-list__sub-btn">
                    <button class="btn-access">
                        @lang('front.course-list.get_access')
                    </button>
                </div>
            </div>

            @foreach($seasons as $season)
                <div class="course-list__wrap">

                    <div class="course-list__wrap-title">
                        <h2>{{ $season->title_short }}</h2>
                    </div>

                    <div class="course-list__wrap-content">

                        @foreach($season->courses as $course)
                            <div class="item">
                                <a href="{{ route('course', $course->slug) }}" class="item__link">

                                    <img src=" {{ asset('images/empty.png') }}" alt="alt">

                                    <span class="item__link-img">
                                        <img src="{{ $course->preview_image['src'] }}" alt="alt">
                                    </span>

                                    <span class="item__link-top">
                                        <span class="name">{{ $course->speaker->name }}</span>
                                        <span class="play">
                                            <img src=" {{ asset('images/play1.png') }}" alt="alt">
                                        </span>
                                    </span>

                                    <span class="item__link-text">
                                        <p>{{ $course->speaker->position }}</p>
                                    </span>

                                    <span class="item__link-bottom">
                                        <span class="hd">@lang('front.course-list.quality')</span>
                                        <span class="series">{{ $course->lessons->count() }} @lang('front.course-list.series')</span>
                                        <span class="lessons">@lang('front.course-list.materials')</span>
                                    </span>
                                </a>

                                <div class="item__icon">
                                    <span class="hd">@lang('front.course-list.quality')</span>
                                    <span class="series">{{ $course->lessons->count() }} @lang('front.course-list.series')</span>
                                    <span class="lessons">@lang('front.course-list.materials')</span>
                                </div>

                            </div>
                        @endforeach

                    </div>
                </div>
            @endforeach
        </div>
    </section>
@stop

