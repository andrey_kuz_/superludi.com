@extends('front.layouts.default')

@section('content')

    @include('front.parts.blog-post.blog-post-heading')

    @include('front.parts.blog-post.blog-post-content')

    <div class="blog-post-tags">
        @include('front.parts.blog.home-tag-cloud')
    </div>

    <div class="blog-post-comments">
        @include('front.comments')
    </div>

    @include('front.parts.blog-post.blog-post-database')

@stop
