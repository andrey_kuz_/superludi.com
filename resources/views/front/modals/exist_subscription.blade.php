<div class="modal-info" id="modal-exist_subscription" data-reveal {{ (\Route::currentRouteName() == 'exist_subscription') ? 'data-show' : '' }}>
    <div class="modal-dialog">
        <div class="modal-content">
            <button class="modal-close close" data-close aria-label="Close modal" type="button"><img src="{{ asset('images/modal_close.png') }}" alt=""></button>
            <div class="enter-box">
                <div class="enter-box__content">
                    <div class="thank_payment_title">
                        <p>@lang('front.modals.exist_subscription.title')</p>
                    </div>
                    <div class="btn-enter">
                        <a href="{{ route('profile') }}">@lang('front.modals.exist_subscription.to_profile')</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
