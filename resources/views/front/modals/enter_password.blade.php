<div class="modal-info" id="modal-enter_password" data-reveal {{ (\Route::currentRouteName() == 'enter_password') ? 'data-show' : '' }}>
    <div class="modal-dialog">
        <div class="modal-content">
            <button class="modal-close close" data-close aria-label="Close modal" type="button"><img src="{{ asset('images/modal_close.png') }}" alt=""></button>
            <div class="enter-box">
                <div class="enter-box__content">
                    <div class="expired_title">
                        <p>@lang('front.modals.enter_password.title')</p>
                    </div>
                    <form id="login_exist_email" action="{{ route('api.login') }}" class="enter-box__form login_form">
                        <div class="form-group">
                            <input name="fb_id" class="form-control" type="hidden" value="{{ ( Session::has('fb_user') ) ? Session::get('fb_user')->id : '' }}">
                            <div class="error-message"></div>
                        </div>
                        <div class="form-group">
                            <input name="email" class="form-control" type="hidden" value="{{ ( Session::has('fb_user') ) ? Session::get('fb_user')->email : '' }}">
                            <div class="error-message"></div>
                        </div>
                        <div class="form-group">
                            <input name="password" class="form-control" type="password" placeholder="@lang('front.modals.enter_password.password_placeholder')" autocomplete="new-password">
                            <div class="error-message"></div>
                        </div>
                        <div class="form-group">
                            <div class="invalid_data error-message"></div>
                        </div>
                        <div class="form-group">
                            <button id="login_exist_email_button" class="enter-box__button btn btn-block">@lang('front.modals.enter_password.continue')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
