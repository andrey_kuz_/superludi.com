<div class="modal-info" id="modal-presale_error" data-reveal {{ (\Route::currentRouteName() == 'presale.error') ? 'data-show' : '' }}>
    <div class="modal-dialog">
        <div class="modal-content">
            <button class="modal-close close" data-close aria-label="Close modal" type="button"><img src="{{ asset('images/modal_close.png') }}" alt=""></button>
            <div class="enter-box">
                <div class="enter-box__content">
                    <div class="thank_payment_title">
                        <p>@lang('front.modals.error_payment.title')</p>
                    </div>
                    <div class="thank_payment_message">
                        <p>@lang('front.modals.error_payment.message')</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
