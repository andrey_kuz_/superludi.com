<div class="modal-info" id="modal-login" data-reveal {{ (\Route::currentRouteName() == 'login') ? 'data-show' : '' }}>
    <div class="modal-dialog">
        <div class="modal-content">
            <button class="modal-close close" data-close aria-label="Close modal" type="button"><img src="{{ asset('images/modal_close.png') }}" alt=""></button>
            <div class="enter-box">
                <div class="enter-box__title">@lang('front.modals.login.login')</div>
                <div class="enter-box__content">
                    <form id="form_login" action="{{ route('api.login') }}" class="enter-box__form login_form">
                        <div class="form-group">
                            <input name="email" class="form-control" type="email" placeholder="Email" autocomplete="new-password">
                            <div class="error-message"></div>
                        </div>

                        <div class="form-group">
                            <input name="password" class="form-control" type="password" placeholder="@lang('front.modals.login.password_placeholder')" autocomplete="new-password">
                            <div class="error-message"></div>
                        </div>
                        <div class="form-group">
                            <div class="invalid_data error-message"></div>
                        </div>

                        <div class="form-group">
                            <button id="form_login_button" class="enter-box__button btn btn-block" type="submit">@lang('front.modals.login.continue')</button>
                        </div>
                    </form>

                    <div class="btn-enter">
                        <button id="open_register" class="link">@lang('front.modals.login.to_register')</button>
                    </div>

                    <div class="btn-enter">
                        <button id="open_forgot_password" class="link">@lang('front.modals.login.forgot_password')</button>
                    </div>
                </div>
            </div>
            <div class="enter-box__social">
                <a href="{{ route('login_by_provider', 'facebook') }}" class="enter-fb link">
                    <span>@lang('front.modals.register.fb_login')</span> <img src="{{ asset('images/fb_icon.png') }}" alt="alt">
                </a>
            </div>
        </div>
    </div>
</div>
