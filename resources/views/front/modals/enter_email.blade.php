<div class="modal-info" id="modal-enter_email" data-reveal {{ (\Route::currentRouteName() == 'enter_email') ? 'data-show' : '' }}>
    <div class="modal-dialog">
        <div class="modal-content">
            <button class="modal-close close" data-close aria-label="Close modal" type="button"><img src="{{ asset('images/modal_close.png') }}" alt=""></button>
            <div class="enter-box">
                <div class="enter-box__content">
                    <div class="expired_title">
                        <p>@lang('front.modals.enter_email.title')</p>
                    </div>
                    <form id="register_by_email" action="{{ route('api.register_by_email') }}" class="enter-box__form">
                        <div class="form-group">
                            <input name="fb_id" class="form-control" type="hidden" value="{{ ( Session::has('fb_user') ) ? Session::get('fb_user')->id : '' }}">
                        </div>
                        <div class="form-group">
                            <input name="name" class="form-control" type="hidden" value="{{ ( Session::has('fb_user') ) ? Session::get('fb_user')->name : '' }}">
                        </div>
                        <div class="form-group">
                            <input name="avatar" class="form-control" type="hidden" value="{{ ( Session::has('fb_user') ) ? Session::get('fb_user')->avatar_original : '' }}">
                        </div>
                        <div class="form-group">
                            <input name="email" class="form-control" type="email" placeholder="Email" autocomplete="new-password">
                            <div class="error-message"></div>
                        </div>
                        <div class="form-group">
                            <button id="register_by_email_button" class="enter-box__button btn btn-block">@lang('front.modals.enter_email.continue')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
