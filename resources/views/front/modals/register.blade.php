<div class="modal-info" id="modal-registration" data-reveal {{ (\Route::currentRouteName() == 'register') ? 'data-show' : '' }}>
    <div class="modal-dialog">
        <div class="modal-content">
            <button class="modal-close close" data-close aria-label="Close modal" type="button"><img src="{{ asset('images/modal_close.png') }}" alt=""></button>
            <div class="enter-box">
                <div class="enter-box__title">@lang('front.modals.register.register')</div>
                <div class="enter-box__content">
                    <form id="form_registration" action="{{ route('api.register') }}" class="enter-box__form">

                        <input type="hidden" name="just_registered" value="1" />

                        <div class="form-group">
                            <input name="name" class="form-control" type="text" placeholder="@lang('front.modals.register.name_placeholder')" autocomplete="new-password">
                            <div class="error-message"></div>
                        </div>

                        <div class="form-group">
                            <input name="email" class="form-control" type="email" placeholder="Email" autocomplete="new-password">
                            <div class="error-message"></div>
                        </div>

                        <div class="form-group">
                            <input name="password" class="form-control" type="password" placeholder="@lang('front.modals.register.password_placeholder')" autocomplete="new-password">
                            <div class="error-message"></div>
                        </div>
                        @if(Session::has('utm_tags'))
                            <div class="form-group">
                                <input name="utm_tags" class="form-control" type="hidden" value="{{ Session::get('utm_tags') }}">
                            </div>
                        @endif

                        {{--<div class="form-group">--}}
                            {{--<input name="confirm_password" class="form-control" type="password" placeholder="@lang('front.modals.register.confirm_password_placeholder')" autocomplete="new-password">--}}
                            {{--<div class="error-message"></div>--}}
                        {{--</div>--}}

                        <div class="form-group">
                            <input name="phone" id="phone" type="tel" class="form-control phone_mask" placeholder="@lang('front.modals.register.phone_placeholder')" autocomplete="new-password">
                            <div class="error-message"></div>
                        </div>

                        <div class="form-group">
                            <button id="form_registration_button" class="enter-box__button btn btn-block">@lang('front.modals.register.continue')</button>
                        </div>
                    </form>

                    <div class="btn-enter">
                        <button id="open_login" class="link">@lang('front.modals.register.to_login')</button>
                    </div>
                </div>
            </div>
            <div class="enter-box__social">
                <a href="{{ route('login_by_provider', 'facebook') }}" class="enter-fb link">
                    <span>@lang('front.modals.register.fb_login')</span> <img src="{{ asset('images/fb_icon.png') }}" alt="alt">
                </a>
            </div>
        </div>
    </div>
</div>
