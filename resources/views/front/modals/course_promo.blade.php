<div class="modal-video" id="modal-promo" data-reveal>

    <button class="modal-close close"  data-close aria-label="Close modal" type="button"><img src="{{ asset('images/modal_close.png') }}" alt=""></button>
    <div class="videoframe">
        <img src="{{ asset('images/placehoder-16x9.svg') }}" />
        <iframe src=""></iframe>
    </div>

</div>
