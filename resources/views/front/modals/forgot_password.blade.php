<div class="modal-info" id="modal-forgot_password" data-reveal {{ (\Route::currentRouteName() == 'forgot_password') ? 'data-show' : '' }}>
    <div class="modal-dialog">
        <div class="modal-content">
            <button class="modal-close close" data-close aria-label="Close modal" type="button"><img src="{{ asset('images/modal_close.png') }}" alt=""></button>
            <div class="enter-box">
                <div class="enter-box__title">
                    <p>@lang('front.modals.forgot_password.title')</p>
                </div>
                <div class="enter-box__content">
                    <form id="forgot_password" action="{{ route('api.forgot_password') }}" class="enter-box__form">
                        <div class="form-group">
                            <input name="email" class="form-control" type="email" placeholder="Email" autocomplete="new-password">
                            <div class="error-message"></div>
                            <div class="success-message"></div>
                        </div>
                        <div class="form-group">
                            <button id="forgot_password_button" class="enter-box__button btn btn-block">@lang('front.modals.forgot_password.continue')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
