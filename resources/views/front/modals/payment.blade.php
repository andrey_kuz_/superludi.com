<div class="modal-info" id="modal-payment" data-reveal>
    <div class="modal-dialog">
        <div class="modal-content">
            <button class="modal-close close" data-close aria-label="Close modal" type="button"><img src="{{ asset('images/modal_close.png') }}" alt=""></button>
            <div class="enter-box">
                <div class="enter-box__title">@lang('front.modals.payment.payment')</div>
                @if(!empty($active_subscription))
                    <div class="enter-box__content">
                        <div class="payment_message">
                            <p>@lang('front.modals.payment.redirect_to_fondy')</p>
                        </div>
                        <div>
                            <a href="{{ route('subscription.checkout', $active_subscription) }}" id="payment_button" class="enter-box__button btn btn-block">@lang('front.modals.payment.continue')</a>
                        </div>
                        <div class="access_message">
                            <p>* @lang('front.modals.payment.access') {{ $active_subscription->period }} @lang('front.modals.payment.period')</p>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
