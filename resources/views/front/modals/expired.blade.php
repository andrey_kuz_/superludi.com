<div class="modal-info" id="modal-expired" data-reveal>
    <div class="modal-dialog">
        <div class="modal-content">
            <button class="modal-close close" data-close aria-label="Close modal" type="button"><img src="{{ asset('images/modal_close.png') }}" alt=""></button>
            <div class="enter-box">
                <div class="enter-box__content">
                    <div class="expired_title">
                        <p>@lang('front.modals.expired.message')</p>
                    </div>
                    <div class="expired_message">
                        <p>@lang('front.modals.expired.access') <a href="#">@lang('front.modals.expired.link')</a></p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
{{--<button class="button btn-free" data-action="openModal" data-target="#modal-expired">Expired</button>--}}
