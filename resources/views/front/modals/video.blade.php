<div class="modal-video" id="aboutVideo" data-reveal>

    <button class="modal-close close"  data-close aria-label="Close modal" type="button"><img src="{{ asset('images/modal_close.png') }}" alt=""></button>
    <div class="videoframe">
        <img src="{{ asset('images/placehoder-16x9.svg') }}" />
        <iframe src="https://fast.wistia.net/embed/iframe/2dkvjiom21?videoFoam=true"></iframe>
    </div>

</div>
