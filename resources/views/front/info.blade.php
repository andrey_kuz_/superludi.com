@extends('front.layouts.default')

@section('content')

    <div class="info-page">

        <div class="row">

            <div class="large-12">

                {!! $info->content !!}

            </div>

        </div>

    </div>

@stop

