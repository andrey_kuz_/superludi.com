@extends('front.layouts.presale')

@section('content')
    <div class="presale">

        <div class="knowledge">Знания лучших</div>

        <div class="row">
            <div class="columns tiny-12">
                <div class="presale-content">
                    <div class="presale-content-left">
                        <h1>Премьера <br> первого сезона</h1>
                        <h5>Участники первого сезона:</h5>
                        <ul class="presale-speakers">
                            <li>
                                <p>Андрей Федорив</p>
                                <span>Основатель креативного агентства Fedoriv <br> — Как построить позиционирование бренда;</span>
                            </li>
                            <li>
                                <p>Владимир Поперешнюк</p>
                                <span>Сооснователь компании Нова Пошта <br> — Как создать большую компанию;</span>
                            </li>
                            <li>
                                <p>Андрей Здесенко</p>
                                <span>Президент корпорации Биосфера <br> — Как расти в кризис;</span>
                            </li>
                            <li>
                                <p>Эдгар Каминский</p>
                                <span>Основатель Kaminskyi Clinic <br> — Как работать с персональным брендом.</span>
                            </li>
                        </ul>
                        <div class="presale-format">
                            <h4>Формат:</h4>
                            <p>4 новых героя. 4 новых курса. Только практики, только лучшие предприниматели;</p>
                            <p>доступ к целому сезону на 90 дней;</p>
                            <p>дополнительные материалы от героев сезона;</p>
                            <p>новая методология. Обучающая программа на основе опыта предпринимателей;</p>
                            <p>изображение и звук лучшего качества. Мы внедряем новые стандарты в онлайн-образовании.</p>
                        </div>

                        @include('front.parts.presale.footer')

                    </div>
                    <div class="presale-content-right">
                        <div class="form-box">
                            <form action="{{ route('presale.checkout') }}" class="form" id="presale_form" method="POST">
                                @csrf
                                <div class="form-price">
                                    <span>299$<sup>399$</sup></span>
                                </div>
                                <div class="input-box">
                                    <input type="text" name="name" placeholder="Имя, фамилия">
                                    <div class="form-error"></div>

                                    <input type="email" name="email" placeholder="Email">
                                    <div class="form-error"></div>

                                    <input name="phone" type="tel" placeholder="Телефон" class="phone_mask">
                                    <div class="form-error"></div>

                                    <input name="promo" type="text" placeholder="Промокод">

                                    <input type="submit" value="Продолжить" id="presale_button">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
