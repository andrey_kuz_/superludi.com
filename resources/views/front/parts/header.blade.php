<header class="header sections">
    <div class="row">
        <div class="header__content">

            <div class="header__content-logo">
                <a href="/">
                    <img src="{{ asset('/images/logo.png') }}" alt="logo">
                </a>
            </div>

            <div class="header__content-wrap">
                @if ( !in_array(\Route::currentRouteName(), ['info']) )
                    @if(!empty($active_subscription))
                        @if($user)
                            @if (!$user->active_subscriptions->count())
                                <a href="{{ route('subscription.checkout', $active_subscription) }}" class="btn-access payment_link {{ (Route::currentRouteName() == 'course') ? 'fixed' : '' }}" data-subscription="{{ $active_subscription->id }}">@lang('front.parts.header.get_access')</a>
                            @endif
                        @else
                            <a href="{{ route('subscription.checkout', $active_subscription) }}" class="btn-access payment_link" data-subscription="{{ $active_subscription->id }}">@lang('front.parts.header.get_access')</a>
                        @endif
                    @endif
                @endif
                @if($user)
                    <div class="header__content-profile">
                        <div class="header__content-profile-name">
                            {{ ($user->info->full_name) ?  $user->info->full_name :  trans('front.parts.header.default_user_name') }}
                        </div>

                        <div class="header__content-profile-avatar">
                            <img src="{{ ($user->info->avatar['src']) ?  $user->info->avatar['src'] : asset('images/default_avatar.png') }}" alt="ava">
                        </div>

                        <div class="header__content-profile-btn">
                            <a href="{{ route('profile') }}" class="profile">@lang('front.parts.header.profile_link')</a>
                            <button class="logout" id="logout">@lang('front.parts.header.logout')</button>
                        </div>
                    </div>
                @else
                    <div class="header__content-btn">
                        <div class="btn-enter">
                            <button class="btnEnter" data-action="openModal" data-target="#modal-login"> @lang('front.parts.header.login')</button>
                        </div>
                    </div>
                @endif
            </div>


        </div>

    </div>
</header>
