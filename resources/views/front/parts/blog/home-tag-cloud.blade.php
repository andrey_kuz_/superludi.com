<section class="tag-cloud-section row">

    <div class="tag-list">

            @foreach($tags as $tag)
                @if(!empty($tag_current) && $tag->id == $tag_current->id)
                    <a class="tag-list-item active" href=""><span>#</span>{{ $tag->title }}</a>
                @else
                    <a class="tag-list-item " href="{{ route('blog-tag', ['slug' => $tag->slug  ]) }}"><span>#</span>{{ $tag->title }}</a>
                @endif
            @endforeach

    </div>

    <div class="more-tags more-blog-tags">еще</div>

</section>