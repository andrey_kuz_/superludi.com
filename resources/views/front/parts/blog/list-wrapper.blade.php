<div class="post-list-wrapper row">
    @include('front.parts.blog.post-list')
    @if( $posts->count() > config('blog.main_show_count_post') )
        <button class="load-posts" id="show_more_posts">
            <span>@lang('front.parts.blog.list-wrapper.look') <br> @lang('front.parts.blog.list-wrapper.more')</span>
            <sup>{{ $posts->count() - config('blog.main_show_count_post') }}</sup>
        </button>
    @endif
</div>
