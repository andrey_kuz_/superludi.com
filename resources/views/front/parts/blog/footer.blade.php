<footer class="responsive-blog-footer">
    <div class="row">
        <div class="medium-8 columns small-order-2 medium-order-1 about-container">
            <div class="row">
                <div class="hide-for-small-only medium-4 columns about-section">
                    <img src="https://placehold.it/250x250">
                </div>
                <div class="medium-8 columns about-section">
                    <h4>@lang('front.parts.blog.footer.about_me')</h4>
                    <p>@lang('front.parts.blog.footer.description')</p>
                    <a href="">@lang('front.parts.blog.footer.read_more')</a>
                </div>
            </div>
        </div>
        <div class="small-12 medium-4 columns small-order-1 medium-order-2 mailing-container">
            <h4 class="mailing-list">@lang('front.parts.blog.footer.join_list')</h4>
            <input type="text" placeholder="@lang('front.parts.blog.footer.email_address')">
            <a class="button expanded subscribe-button" href="#">@lang('front.parts.blog.footer.subscribe_now')</a>
        </div>
    </div>
    <div class="row tag-search">
        <div class="columns">
            <h4>@lang('front.parts.blog.footer.search_by_tag')</h4>
            <ul class="menu simple tag-section">
                @foreach($tags as $tag)
                <li><a href="{{ route('blog-tag', ['slug' => $tag->slug  ]) }}">{{ $tag->title }}</a></li>
                @endforeach
            </ul>
        </div>
    </div>
    <div class="row columns flex-container align-justify">
        <p>@lang('front.parts.blog.footer.aall_rights')</p>
        <div class="up-arrow">
            <a href="#top"><i class="fa fa-chevron-circle-up" aria-hidden="true"></i></a>
        </div>
    </div>
</footer>
