<?php $cnt = 1; ?>
<div class="post-item-box">
    @foreach($posts->take(config('blog.main_show_count_post')) as $post)
        <div class="post-item {{$classes[$loop->index]}} rellax post_item"   data-rellax-speed="{{$cnt}}" data-rellax-percentage="0.5">
            <div class="post-item-image">
                <a href="{{ route('blog-post', ['category_slug' => $post->category->slug, 'post_slug' => $post->slug]) }}">
                    <img src="{{ $post->thumbnail_image['src'] }}" alt="post">

                    <span class="shareit">
                        <span class="icon">
                            <img src=" {{ asset('images/shared.png') }}" alt="alt">
                        </span>
                        @if(!empty($post->social_count->share_count))
                            <span class="text">{{$post->social_count->share_count}}</span>
                        @else
                            <span class="text">0</span>
                        @endif
                    </span>
                    <span class="views">
                        <span class="icon">
                            <img src=" {{ asset('images/viewed.png') }}" alt="alt">
                        </span>
                        @if(!empty($post->social_count->view_count))
                            <span class="text">{{ $post->social_count->view_count }}</span>
                        @else
                            <span class="text">0</span>
                        @endif
                    </span>
                </a>
            </div>
            <div class="post-item-description">
                <a href="{{ route('blog-post', ['category_slug' => $post->category->slug, 'post_slug' => $post->slug]) }}">{{$post->title }}</a>
            </div>
        </div>
        <?php $cnt += 0.5; ?>
        @if($loop->index == 3)
            {{--<div class="post-item normal rellax message left-aligned" data-rellax-speed="{{$cnt}}" data-rellax-percentage="0.5">--}}
                {{--<div class="post-item-image">--}}

                {{--</div>--}}
                {{--<div class="post-item-description">--}}
                    {{--<p>@lang('front.parts.blog.post-list.important_message')</p>--}}
                {{--</div>--}}
            {{--</div>--}}
        @endif
    @endforeach
</div>

