<div class="more row">
    <h2>@lang('front.parts.course.more.title') {{ $course->season->title_short }}</h2>
    <div class="more-wrapper">
        @foreach($more_courses->except($course->id) as $item)
            <div class="course-item">
                <a href="{{ route('course', $item->slug) }}">
                    <img src="{{ $item->preview_image['src'] }}" alt="">
                    <p class="title">{{ $item->speaker->name }}</p>
                    <p class="position">{{ $item->speaker->position }}</p>
                    <button class="play"></button>
                </a>
            </div>
        @endforeach

    </div>
</div>