<div class="about-main-sidebar">
    <div class="sidebar-list sidebar-list-desktop">
        @foreach($course->lessons as $lesson)
            <div class="course-item course_lesson {{ (!$lesson->hasAccess($user)) ? 'blocked' : '' }}"
                 {{ (!$lesson->hasAccess($user)) ? 'data-blocked="blocked' : '' }} id="{{ $lesson->id }}">
                <img src="{{ $lesson->preview_image['src'] }}" alt="">
                <aside>
                    <button class="play">
                        <img class="unlocked" src="{{ asset('images/play.png') }}" alt="alt">
                        <img class="blocked" src="{{ asset('images/close.png') }}" alt="alt">
                    </button>
                    <p>@lang('front.parts.course.sidebar.series') {{ $loop->iteration }}: {{ $lesson->title }}</p>
                </aside>
            </div>
        @endforeach
    </div>
    <button class="btn-arrow" id="scroll_lesson">
        <img src="{{ asset('images/arrow-down.png') }}" alt="btn">
    </button>

</div>
