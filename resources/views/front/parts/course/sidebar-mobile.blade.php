<div class="about-main-sidebar about-main-sidebar-mobile">
    <div class="sidebar-list">
        @foreach($course->lessons as $lesson)
            <div class="course-item course_lesson {{ (!$lesson->hasAccess($user)) ? 'blocked' : '' }}" id="{{ $lesson->id }}">
                <img src="{{ $lesson->preview_image['src'] }}" alt="">
                <aside>
                    <button class="play">
                        <img class="unlocked" src="{{ asset('images/play.png') }}" alt="alt">

                        <img class="blocked" src="{{ asset('images/close.png') }}" alt="alt">
                    </button>
                    <p>@lang('front.parts.course.sidebar.series') {{ $loop->iteration }}: {{ $lesson->title }}</p>
                </aside>
            </div>
        @endforeach
    </div>
</div>