<div class="about-nav columns large-12 medium-12 small-12 tiny-12">

    <div class="about-nav__dropdown">

        <button class="season" id="show_seasons">
            <span>{{ $course->season->title_short }}</span>
            <i></i>
        </button>

        {{--<ul class="other_seasons">--}}

            {{--@foreach($seasons as $season)--}}
                {{--@if($season->first_course)--}}
                    {{--<li>--}}
                        {{--<a href="{{ route('course', $season->first_course->slug) }}">{{ $season->title }}</a>--}}
                    {{--</li>--}}
                {{--@endif--}}
            {{--@endforeach--}}

        {{--</ul>--}}

    </div>

    <p class="name">{{ $course->speaker->name }}</p>

    <div class="about-nav-courselist">

        <ul>
            @foreach($more_courses as $item)
                <li class="{{ ($course->id == $item->id) ? 'active' : '' }}">
                    <a href="{{ route('course', $item->slug) }}">
                        <img src="{{ $item->speaker->avatar['src'] }}" alt="alt">
                    </a>
                </li>
            @endforeach
        </ul>

    </div>

</div>