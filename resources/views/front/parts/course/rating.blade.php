<div class="rating">
    <h4>@lang('front.parts.course.rating.title')</h4>
    <div class="rating-slider">
        <div class="rating-slider-point"></div>
    </div>
</div>