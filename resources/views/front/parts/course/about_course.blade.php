<div class="about row">

    @include('front.parts.course.nav')

    <div class="about-main columns large-12 medium-12 small-12 tiny-12">

        <div class="about-main-content course_content">

            <div class="current-video">

                <div class="current_video_player" style="display:none;">
                    <div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
                        <div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;">
                            <iframe src="{{ $course->banner_link }}"  title="Wistia" allowtransparency="true" frameborder="0" scrolling="no"  class="wistia_embed" name="wistia_embed" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen width="100%" height="100%"></iframe>
                        </div>
                    </div>
                </div>

                <div class="current-video-content">
                    <img src="{{ $course->banner_image['src'] }}"  alt="alt">
                    <p>{{ $course->speaker->position }}</p>
                    <button class="play-button"></button>
                    <aside>
                        <div>
                            <img src="{{ asset('images/hd1.png') }}" alt="">
                            <span>@lang('front.parts.course.about.quality')</span>
                        </div>
                        <div>
                            <img src="{{ asset('images/series1.png') }}" alt="">
                            <span>{{ $course->lessons->count() }} @lang('front.parts.course.about.series')</span>
                        </div>
                        <div>
                            <img src="{{ asset('images/mats1.png') }}" alt="">
                            <span>{{ $course->materials_count }} @lang('front.parts.course.about.materials')</span>
                        </div>
                    </aside>
                </div>
                <img src="{{ asset('images/empty.png') }}" class="gag current_lesson_preview" alt="alt">
            </div>
            @if($user && count($course->lessons))
                <div class="more-series">
                    <button class="more-button" id="show_lessons">
                        <span>@lang('front.parts.course.about.other_series')</span>
                        <i></i>
                    </button>
                </div>
            @endif
            @if($user && count($course->lessons))
                @include('front.parts.course.sidebar-mobile')
            @endif

            <div class="intro">
                <h4 class="current_lesson_title">{{ $course->title }}</h4>
                <p class="current_lesson_description">{{ $course->description }}</p>
            </div>

            @if ($user && $course->hasAccess($user) && $course->materials->count())
                <div class="materials current_lesson_materials">

                    <h4>@lang('front.parts.course.about.course_materials')</h4>

                    @foreach($course->materials as $material)

                        @if($material->size)

                            <div class="materials-item">

                                @switch($material->extension)
                                    @case('pdf')
                                        <img src="{{ asset('/images/pdf.png') }}" alt="alt">
                                    @break

                                    @case('xls')
                                        <img src="{{ asset('/images/xls.png') }}" alt="alt">
                                    @break

                                    @case('xlsx')
                                        <img src="{{ asset('/images/xls.png') }}" alt="alt">
                                    @break
                                @endswitch

                                <a href="{{ $material->file['src'] }}" target="_blank">{{ $material->title }}</a>
                                <sup>{{ $material->size }} MB</sup>

                            </div>

                        @endif

                    @endforeach

                </div>
            @endif

        </div>

        @include('front.parts.course.about_lesson')

        @if($user && count($course->lessons))
            @include('front.parts.course.sidebar')
        @endif

    </div>

</div>
