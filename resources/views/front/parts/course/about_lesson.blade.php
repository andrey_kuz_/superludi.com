<div class="about-main-content lesson_content">

    <div class="current-video">

        <div class="current_video_player" style="display:none;" id="" data-start-time="0">
            <div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
                <div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;">
                    <iframe src=""  title="Wistia" allowtransparency="true" frameborder="0" scrolling="no"  class="wistia_embed" name="wistia_embed" allowfullscreen mozallowfullscreen webkitallowfullscreen oallowfullscreen msallowfullscreen width="100%" height="100%"></iframe>
                </div>
            </div>
        </div>
        <div class="current-video-content">
            <img src="" alt="alt"  class=" current_lesson_preview">
            <button class="play-button"></button>
        </div>

        <img class="gag" src="{{ asset('images/empty.png') }}" alt="alt">
    </div>
    <div class="more-series">
        <button class="more-button" id="show_lessons">
            <span>@lang('front.parts.course.about.other_series')</span>
            <i></i>
        </button>
    </div>

    @include('front.parts.course.sidebar-mobile')

    <div class="intro">
        <h4 class="current_lesson_title"></h4>
        <p class="current_lesson_description"></p>
    </div>

    <div class="materials current_lesson_materials" data-materials="course">
        <h4>@lang('front.parts.course.about.course_materials')</h4>
    </div>

    <div class="materials current_lesson_materials" data-materials="lesson">
        <h4>@lang('front.parts.course.about.episode_materials')</h4>
    </div>

    <div class="comments"></div>

</div>

