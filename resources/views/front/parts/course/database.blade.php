@if(count($posts) && $user)
    <div class="database row">
        <h2>@lang('front.parts.course.database.title')</h2>
        <div class="database-wrapper">
            @foreach($posts->take(2) as $post)
                <div class="database-item">
                    <a href="{{ route('blog-post', [$post->category->slug, $post->slug]) }}" class="database-item-thumbnail">
                        <img src="{{ $post->thumbnail_image['src'] }}" alt="">
                    </a>
                    <a href="{{ route('blog-post', [$post->category->slug, $post->slug]) }}">
                        <p class="database-item-description">{{ $post->title }}</p>
                    </a>
                </div>
            @endforeach

            <a href="{{ route('blog-main') }}" class="more-materials">
                <span>@lang('front.parts.course.database.look')</span><br>
                <span>@lang('front.parts.course.database.more')</span>
                <sup>({{ $posts->count() }})</sup>
            </a>
        </div>
    </div>
@endif
