<section class="telegram sections" id="telegram">
    <div class="row">
        <div class="telegram__content">
            <div class="telegram__content-wrap">
                <div class="telegram__content-img" id="telegramMobile" >
                    <img src=" {{ asset('images/mobile.png') }}" alt="alt">
                </div>
            </div>

            <div class="telegram__content-right">
                <div class="text">
                    <p>@lang('front.parts.home.telegram.title')<br>@lang('front.parts.home.telegram.content')</p>
                </div>

                <a href="https://t.me/sl_media" target="_blank" class="btn-access">@lang('front.parts.home.telegram.join')</a>
            </div>
        </div>
    </div>
</section>