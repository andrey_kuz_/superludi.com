<section class="piracy sections" id="piracy">
    <div class="piracy__content">
        <div class="piracy__content-title">
            @lang('front.parts.home.piracy.title')
        </div>
        <div class="piracy__content-img">
            <img src="{{ asset('/images/pirat.png') }}" alt="alt">
        </div>

    </div>
</section>