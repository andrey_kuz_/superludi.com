<section class="info_course sections" id="infoCourse">
    <div class="row">

        <div class="info_course__content">

            <div class="info_course__content-bg rellax" data-rellax-speed="4.5" data-rellax-percentage="1">
                <img src=" {{ asset('images/logo-big.jpg') }}" alt="alt">

                <video playsinline="playsinline" preload="auto" autoplay="autoplay" muted="muted">
                    <source src="{{ asset('images/СС.mov') }}" type="video/mp4">
                </video>

            </div>

            <div class="info_course__content-title">
                <h1>@lang('front.parts.home.info_course.title')</h1>
            </div>

            <div class="info_course__content-box">
                <div class="info_course__content-item calculator">
                    <span>@lang('front.parts.home.info_course.courses.quantity')</span><br>@lang('front.parts.home.info_course.courses.item')
                </div>

                <div class="info_course__content-item lessons">
                    <span>@lang('front.parts.home.info_course.lessons.quantity')</span><br>@lang('front.parts.home.info_course.lessons.item')
                </div>

                <div class="info_course__content-item clock">
                    <span>@lang('front.parts.home.info_course.duration.quantity')</span><br>@lang('front.parts.home.info_course.duration.item')
                </div>

                <div class="info_course__content-item calendar">
                    <span>@lang('front.parts.home.info_course.access.quantity')</span><br>@lang('front.parts.home.info_course.access.item')
                </div>
            </div>
            
        </div>

    </div>
</section>