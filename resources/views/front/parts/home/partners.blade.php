<section class="partners sections">

    <div class="partners__title">
        <div class="row">
            <h2>@lang('front.parts.home.partners.title')</h2>
        </div>
    </div>

    <div class="partners__content">

        <div class="partners__content-item">
            <img src=" {{ asset('images/partner1.jpeg') }}" alt="alt">
        </div>

        <div class="partners__content-item">
            <img src=" {{ asset('images/partner2.jpeg') }}" alt="alt">
        </div>

        <div class="partners__content-item">
            <img src=" {{ asset('images/partner3.jpeg') }}" alt="alt">
        </div>

        <div class="partners__content-item">
            <img src=" {{ asset('images/partner4.jpeg') }}" alt="alt">
        </div>

        <div class="partners__content-item">
            <img src=" {{ asset('images/partner5.jpeg') }}" alt="alt">
        </div>

        <div class="break"></div>

        <div class="partners__content-item">
            <img src=" {{ asset('images/partner6.jpeg') }}" alt="alt">
        </div>

        <div class="partners__content-item">
            <img src=" {{ asset('images/partner7.jpeg') }}" alt="alt">
        </div>

        <div class="partners__content-item">
            <img src=" {{ asset('images/partner8.jpeg') }}" alt="alt">
        </div>

        <div class="partners__content-item">
            <img src=" {{ asset('images/partner9.jpeg') }}" alt="alt">
        </div>

        <div class="partners__content-item">
            <img src=" {{ asset('images/partner10.jpeg') }}" alt="alt">
        </div>

    </div>

</section>