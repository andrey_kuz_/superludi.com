<section class="subscription sections">
    <div class="row">
        <div class="subscription__title">
            <h2>@lang('front.parts.home.subscription.title')</h2>
        </div>

        <div class="subscription__content">
            <form class="subscription__content-form" action="{{ route('api.subscribe') }}" target="_blank" novalidate="novalidate" id="subcribe_unisender">

                <div class="subscription__content-form-row input-group">
                    <input type="email" class="email" name="email" placeholder="@lang('front.parts.home.subscription.email_placeholder')" required="required">
                    <button type="submit" name="subscribe" id="form_subscribe_button" class="btn-access">@lang('front.parts.home.subscription.receive')</button>
                    <div class="error-message"></div>
                </div>

                <div class="subscription__content-form-row checkbox">
                    <input name="user_agree_subscribe" id="user_agree_subscribe"  type="checkbox" value="true" >
                    <label for="user_agree_subscribe" class="user_agree_span" >@lang('front.parts.home.subscription.agree_subscribe.accept')
                        <a href="{{ route('info', ['slug' => 'terms']) }}" target="_blank">@lang('front.parts.home.subscription.agree_subscribe.conditions')</a> @lang('front.parts.home.subscription.agree_subscribe.and')<br>
                        <a href="{{ route('info', ['slug' => 'privacy']) }}" target="_blank">@lang('front.parts.home.subscription.agree_subscribe.privacy_policy')</a>.</label>
                    <div class="error-message"></div>
                </div>

                <div class="subscribe-box__form-message"></div>
                <div class="subscribe-box__form-success"></div>

                <button type="submit" name="subscribe" class="btn-access mobile">@lang('front.parts.home.subscription.receive')</button>

            </form>
        </div>
    </div>
</section>
