<section class="video_trailer sections">

    <div class="video_trailer__bg">
        <img src="{{ asset('/images/mainbg.jpg') }}" alt="alt">
        <video playsinline="playsinline" preload="auto" autoplay="autoplay" loop="loop" muted="muted">
            <source src="{{ asset('/images/COIN_LONG_1.mp4') }}" type="video/mp4">
        </video>
    </div>

    <div class="video_trailer__play">
        <button class="video_trailer__play-btn" data-action="openModal" data-target="#aboutVideo">
            <img src=" {{ asset('images/play.png') }}" alt="alt">
        </button>

        <div class="video_trailer__play-text">
            <p>@lang('front.parts.home.video_trailer.look_trailer')</p>
            <span>(@lang('front.parts.home.video_trailer.time'))</span>
        </div>

    </div>

    <div class="row">
        <div class="video_trailer__content">
            <div class="video_trailer__content-left">
                <p>@lang('front.parts.home.video_trailer.title')</p>
                @if(!empty($active_subscription))
                    @if($user)
                        @if($user->active_courses->count())
                            <a href="{{ route('courses') }}" class="btn-access-big payment_link">
                                <span>@lang('front.parts.home.video_trailer.view_courses')</span>
                            </a>
                        @else
                            <a href="{{ route('subscription.checkout', $active_subscription) }}" class="btn-access-big payment_link">
                                <span>@lang('front.parts.home.video_trailer.get_access') {{ count($active_subscription->active_courses) }} @lang('front.parts.home.video_trailer.courses_price') ${{ number_format($active_subscription->price) }}</span>
                            </a>
                        @endif
                    @else
                        <a href="{{ route('login', ['buy_subscription' => $active_subscription]) }}" class="btn-access-big payment_link">
                            <span>@lang('front.parts.home.video_trailer.get_access') {{ count($active_subscription->active_courses) }} @lang('front.parts.home.video_trailer.courses_price') ${{ number_format($active_subscription->price) }}</span>
                        </a>
                    @endif
                @endif
            </div>

            <div class="video_trailer__content-right">

                <div class="video_trailer__content-right-wrap">

                    <p>{{ $latest_season->title }}<span class="live"> LIVE</span></p>

                    <div class="btn-season">
                        @foreach($latest_season->courses as $key => $course)
                            <a class="btn-video-live scrollTo {{ ($key == 0) ?  'active' : '' }} "   href="#course_{{$course->id}}" id="item_course_{{ $course->id }}">
                                <img src="{{ $course->speaker->avatar['src'] }}" alt="alt">
                            </a>
                        @endforeach
                    </div>
                </div>

                <div class="video_trailer__content-right-wrap">
                    @if(!empty($active_subscription))
                        <a href="{{ route('subscription.checkout', $active_subscription) }}" class="btn-access-big payment_link" data-subscription="{{ $active_subscription->id }}">
                            <span>@lang('front.parts.home.video_trailer.get_access') {{ count($active_subscription->active_courses) }} @lang('front.parts.home.video_trailer.courses_price') ${{ number_format($active_subscription->price) }}</span>
                        </a>
                    @endif

                </div>

            </div>
        </div>
    </div>

    <div class="video_trailer__down">
        <a class="btn-down btn-animation scrollTo"  href="#whatsNew" >
            <img src=" {{ asset('images/down.png') }}" alt="alt">
        </a>
    </div>

</section>
