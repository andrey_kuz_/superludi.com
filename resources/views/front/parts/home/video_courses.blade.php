<section class="video_courses sections" id="videoCourses">

    <aside class="scrollTop">
        <button>Вверх</button>
    </aside>

    <div class="row">

        <div class="video_courses__content">

            <div class="video_courses__content-title rellax" data-rellax-speed="-2" data-rellax-percentage="0.5" >
                <h2>{{ $latest_season->title_short }}</h2>
            </div>

            <div class="video_courses__content-btn " data-rellax-speed="2" data-rellax-percentage="0.1">

                @foreach($latest_season->courses as $key => $course)
                    <a href="#course_{{$course->id}}" class="scrollTo btn-curses {{ ($key == 0) ?  'active' : ''}}" id="item_id_{{ $course->id }}">
                       {{ $course->title }}
                    </a>
                @endforeach

            </div>

            <div class="video_courses__content-video" id="videoCursesContent">

                <?php $cnt = 1; ?>
                @foreach($latest_season->courses as $course)

                    <div class="item" id="course_{{$course->id}}" data-rellax-speed="{{$cnt}}" data-rellax-percentage="0.5" data-course-id="{{ $course->id }}" data-scroll-block="end">

                        <a @if($user)
                            href="{{ route('course', $course->slug) }}"
                           @else
                            data-action="openPromoModal" data-promo-link="{{ $course->promo_link }}"
                           @endif
                           class="item__link">

                            <img src=" {{ asset('images/empty.png') }}" alt="alt">

                            <span class="item__link-img">
                               <img src=" {{ $course->preview_image['src'] }}" alt="alt">
                            </span>

                            <span class="item__link-top">
                               <span class="name">{{ $course->speaker->name }}</span>
                               <button class="play show_promo">
                               {{--<button class="play">--}}
                                   <img src=" {{ asset('images/play1.png') }}" alt="alt">
                               </button>
                            </span>

                            <span class="item__link-text">
                               <p>{{ $course->speaker->position }}</p>
                            </span>

                            <span class="item__link-bottom">
                                <span class="hd">@lang('front.parts.home.video_courses.quality')</span>
                                <span class="series">{{ $course->lessons->count() }} @lang('front.parts.home.video_courses.series')</span>
                                <span class="lessons">@lang('front.parts.home.video_courses.materials')</span>
                            </span>
                        </a>

                        <div class="icon" >
                            <span class="hd">@lang('front.parts.home.video_courses.quality')</span>
                            <span class="series">{{ $course->lessons->count() }} @lang('front.parts.home.video_courses.series')</span>
                            <span class="lessons">@lang('front.parts.home.video_courses.materials')</span>
                        </div>
                    </div>
                    <?php $cnt += 0.5; ?>

                @endforeach

                <div class="video_courses__content-video-btn" data-rellax-speed="{{$cnt}}" data-rellax-percentage="0.5">
                    <a href="{{ ($user) ? route('courses') : ''}}" class="btn-more" id="more_courses">
                       <span class="text">@lang('front.parts.home.video_courses.look') <br>@lang('front.parts.home.video_courses.all_courses')</span>
                        <span class="counter">
                           ({{ $count_published_courses }})
                       </span>
                    </a>
                </div>
            </div>

        </div>

    </div>

</section>
