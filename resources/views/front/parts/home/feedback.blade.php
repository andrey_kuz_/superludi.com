<section class="feedback sections">

    <div class="feedback__title">
        <h2>@lang('front.parts.home.feedback.title')</h2>
    </div>

    <div class="row">

        <div class="feedback__slider slider" >

            <div class="feedback__slider-item">
                <div class="item__content">
                    <div class="item__content-comment">
                        <p>@lang('front.parts.home.feedback.item_1.comment')</p>
                    </div>

                    <div class="item__content-name">
                        <h4>@lang('front.parts.home.feedback.item_1.name')</h4>
                    </div>

                    <div class="item__content-text">
                        <p>@lang('front.parts.home.feedback.item_1.position')</p>
                    </div>
                </div>

                <div class="item__photo">
                    <img src=" {{ asset('images/slider1.jpg') }}" alt="alt">
                </div>
            </div>

            <div class="feedback__slider-item">
                <div class="item__content">
                    <div class="item__content-comment">
                        <p>@lang('front.parts.home.feedback.item_1.comment')</p>
                    </div>

                    <div class="item__content-name">
                        <h4>@lang('front.parts.home.feedback.item_1.name')</h4>
                    </div>

                    <div class="item__content-text">
                        <p>@lang('front.parts.home.feedback.item_1.position')</p>
                    </div>
                </div>

                <div class="item__photo">
                    <img src=" {{ asset('images/slider1.jpg') }}" alt="alt">
                </div>
            </div>

            <div class="feedback__slider-item">
                <div class="item__content">
                    <div class="item__content-comment">
                        <p>@lang('front.parts.home.feedback.item_1.comment')</p>
                    </div>

                    <div class="item__content-name">
                        <h4>@lang('front.parts.home.feedback.item_1.name')</h4>
                    </div>

                    <div class="item__content-text">
                        <p>@lang('front.parts.home.feedback.item_1.position')</p>
                    </div>
                </div>

                <div class="item__photo">
                    <img src=" {{ asset('images/slider1.jpg') }}" alt="alt">
                </div>
            </div>

        </div>

    </div>

</section>
