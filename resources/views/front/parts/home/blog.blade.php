<section class="base sections">
    <div class="row">

        <div class="base__title">
            <h2>@lang('front.parts.home.base.title')</h2>
        </div>

        <div class="base__content">
            @foreach($posts->take(2) as $post)
                <div class="base__content-item">
                    <div class="base__content-item-img">
                        <a href="{{ route('blog-post', [$post->category->slug, $post->slug]) }}"><img src=" {{ $post->thumbnail_image['src'] }}" alt="alt"></a>
                    </div>
                    <div class="base__content-item-text">
                        <a href="{{ route('blog-post', [$post->category->slug, $post->slug]) }}"><p>{{ $post->title }}</p></a>
                    </div>
                </div>
            @endforeach

            <div class="base__content-btn">
                <a href="{{ route('blog-main') }}" class="btn-more">
                    <span class="text">@lang('front.parts.home.base.look_more')</span>
                    <span class="counter">({{ count($posts) }})</span>
                </a>
            </div>
        </div>

    </div>
</section>