<section class="question sections" id="question">

    <div class="row" >

        <h2 class="question__title title">@lang('front.parts.home.questions.title')</h2>

        <div class="question__content" id="questionContent">
            <ul class="accordion">
                <li>
                    <div class="accordion__title">@lang('front.parts.home.questions.item_1.title')</div>
                    <div class="accordion__content">
                        <p>@lang('front.parts.home.questions.item_1.content')</p>
                    </div>
                </li>

                <li>
                    <div class="accordion__title">@lang('front.parts.home.questions.item_2.title')</div>
                    <div class="accordion__content">
                        <p>@lang('front.parts.home.questions.item_2.content')</p>
                    </div>
                </li>

                <li>
                    <div class="accordion__title">@lang('front.parts.home.questions.item_3.title')</div>
                    <div class="accordion__content">
                        <p>@lang('front.parts.home.questions.item_3.content')</p>
                    </div>
                </li>

                <li>
                    <div class="accordion__title">@lang('front.parts.home.questions.item_4.title')</div>
                    <div class="accordion__content">
                        <p>@lang('front.parts.home.questions.item_4.content')</p>
                    </div>
                </li>

                <li>
                    <div class="accordion__title">@lang('front.parts.home.questions.item_5.title')</div>
                    <div class="accordion__content">
                        <p>@lang('front.parts.home.questions.item_5.content')</p>
                    </div>
                </li>

                <li>
                    <div class="accordion__title">@lang('front.parts.home.questions.item_6.title')</div>
                    <div class="accordion__content">
                        <p>@lang('front.parts.home.questions.item_6.content')</p>
                    </div>
                </li>
            </ul>
        </div>

    </div>

</section>