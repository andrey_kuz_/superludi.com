<section class="knowledge sections" id="knowledge">
    <div class="row">
        <div class="knowledge__title">
            <h2>@lang('front.parts.home.knowledge.title')</h2>
        </div>

        <div class="knowledge__content">

            <div class="knowledge__content-img" id="milk">
                <img src="{{ asset('/images/milk.png') }}" alt="alt">
            </div>

        </div>
    </div>
</section>