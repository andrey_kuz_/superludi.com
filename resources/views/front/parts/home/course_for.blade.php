<section class="course_for sections">

    <div class="row">
        <div class="course_for__title">
            <h2>@lang('front.parts.home.course_for.title')</h2>
        </div>
    </div>

    <div class="row align-center">

        <div class="medium-4 large-4  small-6 tiny-12 columns">
            <div class="course_for__item">
                <div class="course_for__item-icon">
                    <img src=" {{ asset('images/icon-rocket.png') }}" alt="alt">
                </div>
                <div class="course_for__item-text">
                    <p>@lang('front.parts.home.course_for.item_2')</p>
                </div>
            </div>
        </div>

        <div class="medium-4 large-4 small-6 tiny-12 columns">
            <div class="course_for__item">
                <div class="course_for__item-icon">
                    <img src=" {{ asset('images/icon-star.png') }}" alt="alt">
                </div>
                <div class="course_for__item-text">
                    <p>@lang('front.parts.home.course_for.item_1')</p>
                </div>
            </div>
        </div>

        <div class="medium-4 large-4  small-6 tiny-12 columns">
            <div class="course_for__item">
                <div class="course_for__item-icon">
                    <img src=" {{ asset('images/icon-dollar.png') }}" alt="alt">
                </div>
                <div class="course_for__item-text">
                    <p>@lang('front.parts.home.course_for.item_3')</p>
                </div>
            </div>
        </div>

    </div>

</section>
