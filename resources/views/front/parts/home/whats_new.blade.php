<section class="whats_new sections" id="whatsNew" data-scroll-block="center">
    <div class="row">

        <div class="whats_new-bg">
            <img class="rellax"  data-rellax-speed="-1" src=" {{ asset('images/screenshot1.jpg') }}" alt="alt">
        </div>

        <div class="whats_new__title">
            <h2>@lang('front.parts.home.whats_new.title')</h2>
        </div>

        <div class="whats_new__content">
            <div class="whats_new__content-item">
                <h4>@lang('front.parts.home.whats_new.item_1.title')</h4>
                <p>@lang('front.parts.home.whats_new.item_1.content')</p>
            </div>

            <div class="whats_new__content-item">
                <h4>@lang('front.parts.home.whats_new.item_2.title')</h4>
                <p>@lang('front.parts.home.whats_new.item_2.content')</p>
            </div>

            <div class="whats_new__content-item">
                <h4>@lang('front.parts.home.whats_new.item_3.title')</h4>
                <p>@lang('front.parts.home.whats_new.item_3.content')</p>
            </div>

            <div class="whats_new__content-item">
                <h4>@lang('front.parts.home.whats_new.item_4.title')</h4>
                <p>@lang('front.parts.home.whats_new.item_4.content')</p>
            </div>
        </div>
    </div>
</section>