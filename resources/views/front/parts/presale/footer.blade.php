<footer class="footer" id="footer">

    <div class="footer__content-item">
        <p>@lang('front.parts.footer.payment_types')</p>
        <div class="cards">
            <img src=" {{ asset('images/master.png') }}" alt="alt">
            <img src=" {{ asset('images/visa.png') }}" alt="alt">
        </div>
    </div>

    <div class="footer__content-item">

        <div class="mail">
            <p>@lang('front.parts.footer.support')</p>
            <a href="mailto:info@superludi.com">info@superludi.com</a>
        </div>

        <div class="phone">
            <a href="tel:0800210791">0 800 210 791</a>
            <span>@lang('front.parts.footer.free')</span>
        </div>

        <ul class="soc">
            <li><a href="https://t.me/sl_media" target="_blank">
                    <img src=" {{ asset('images/telegram.svg') }}" alt="alt">
                </a>
            </li>
            <li>
                <a href="https://www.facebook.com/superludi.official" target="_blank">
                    <img src=" {{ asset('images/facebook.svg') }}" alt="alt">
                </a>
            </li>
            <li>
                <a href="https://www.instagram.com/superludi.official/" target="_blank">
                    <img src=" {{ asset('images/instagram.svg') }}" alt="alt">
                </a>
            </li>
        </ul>

    </div>

    <div class="footer__content-item">
        <p class="footer-copyright">© @lang('front.parts.footer.copyright')</p>
    </div>

</footer>
