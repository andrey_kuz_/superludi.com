<header class="header">
    <div class="row">
        <div class="columns tiny-12">
            <div class="header-content">
                <a href="javascript:void(0)" class="logo">
                    <img src="{{ asset('images/presale/presale-logo.png') }}" alt="alt">
                </a>
                <img src="{{ asset('images/presale/logo-btn.png') }}" alt="" class="header-content-label">
            </div>
        </div>
    </div>
</header>
