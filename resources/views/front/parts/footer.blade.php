<footer class="footer" id="footer">
    <div class="row">

        <div class="footer__content">
            <div class="footer__content-item">

                <div class="footer__content-item-btn">
                    <a class="scrollTo" href="{{ (\Route::currentRouteName() != 'home') ? route('home') : '' }}#questionContent">@lang('front.parts.footer.links.faq')</a>
                    <a href="{{ route('info', ['slug' => 'terms']) }}" target="_blank">@lang('front.parts.footer.links.conditions')</a>
                    <a href="{{ route('info', ['slug' => 'privacy']) }}" target="_blank">@lang('front.parts.footer.links.privacy_policy')</a>
                    <a href="{{ route('info', ['slug' => 'disclaimer']) }}" target="_blank">@lang('front.parts.footer.links.copyright')</a>
                </div>

                <p class="footer-copyright">
                    © @lang('front.parts.footer.copyright')<br />
                    created by <a href="https://ukrosoftgroup.com/en" target="_blank">Ukrosoft</a>
                </p>

            </div>

            <div class="footer__content-item">
                <div class="mail">
                    <p>@lang('front.parts.footer.support')</p>
                    <a href="mailto:info@superludi.com">info@superludi.com</a>
                </div>

                <div class="phone">
                    <a href="tel:0800210791">0 800 210 791</a>
                    <p class="phone-number">0 800 210 791</p>
                    <span>@lang('front.parts.footer.free')</span>
                </div>

                <ul class="soc">
                    <li><a href="https://t.me/sl_media" target="_blank">
                            <img src=" {{ asset('images/telegram.svg') }}" alt="alt">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com/superludi.official" target="_blank">
                            <img src=" {{ asset('images/facebook.svg') }}" alt="alt">
                        </a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/superludi.official/" target="_blank">
                            <img src=" {{ asset('images/instagram.svg') }}" alt="alt">
                        </a>
                    </li>
                </ul>

            </div>

            <div class="footer__content-item">
                <p>@lang('front.parts.footer.payment_types')</p>

                <div class="cards">
                    <img src=" {{ asset('images/master.png') }}" alt="alt">
                    <img src=" {{ asset('images/visa.png') }}" alt="alt">
                </div>
            </div>
        </div>

    </div>

    <div class="footer__btn ">
        <button class="btn-down btn-animation" id="footerDown" data-action=scrollTo data-target="#piracy"   >
            <img src=" {{ asset('images/down.png') }}" alt="alt">
        </button>
    </div>
</footer>
