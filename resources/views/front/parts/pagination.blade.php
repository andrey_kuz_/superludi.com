@if ($paginator->hasPages())
    <ul class="pagination text-center" role="navigation" aria-label="Pagination" data-page="6" data-total="16">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class=" disabled" aria-disabled="true" aria-label="@lang('front.pagination.previous')">
                <span aria-hidden="true">@lang('front.pagination.previous')</span>
            </li>
        @else
            <li class=" disabled">
                <a href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('front.pagination.previous')">@lang('front.pagination.previous')</a>
            </li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="disabled" aria-disabled="true"><span>{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="current" aria-current="page"><span>{{ $page }}</span></li>
                    @else
                        <li ><a href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li>
                <a href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('front.pagination.next')">@lang('front.pagination.next')</a>
            </li>
        @else
            <li class="disabled" aria-disabled="true" aria-label="@lang('front.pagination.next')">
                <span aria-hidden="true">@lang('front.pagination.next')</span>
            </li>
        @endif
    </ul>
@endif