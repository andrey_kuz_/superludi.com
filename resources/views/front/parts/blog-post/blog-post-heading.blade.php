<div class="blog-post-heading">
    <img src="{{ $post->hero_image['src'] }}" alt="">
    <h1>{{$post->title}}</h1>
    <aside class="heading-icons">
        <div class="heading-icons-shared">
            <img src="{{ asset('images/shared.png') }}" alt="">
            <span>{{$post->social_count->share_count}}</span>
        </div>
        <div class="heading-icons-viewed">
            <img src="{{ asset('images/viewed.png') }}" alt="">
            <span>{{$post->social_count->view_count}}</span>
        </div>
    </aside>
</div>