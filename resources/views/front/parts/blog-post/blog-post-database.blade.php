<div class="blog-post-database database row">
    <h2>@lang('front.parts.blog-post.blog-post-database.knowledge_base')</h2>
    <div class="database-wrapper">
        @foreach($all_posts->except($post->id)->take(2) as $item)
            <div class="database-item">
                <a href="{{ route('blog-post', ['category_slug' => $item->category->slug, 'post_slug' => $item->slug]) }}" class="database-item-thumbnail">
                    <img src="{{ $item->thumbnail_image['src'] }}" alt="">
                </a>
                <a href="{{ route('blog-post', ['category_slug' => $item->category->slug, 'post_slug' => $item->slug]) }}">
                    <p class="database-item-description">{{$item->title }}</p>
                </a>
            </div>
        @endforeach
            <div class="database-btn">
                <a href="{{ route('blog-main') }}" class="more-materials">
                    <span>@lang('front.parts.blog-post.blog-post-database.look')</span><br>
                    <span>@lang('front.parts.blog-post.blog-post-database.more')</span>
                    <sup>({{ count($all_posts) }})</sup>
                </a>
            </div>

    </div>
</div>