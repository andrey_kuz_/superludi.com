@extends('front.layouts.default')

@section('content')

    @include('front.parts.home.video_trailer')

    @include('front.parts.home.whats_new')

    @include('front.parts.home.info_course')

    @include('front.parts.home.video_courses')

    @include('front.parts.home.course_for')

    @include('front.parts.home.partners')

    @include('front.parts.home.blog')

    @include('front.parts.home.telegram')

    @include('front.parts.home.questions')

    @include('front.parts.home.subscription')

    @include('front.parts.home.knowledge')

@stop

