@extends('front.layouts.default')

@section('content')

    @include('front.parts.course.about_course')

    @include('front.parts.course.more')

    @include('front.parts.course.database')

@stop

