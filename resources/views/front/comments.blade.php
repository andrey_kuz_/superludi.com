@isset($commentable)
    @if($commentable->comments()->published()->count())

        <div class="comments">

            <h4>@lang('front.comments.title')<sup>({{ $commentable->comments()->published()->count() }})</sup></h4>

            @include('front.comment-item', ['comments' => $comments, 'depth' => 0])

            @if( $user )
                <div class="add-new-comment" data-type="{{$commentable->getTableName()}}" data-type-id="{{$commentable->id}}">
                    <textarea></textarea>
                    <input type="button" class="add_new" value="@lang('front.comments.send')"/>
                </div>
            @endif
            @if($commentable->comments()->where('parent_id', 0)->published()->count() > $commentable::VISIBLE_COMMENTS && $count_available_comments!= 0)
                <button class="more-comments"><span>@lang('front.comments.more_comments')</span><sup>({{ $count_available_comments }})</sup></button>
            @endif
        </div>

    @elseif( $user )

        <div class="comments">

            <h4>@lang('front.comments.title')</h4>

            <div class="add-new-comment" data-type="{{$commentable->getTableName()}}" data-type-id="{{$commentable->id}}">
                <textarea></textarea>
                <input type="button" class="add_new" value="@lang('front.comments.send')"/>
            </div>

           {{-- <button class="more-comments"><span>@lang('front.comments.more_comments')</span><sup></sup></button>--}}

        </div>

    @endif
@endisset
