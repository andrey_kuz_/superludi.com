@foreach($comments as $comment)
    <div class="comments-item comments-item-{{ $depth }}"
         data-id="{{ $comment->id }}"
         data-type="{{ $commentable->getTableName() }}"
         data-type-id="{{ $comment->commentable_id }}"
         data-parent-id="{{ $comment->parent_id }}"
    >
        <p class="text">{{ $comment->content }}</p>
        <aside class="info">
            @if($comment->user)
                <a href="#">{{ ($comment->user->info->full_name) ? $comment->user->info->full_name : trans('front.comment-item.anonim') }} </a>
            @else
                <a href="#">{{ trans('front.comment-item.anonim') }}</a>
            @endif
            <span>{{ $comment->created_at }}</span>
            @if( $user )
                <button class="reply">@lang('front.comment-item.reply')</button>
            @endif
        </aside>
        @if( $comment->parent_id == 0 &&  $comment->childrens->count() > 1)
            @if(empty($replace) && $comment->parent_id == 0)
                <button id="show_more_comments" style="color:white; text-align: right; width: 100%" data-type="{{$commentable->getTableName()}}" data-type-id="{{$commentable->id}}">@lang('front.comment-item.more_comments')</button>
            @endif
        @elseif(!empty($comment->childrens()->first())  && $comment->childrens()->first()->hasChildrens())
            @if(empty($replace) && $comment->parent_id == 0)
                <button id="show_more_comments" style="color:white; text-align: right; width: 100%" data-type="{{$commentable->getTableName()}}" data-type-id="{{$commentable->id}}">@lang('front.comment-item.more_comments')</button>
            @endif
        @endif

    </div>
    @if(empty($replace))
        @if( isset($comment->childrens) && $comment->parent_id == 0)
            @include('front.comment-item', ['comments' => $comment->childrens->take(1),'depth' => $depth + 1])
        @endif
    @else
        @if( isset($comment->childrens))
            @include('front.comment-item', ['comments' => $comment->childrens,'depth' => $depth + 1])
        @endif
    @endif
@endforeach
