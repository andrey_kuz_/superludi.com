@extends('front.layouts.default')

@section('content')

    <section class="profile sections">

        <div class="row profile_data">
            <div class="profile__container">

                <div class="profile__container-avatar edit">
                    <img src="{{ ($user->info->avatar['src']) ?  $user->info->avatar['src'] : asset('images/default_avatar.png') }}" alt="avatar">
                    <input type="file" name="avatar" class="input-avatar"/>
                </div>


                <div class="profile__container-info">
                    <div class="info-name">
                        <p id="profileInputName">
                           {{ ($user->info->full_name) ? $user->info->full_name  : trans('front.profile.default_user_name') }}
                        </p>
                        <div class="error-message"></div>
                        <span class="edit"></span>
                    </div>

                    <div class="info-mail">
                        {{ $user->email }}
                    </div>

                    <div class="info-city">
                        <p id="profileInputCity">{{ ($user->info->city) ?  $user->info->city : trans('front.profile.default_user_city') }}</p>
                        <div class="error-message"></div>
                        <span class="edit"></span>
                    </div>

                    <div class="info-setting">
                        <button class="btn-setting">@lang('front.profile.settings')</button>
                    </div>

                    @if(count($active_subscriptions))

                        <div class="info-validity" id="infoValidity">

                            @foreach($active_subscriptions as $item)
                                <div class="info-validity-item">
                                    <div class="info-validity__title">
                                        @lang('front.profile.validity_title')
                                        @foreach ($item->subscription_type->seasons as $season)
                                            <p>{{ $season->title_short }}</p>
                                        @endforeach
                                    </div>
                                    <div class="info-validity__content">

                                        <div class="info-validity__content-deadline">
                                            @lang('front.profile.validity_deadline') <span> {{ $item->expiring_at->format('d.m.Y') }} </span>
                                        </div>

                                        <div class="info-validity__content-btn">
                                            <a href="{{ route('subscription.recurring', $item->id) }}">@lang('front.profile.extend_access')</a>
                                        </div>

                                    </div>
                                </div>

                                <button class="btn-arrow">
                                    <img src="{{ asset('images/arrow-down.png') }}" alt="btn">
                                </button>
                            @endforeach
                        </div>

                    @endif

                </div>

            </div>

            <div class="profile__setting" id="profileSetting">
                <div class="profile__setting-title">
                    <h2>@lang('front.profile.settings')</h2>
                </div>

                <div class="profile__setting-wrap">

                    <div class="col mail">
                        <form class="profile__setting-form" id="edit_user_email" action="{{ route('api.profile.update_email') }}"  target="_blank" novalidate="novalidate">
                            <div class="form__mail">
                                <input type="email" class="email response_email" readonly value="{{ $user->email }}" id="response_user_email">
                                <button type="button" class="changeSmall" id="update_profile_email">@lang('front.profile.change_email')</button>
                            </div>

                            <div class="form__mail-new">
                                <input type="email" class="email" name="email" placeholder="@lang('front.profile.new_email_placeholder')" required="required">
                                <div class="error-message"></div>
                                <input type="email" class="email" name="email_confirm" placeholder="@lang('front.profile.new_email_confirm_placeholder')" required="required">
                                <button class="change" type="submit">@lang('front.profile.change_button')</button>
                            </div>
                        </form>
                    </div>

                    <div class="col password">
                        <form class="profile__setting-form" id="edit_user_password" action="{{ route('api.profile.update_password') }}"  target="_blank" novalidate="novalidate">
                            <div class="form__password">
                                <input type="password" class="password" readonly  value="{{ $user->password }}" id="response_user_password">
                                <button type="button" class="changeSmall" id="update_profile_password">@lang('front.profile.change_password')</button>
                            </div>

                            <div class="form__password-new">
                                <input type="password" class="password" name="password" placeholder="@lang('front.profile.new_password_placeholder')" required="required">
                                <div class="error-message"></div>
                                <input type="password" class="password" name="password_confirm" placeholder="@lang('front.profile.new_password_confirm_placeholder')" required="required">
                                <button class="change" type="submit">@lang('front.profile.change_button')</button>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="profile__setting-close">
                    <button class="btn-close setting-close"></button>
                </div>

            </div>

        </div>

    </section>
    @if(count($active_subscriptions))
        @if(isset($viewed_courses) && count($viewed_courses))

            <section class="viewed section">

                <div class="row">

                    <div class="viewed__title">
                        <h2>@lang('front.profile.viewed_courses')</h2>
                    </div>

                    <div class="viewed__container">

                        @foreach($viewed_courses as $course)

                            <div class="item">
                                <a href="{{ route('course', $course->slug) }}" class="item__link">

                                    <img src=" {{ asset('images/empty.png') }}" alt="alt">

                                    <span class="item__link-img">
                                       <img src="{{ asset($course->preview_image['src']) }}" alt="alt">
                                    </span>

                                    <span class="item__link-top">
                                       <span class="name">{{ $course->speaker ? $course->speaker->name : '' }}</span>
                                       <span class="play">
                                           <img src=" {{ asset('images/play1.png') }}" alt="alt">
                                       </span>
                                    </span>

                                    <span class="item__link-bottom">
                                        {{ $course->title }}
                                    </span>
                                </a>
                            </div>

                        @endforeach

                    </div>

                </div>

            </section>

        @endif

        @if(isset($recommended_courses) && count($recommended_courses))

            <section class="recommended section">

                <div class="row">

                    <div class="recommended__title">
                        <h2>@lang('front.profile.recommended_courses')</h2>
                    </div>

                    <div class="recommended__container">

                        @foreach($recommended_courses as $course)

                            <div class="item">

                                <a href="{{ route('course', $course->slug) }}" class="item__link">

                                    <img src=" {{ asset('images/empty.png') }}" alt="alt">

                                    <span class="item__link-img">
                                        <img src="{{ $course->preview_image['src'] }}" alt="alt">
                                    </span>

                                    <span class="item__link-top">
                                        <span class="name">{{ $course->speaker ? $course->speaker->name : '' }}</span>
                                        <span class="play">
                                            <img src=" {{ asset('images/play1.png') }}" alt="alt">
                                        </span>
                                    </span>

                                    <span class="item__link-bottom">
                                         {{ $course->title }}
                                    </span>
                                </a>
                            </div>

                        @endforeach

                    </div>

                </div>

            </section>
        @endif
    @endif

@stop

