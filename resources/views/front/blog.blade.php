@extends('front.layouts.default')

@section('content')
    @php($classes = ['small', 'large', 'horizontal tiny', 'normal', 'large centered', 'normal', 'small'])
    @include('front.parts.blog.home-tag-cloud')
    @include('front.parts.blog.list-wrapper')
@stop