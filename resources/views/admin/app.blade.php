<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Admin Panel</title>

    <link rel="stylesheet" href="{{ mix('/css/admin.css') }}">

</head>
<body>

    <div id="root"></div>

    <script src="{{ mix('/js/app-admin.js') }}"></script>

</body>
</html>
