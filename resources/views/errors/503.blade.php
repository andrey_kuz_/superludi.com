@extends('front.layouts.default')

@section('content')

    <div class="page404">
        <p>503</p>
        <a href="/"><span>@lang('front.errors.back_home')</span></a>
    </div>

@stop

