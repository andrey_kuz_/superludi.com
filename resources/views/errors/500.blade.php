@extends('front.layouts.default')

@section('content')

    <div class="page404">
        <p>500</p>
        <a href="/"><span>@lang('front.errors.back_home')</span></a>
    </div>

@stop

