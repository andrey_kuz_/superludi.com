$(document).ready(function () {

    let modals = $('[data-reveal][data-show]');

    if(modals.length > 0) {
        modals.foundation('open');
    }

    $('input[data-mask="phone"]').mask("+38(999) 999-99-99", { autoclear: false });

});





