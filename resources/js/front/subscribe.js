import {showLoader, hideLoader} from './loader';
$(document).ready(function(){

    $('#subcribe_unisender').submit(function(e){
        e.preventDefault();

        let rows = $(this).find('input');
        let hasError = false;

        let $elementForLoader = $(this).parent();

        $.each(rows, function (index, row) {

            let error_message = '';

            if (row.type === 'email') {

                if (row.value === '') {
                    error_message = 'Введите email';
                } else if (!isEmail(row.value)) {
                    error_message = 'Некорректный  email.';
                }

                if (error_message) {
                    $(this).siblings('.error-message').show();
                    $(this).siblings('.error-message').text(error_message);
                }

                row.onfocus = function() {
                    $(this).siblings('.error-message').hide();
                    $(this).siblings('.error-message').empty();
                };
            }

            if (row.type === 'checkbox') {
                if (!row.checked) {
                    error_message = 'Для того, чтобы продолжить, Вам нужно принять Условия соглашения.';
                }

                if (error_message) {
                    $(this).siblings('.error-message').show();
                    $(this).siblings('.error-message').text(error_message);
                } else {
                    $(this).siblings('.error-message').hide();
                    $(this).siblings('.error-message').empty();
                }
            }

            if (error_message) {
                hasError = true;
            }
        });
        if(hasError === true) {
            return;
        } else {
            let url = $(this).attr('action');
            let data = $(this).serialize();
            subscribe(url, data, $elementForLoader);
        }

    });

    function subscribe(url, data, $elementForLoader)
    {
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: 'json',
            success: function (response) {
                $('#subcribe_unisender').replaceWith(response);
            },
            error: function(response){
                let errors = response.responseJSON.errors;
                $.each( errors, function( key, value ) {
                    serverErrors(key, value[0]);
                });
            },
            '$elementForLoader': $elementForLoader
        });
    }

    function isEmail(email) {
        let regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

    function serverErrors(field, msg)
    {
        let form = $('#subcribe_unisender');
        let elements = form.find('input');

        $.each( elements, function( key, elem ) {

            if (elem.name === field) {

                $(this).siblings('.error-message').show();
                $(this).siblings('.error-message').text(msg);

                elem.onfocus = function() {
                    $(this).siblings('.error-message').hide();
                    $(this).siblings('.error-message').empty();
                };
            }
        });
    }
});