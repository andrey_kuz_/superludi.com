import {showLoader, hideLoader} from './loader';

$(function () {

    $('#login_exist_email').submit(function (e) {
        e.preventDefault();

        let url = $(this).attr('action');
        let data = $(this).serialize();

        let $elementForLoader = $(this).parent();

        login(url, data, $elementForLoader);
    });


    $('#form_login').submit(function (e) {

        e.preventDefault();

        let $elementForLoader = $(this).parent();

        let rows = $(this).find('input');
        let hasError = false;


        $.each(rows, function (index, row) {
            let error_message = '';

            if (row.type === 'email' || row.type === 'password') {

                error_message = getErrorMessage(row);

                if (error_message) {
                    $(this).next().show();
                    $(this).next().text(error_message);
                }

                row.onfocus = function() {
                    $(this).next().hide();
                    $(this).next().empty();
                };
            }

            if (row.value === '' || error_message) {
                hasError = true;
            }
        });

        if(hasError === true) {
            return;
        } else{
            let url = $(this).attr('action');
            let data = $(this).serialize();
            login(url, data, $elementForLoader);
        }
    });

    function login(url, data, $elementForLoader) {
        showLoader($elementForLoader, true);
        $.post({
            url: url,
            data: data,
            dataType: 'json',
            success: function (response) {
                localStorage.setItem('token', response.token);
                $.ajaxSetup({
                    headers: {
                        'authorization': 'Bearer ' + response.token
                    }
                });
                if (response.redirect_to) {
                    window.location.replace(response.redirect_to);
                } else {
                    window.location.replace(window.location.origin);
                }
            },
            error: function(response){
                hideLoader($elementForLoader);
                let error = response.responseJSON.message;
                serverError(error);
            }
        });
    }

    function getErrorMessage(elem) {

        let msg = '';

        if (elem.name === 'email') {
            if (elem.value === '') {
                msg = 'Введите email';
                return msg;
            } else if (!isEmail(elem.value)) {
                msg = 'Некорректный  e-mail.';
                return msg;
            }
        }

        if (elem.name === 'password') {

            if (elem.value === '') {
                msg = 'Ведите пароль';
                return msg;
            }
        }
    }

    function serverError(message)
    {
        let form = $('.login_form');
        let rows = form.find('input');
        let invalid = form.find('.invalid_data');

        invalid.text(message);
        invalid.show();

        $.each(rows, function (index, row) {
            row.onfocus = function() {
                invalid.hide();
                invalid.empty();
            };
        });
    }

    function isEmail(email) {
        let regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

});
