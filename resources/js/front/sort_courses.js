$(function () {

    let video_trailer_buttons = $('.video_trailer__content').find('.btn-video-live');
    let video_courses_buttons = $('.video_courses__content-btn').find('.btn-curses');

    $('.video_trailer__content .btn-video-live').click(function () {
        let course_id = $(this).attr('id').match(/\d+/)[0];
        // sortCourses(course_id);

        activeCourse(course_id);
    });

    $('.video_courses__content .btn-curses').click(function(){
        let course_id = $(this).attr('id').match(/\d+/)[0];
        // sortCourses(course_id);

        activeCourse(course_id);
    });

    // function sortCourses(course_id)
    // {
    //     let selected_course = $('.video_courses__content-video').find("[data-course-id='" + course_id + "']");
    //     selected_course.detach().prependTo('.video_courses__content-video');
    // }

    function activeCourse(course_id)
    {
        $.each(video_trailer_buttons, function(key, value ) {
            let id = $(this).attr('id').match(/\d+/)[0];
            if (id === course_id) {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }
        });

        $.each(video_courses_buttons, function(key, value ) {
            let id = $(this).attr('id').match(/\d+/)[0];
            if (id === course_id) {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }
        });
    }

});
