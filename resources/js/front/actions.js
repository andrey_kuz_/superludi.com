$(function () {

    // TODO: the whole file needs review

    const $body = $('body');

    $('#footerDown').on('click', function () {
        let $privacy = $('#piracy');

        if ($privacy.length) {
            $privacy.toggleClass('active');

            setTimeout(function () {
                $privacy.get(0).scrollIntoView({block: 'start', behavior: 'smooth'});
            }, 300);
        }

        return false;
    });

    $body.on('click', '[data-action=scrollTo]', function () {
        let $target = $($(this).attr('data-target'));

        if ($target.length) {
            $target.get(0).scrollIntoView({block: 'start', behavior: 'smooth'})
            return false;
        } else {
            return true;
        }
    });

    $body.on('click', '[data-action=openModal]', function () {
        $($(this).attr('data-target')).foundation('open');

        return false;
    });

    $(document).on('closed.zf.reveal', '[data-reveal]', function () {

        let iframe = $(this).find('iframe');
        let videoSrc = iframe.attr('src');

        iframe.attr('src','');
        iframe.attr('src',videoSrc);
    });

    $('#open_register').click(function(){
        $('#modal-login').foundation('close');
        $('#modal-registration').foundation('open');
    });

    $('#open_forgot_password').click(function(){
        $('#modal-login').foundation('close');
        $('#modal-forgot_password').foundation('open');
    });

    $('#open_login').click(function(){
        $('#modal-registration').foundation('close');
        $('#modal-login').foundation('open');
    });

    $('#more_courses').click(function (e) {

        if (!$(this).attr('href').length) {
            e.preventDefault();
            $('#modal-registration').foundation('open');
        }
    });


    $('#aboutVideo .modal-close').click(function(){
        let iframe = $(this).parent().find('iframe');
        let videoSrc = iframe.attr("src");
        iframe.attr("src","");
        iframe.attr("src",videoSrc);
    });

    $('.btn-curses').click(function () {
        let targetId = $(this).attr('data-selected-course');
        $(".video_courses__content-video .item").each(function () {

            if ($(this).attr('data-course-id') === targetId) {
                let elem = $(this)[0];
                let block = $(this).attr('data-scroll-block');
                elem.scrollIntoView({
                    behavior: "smooth",
                    block: block
                });
            }
        });
    });

    $('.more-tags').click(function () {
        $('.tag-list').toggleClass('visible');
    });

    $('.scrollTop').click(function () {
        $("html, body").animate({ scrollTop: 0 }, 1000);
    });

    let courseItemArr = $('.sidebar-list-desktop .course-item');
    let arrowClick = $('#scroll_lesson');
    if (courseItemArr.length > 3) {
        arrowClick.addClass('visible');
        let scroll_counter = 185;
        let scroll_wrapper = $('.sidebar-list-desktop');
        arrowClick.click(function () {
            scroll_wrapper.animate({
                "scrollTop": scroll_wrapper.scrollTop() + scroll_counter
            }, 400);
        })
    } else {
        arrowClick.removeClass('visible');
    }


});
