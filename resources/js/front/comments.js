$(function () {
    var reply_current ;

    // TODO: should be refactored to not use CSS classes

    $('body').on('click', '.reply', function () {
       reply_current = $(this);
       $(this).closest('div').append(
           '<div class="add-child">'+
           '<textarea></textarea>' +
           '<input type="button" class="add_comment" value="Ответить"/>' +
           '<input type="button" class="close_comment" value="Закрыть"/>' +
           '</div>');
       reply_current.hide();
   });
    $('body').on("click", ".add_new", function(e) {
        var comment = $(this).closest('.add-new-comment');
        var content = comment.find('textarea').val();
        var type = comment.attr('data-type');
        var typeId = comment.attr('data-type-id');
        if (content == '') {
            return false;
        } else {
            $.ajax({
                type: "POST",
                url: '/api/front/comment/' + type + '/' + typeId,
                data: {
                    content: content
                },
                success: function success(response) {
                        $('.add-new-comment').before('<p class="info-message">Комментарий добавлен, он будет виден на сайте когда пройдёт модерацию</p>');
                        $('.add-new-comment textarea').hide();
                        $('.add-new-comment').hide();
                        setTimeout(function () {
                            $('.info-message')[0].remove();
                            $('.add-new-comment textarea').val('').show();
                            $('.add-new-comment').show();
                        }, 4000);
                },
                error: function (data) {
                    var response = data.responseJSON;
                    var errorString = '<ul class="error-message">';
                    $.each( response.errors, function( key, value) {
                        errorString += '<li>' + value + '</li>';
                    });
                    errorString += '</ul>';
                    $('.add-new-comment').before(errorString);
                    $('.add-new-comment textarea').hide();
                    $('.add-new-comment').hide();
                    setTimeout(function () {
                        $('.error-message').remove();
                        $('.add-new-comment textarea').val('').show();
                        $('.add-new-comment').show();
                    }, 4000);

                }
            });
        }
    });

    $('body').on("click", ".add_comment", function(e) {
        var comment = $(this).closest('.comments-item');
        var content = comment.find('textarea').val();
        var commentId = comment.attr('data-id');
        var type = comment.attr('data-type');
        var typeId = comment.attr('data-type-id');
        var point = $(this);
        if (content == '') {
            return false;
        } else {
            $.ajax({
                type: "POST",
                url: '/api/front/comment/'  + type + '/' + typeId,
                data: {
                    content: content,
                    parent_id: commentId
                },
                success: function success(response) {
                        point.closest('.comments-item').children('.add-child').before('<p class="info-message">Комментарий добавлен, он будет виден на сайте когда пройдёт модерацию</p>');
                        point.closest('.comments-item').children('.add-child').remove();
                        setTimeout(function () {
                            $('.comments-item').find('.info-message').remove();
                            reply_current.show();
                        }, 4000);

                },
                error: function (data) {
                    var response = data.responseJSON;
                    var errorString = '<ul class="error-message-child">';
                    $.each( response.errors, function( key, value) {
                        errorString += '<li>' + value + '</li>';
                    });
                    errorString += '</ul>';
                    point.closest('.comments-item').children('.add-child').before(errorString);
                    point.closest('.comments-item').children('.add-child').remove();
                    setTimeout(function () {
                        $('.error-message-child').remove();
                        reply_current.show();
                    }, 4000);
                }
            });
        }
    });


    $('body').on('click', '#show_more_comments', function(){

        let model_type = $(this).attr('data-type');
        let model_id = $(this).attr('data-type-id');
        let parent_comment = $(this).parent('.comments-item');
        let comment_id = parent_comment.attr('data-id');
        let url = '/api/front/comment/show_more';
        $.ajax({
            type: "POST",
            url: url,
            data: {type: model_type, id: model_id, comment_id: comment_id},
            success: function (response) {
                if (response.has_child) {
                    parent_comment.next().remove();
                }
                parent_comment.replaceWith(response.comments);
            }
        });
    });

    $('body').on('click', '.more-comments', function () {

        let items = $('.comments-item[data-parent-id="0"]');
        let visible_count = items.length;
        let model_type =items.attr('data-type');
        let model_id = items.attr('data-type-id');
        let url = '/api/front/comment/show_other';
        $.ajax({
            type: "POST",
            url: url,
            data: {type: model_type, id: model_id, offset: visible_count},
            success: function (response) {
                $('.add-new-comment').before(response.comments);
                if (response.count <= 0) {
                    $('.more-comments').remove();
                } else {
                    $('.more-comments').find('sup').text(response.count);
                }
            }
        });
    });

    $('body').on('click', '.close_comment', function(){

        $(this).parent('div.add-child').remove();
        reply_current.show();
    });

});
