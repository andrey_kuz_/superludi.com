import {showLoader, hideLoader} from './loader';

$(function () {

    $('.enter-fb').click(function(e){
        showLoader($(this).parents('.modal-content'), true);
    });

    //login by Facebook ------------------------

    $('#register_by_email').submit(function(e){
        e.preventDefault();

        let $elementForLoader = $(this).parent();

        let rows = $(this).find('input');
        let hasError = false;

        $.each(rows, function (index, row) {

            let error_message = '';

            if (row.type === 'email') {

                error_message = getErrorMessage(row);

                if (error_message) {
                    $(this).next().show();
                    $(this).next().text(error_message);
                }
                row.onfocus = function() {
                    $(this).next().hide();
                    $(this).next().empty();
                };
                if ($(this).val().trim() === '' || error_message) {
                    hasError = true;
                }
            }
        });

        if(hasError === true) {
            return;
        } else{
            let url = $(this).attr('action');
            let data = $(this).serialize();
            let elem = $(this).find('input[name="email"]');

            showLoader($elementForLoader, true);

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                dataType: 'json',
                success: function (response) {
                    loginByFB(response.user, $elementForLoader);
                },
                error: function(response){
                    let errors = response.responseJSON.errors;
                    $.each( errors, function( key, value ) {
                        elem.next().show();
                        elem.next().text(value[0]);
                    });
                }
            });
        }
    });

    function loginByFB(data, $elementForLoader) {
        let url = '/api/auth/login_by_fb';
        $.post({
            url: url,
            data: data,
            dataType: 'json',
            success: function (response) {
                localStorage.setItem('token', response.token);
                $.ajaxSetup({
                    headers: {
                        'authorization': 'Bearer ' + response.token
                    }
                });
                if (response.redirect_to) {
                    window.location.replace(response.redirect_to);
                } else {
                    window.location.replace(window.location.origin);
                }
            },
            error: function(response){
                hideLoader($elementForLoader);
                let errors = response.responseJSON;
            }
        });
    }
    //login by Facebook end------------------------


    $('#form_registration').submit(function (e) {
        e.preventDefault();

        let $elementForLoader = $(this).parent();

        let rows = $(this).find('input');
        let hasError = false;
        // let password = $(this).find('input[name="password"]').val();

        $.each(rows, function (index, row) {
            let error_message = '';

            if (row.type === 'text' || row.type === 'email' || row.type === 'password' || row.type === 'tel') {

                error_message = getErrorMessage(row);
            }

            if (row.type === 'tel') {
                $(this).parent().next().show();
                $(this).parent().next().text(error_message);
            }

            // if (row.name === 'confirm_password') {
            //     let retype = $(this).val();
            //     confirm = checkPassword(password, retype);
            //     if (confirm === false) {
            //         error_message = 'Пароли не совпадают';
            //     }
            // }

            if (error_message) {
                $(this).next().show();
                $(this).next().text(error_message);
            }
            row.onfocus = function() {
                $(this).next().hide();
                $(this).next().empty();
            };

            if (row.value === '' || error_message) {
                hasError = true;
            }
        });

        if(hasError === true) {
            return;
        } else{
            let url = $(this).attr('action');
            let data = $(this).serialize();
            sendData(url, data, $elementForLoader);
        }

    });

    function sendData(url, data, $elementForLoader)
    {
        showLoader($elementForLoader, true);
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: 'json',
            success: function () {
                login(data, $elementForLoader);
            },
            error: function(response){
                hideLoader($elementForLoader);
                let errors = response.responseJSON.errors;
                $.each( errors, function( key, value ) {
                    serverErrors(key, value[0]);
                });
            }
        });
    }

    function login(data, $elementForLoader) {
        let url = '/api/auth/login';
        $.post({
            url: url,
            data: data,
            dataType: 'json',
            success: function (response) {
                localStorage.setItem('token', response.token);
                $.ajaxSetup({
                    headers: {
                        'authorization': 'Bearer ' + response.token
                    }
                });
                if (response.redirect_to) {
                    window.location.replace(response.redirect_to);
                } else {
                    window.location.replace(window.location.origin);
                }
            },
            error: function(response){
                hideLoader($elementForLoader);
                let error = response.responseJSON.message;
            }
        });
    }

    //validate functions--------------------
    function getErrorMessage(elem)
    {
        let msg = '';

        if (elem.name === 'name') {
            if (elem.value === '') {
                msg = 'Введите имя';
                return msg;
            }
        }

        if (elem.name === 'email') {
            if (elem.value === '') {
                msg = 'Введите email';
                return msg;
            } else if (!isEmail(elem.value)) {
                msg = 'Некорректный  e-mail.';
                return msg;
            }
        }

        if (elem.name === 'phone') {
            if (elem.value === '') {
                msg = 'Введите номер телефона';
                return msg;
            } else if (elem.value.length < 12 ) {
                msg = 'Некорректный номер телефона';
                return msg;
            }
        }

        if (elem.name === 'password') {

            if (elem.value === '') {
                msg = 'Ведите пароль';
                return msg;
            } else if (elem.value.length < 8 ) {
                msg = 'Минимум 8 символов';
                return msg;
            }
        }
    }
    // function checkPassword(password, retype) {
    //     let confirm  = true;
    //
    //     if( retype !== password) {
    //         confirm = false;
    //     }
    //     return confirm;
    // }

    function isEmail(email) {
        let regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

    function serverErrors(field, msg)
    {
        let form = $('#form_registration');
        let elements = form.find('input');

        $.each( elements, function( key, elem ) {
            if (elem.name === field) {
                if (elem.type === 'text' || elem.type === 'email' || elem.type === 'password') {
                    $(this).next().show();
                    $(this).next().text(msg);
                }

                if (elem.type === 'tel') {
                    $(this).parent().next().show();
                    $(this).parent().next().text(msg);
                }
            }

            elem.onfocus = function() {
                $(this).next().hide();
                $(this).next().empty();
            };
        });
    }
    //validate functions end--------------------

});
