$(function () {

    $('.accordion .accordion__title').click(function () {
        $(this).toggleClass('active').next('.accordion__content').slideToggle();
    });

    $('.program__content-accordion .btn-more').click(function () {
        $('.program__content-accordion').addClass('viewall');
    });

});
