import {showLoader, hideLoader} from './loader';
$(function () {

    $('#forgot_password').submit(function (e) {
        e.preventDefault();

        let $elementForLoader = $(this).parent();

        let row = $(this).find('input[name="email"]');
        let hasError = validateData(row);
        let form = $(this);

        if(hasError === true) {
            return;
        } else{
            let url = $(this).attr('action');
            let data = $(this).serialize();

            showLoader($elementForLoader, true);

            $.post({
                url: url,
                data: data,
                dataType: 'json',
                success: function (response) {
                    hideLoader($elementForLoader);
                    let message = response.message;
                    form.find('.success-message').text(message);
                    form.find('.success-message').show();
                    form.find('#forgot_password_button').hide();
                    setTimeout(function() { window.location.replace(window.location.origin) }, 2000);
                },
                error: function(response){
                    hideLoader($elementForLoader);
                    let errors = response.responseJSON.errors;
                    $.each( errors, function( key, value ) {
                        row.next().show();
                        row.next().text(value[0]);
                    });
                },
            });
        }
    });


    function validateData(elem)
    {
        let hasError = false;
        let error_message = '';

        if (elem.val().trim() === '') {
            error_message = 'Введите email';
        } else if(!isEmail(elem.val().trim())){
            error_message = 'Некорректный  e-mail.';
        }

        if (error_message) {
            elem.next().show();
            elem.next().text(error_message);
        }

        if (elem.val().trim() === '' || error_message) {
            hasError = true;
        }

        return hasError;
    }

    function isEmail(email) {
        let regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }



});