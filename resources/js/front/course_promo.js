$(function () {

    $('body').on('click', '[data-action=openPromoModal]', function(){

        let promo_link = $(this).attr('data-promo-link');

        if (promo_link) {

            let promo_modal = $('#modal-promo');
            let promo_iframe = promo_modal.find('iframe');

            promo_iframe.attr('src', promo_link);
            promo_modal.foundation('open');
        }
    });

});
