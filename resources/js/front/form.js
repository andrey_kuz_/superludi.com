import 'intl-tel-input/build/js/intlTelInput-jquery';


jQuery(document).ready(function() {

    jQuery(".phone_mask").intlTelInput({

        preferredCountries: ['ua', 'ru', 'by', 'kz'],
        nationalMode: false,
        separateDialCode: false,
        formatOnDisplay: false,
        autoPlaceholder: "polite",
        numberType: "MOBILE"
    });

    function checkLength(input) {
        if(input.intlTelInput("getSelectedCountryData")['iso2'] == 'ua') {
            var text = input.val();
            text = text.substr(0, 13);
            input.val(text);
        }
        if(input.intlTelInput("getSelectedCountryData")['iso2'] == 'ru') {
            var text = input.val();
            text = text.substr(0, 12);
            input.val(text);
        }
        if(input.intlTelInput("getSelectedCountryData")['iso2'] == 'by') {
            var text = input.val();
            text = text.substr(0, 13);
            input.val(text);
        }
        if(input.intlTelInput("getSelectedCountryData")['iso2'] == 'kz') {
            var text = input.val();
            text = text.substr(0, 12);
            input.val(text);
        }

    }

    jQuery("body").on("click touchstart", ".intl-tel-input", function () {
        var target = jQuery(this);
        var input = target.closest(".intl-tel-input.allow-dropdown").find('input');
        checkLength(input);

    });
    jQuery("body").on("keydown", ".intl-tel-input.allow-dropdown input", function (e) {
        if(jQuery(this).val().length == 1 && $(this).val() != '+') {
            jQuery(this).val('+' + $(this).val());
        }

        if((e.keyCode >=48 && e.keyCode <=57)
            || (e.keyCode >=96 && e.keyCode <=105)
            || e.keyCode==8 // backspace
            || (e.keyCode >=37 && e.keyCode <=40)
            || e.keyCode==46 )
        {
            return true;
        } else {
            return false;
        }

    });
    jQuery("body").on("keyup", ".intl-tel-input.allow-dropdown input", function (e) {
        checkLength(jQuery(this));
    });


});