$(function () {

    let isRefreshing = false;
    let jwtToken = $('meta[name="jwt-token"]').attr('content');

    if (jwtToken) {
        localStorage.setItem('token', jwtToken);
    } else {
        jwtToken = localStorage.getItem('token');
    }

    if (jwtToken) {
        $.ajaxSetup({
            headers: {
                'authorization': 'Bearer ' + jwtToken
            },
            statusCode: {
                401: function() {
                    let url = '/api/auth/refresh';
                    if (!isRefreshing) {
                        isRefreshing = true;
                        $.get({
                            url: url,
                            dataType: 'json',
                            success: function (data) {
                                isRefreshing = false;
                                localStorage.setItem('token', data.token);
                                $.ajaxSetup({
                                    headers: {
                                        'authorization': 'Bearer ' + data.token
                                    },
                                });
                            },
                            error: function() {
                                isRefreshing = false;
                                localStorage.removeItem('token');
                                window.location.replace('/');
                            }
                        });
                    }
                }
            }
        });
    }

});
