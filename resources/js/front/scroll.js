import  'nicescroll';

$(function () {

    let scrollCourses = $('.sidebar-list-desktop');

    scrollCourses.niceScroll({
        cursoropacitymin: 1,
        railpadding: { top: 0, right: -10, left: 0, bottom: 0 },
    });

    $(window).on('scroll', function () {
        let $telegram = $('#telegram');
        let $telegramMobile = $('#telegramMobile');

        if ($telegram.length && $telegramMobile.length) {
            let eTop = $telegram.offset().top;
            let startTop = eTop - window.innerHeight;
            let endTop = eTop + window.innerHeight / 10 ;

            $(window).on('scroll', function () {
                let wScrollTop = $(window).scrollTop();
                let offset = -100 * (wScrollTop - startTop) / (endTop - startTop) + 75;

                if (offset <= 0) {
                    $telegramMobile.css({
                        'transform': 'translateY(' + 0 + '%)',
                    });
                } else {
                    $telegramMobile.css({
                        'transform': 'translateY(' + offset * 2 + '%)',
                    });
                }
            });
        }

        var w_top = $(window).scrollTop();
        var e_top = $('#footer');

        var w_height = $(window).height();
        var d_height = $(document).height();

        var e_height = $('#footer').outerHeight();

        let $down = $('#footerDown');

        if (w_top + 200 >= e_top || w_height + w_top == d_height || e_height + e_top < w_height){
            $down.css({
                'visibility': 'visible',
            });
        }

        var target = $('.btn-down ');
        var targetPos = target.offset().top;
        var winHeight = $(window).height();
        var scrollToElem = targetPos - winHeight;
        $(window).scroll(function(){
            var winScrollTop = $(this).scrollTop();
            if(winScrollTop > scrollToElem){
                $down.css({
                    'visibility': 'visible',
                });
            }
        });

    });

    let $knowledge = $('.knowledge__content');
    let $milk = $('#milk');

    if ($knowledge.length && $milk.length) {

        $(window).on('scroll', function () {
            let eTop = $knowledge.offset().top;
            let startTop = eTop - window.innerHeight;
            let endTop = eTop + $knowledge.height();

            let wScrollTop = $(window).scrollTop();
            let angleRotate = 30 * (wScrollTop - startTop) / (endTop - startTop) - 15;

            $milk.css({
                'transform': 'rotate(' + angleRotate + 'deg)'
            });
        });
    }

    let hash = window.location.hash;

    if (hash === '#questionContent') {
        if($(hash).length > 0){
            let noHashURL = window.location.href.replace(hash, "");
            window.history.replaceState('', document.title, noHashURL);
            let elementClick = $(hash);
            let destination = $(elementClick).offset().top -$(window).height()/3;
            if ( /^((?!chrome|android).)*safari/i.test(navigator.userAgent)) {
                $('body').animate({ scrollTop: destination }, 1100); //1100 - скорость
            }
            else {
                $('html').animate({ scrollTop: destination }, 1100);
            }
        }
    }

    let speakersPos = $('#videoCursesContent');

    if (speakersPos.length > 0) {

        speakersPos = speakersPos.offset().top;

        $(window).scroll(function() {

            if ($(window).scrollTop() >= speakersPos + 300) {
                $('.scrollTop').addClass('visible');
            } else {
                $('.scrollTop').removeClass('visible');
            }
        });
    }


    $(".scrollTo").click(function (e) {

        var elementClick = $(this).attr("href");
        var destination = $(elementClick).offset().top -$(window).height()/3;
        if ( /^((?!chrome|android).)*safari/i.test(navigator.userAgent)) {
            $('body').animate({ scrollTop: destination }, 1100); //1100 - скорость
        }
        else {
            $('html').animate({ scrollTop: destination }, 1100);
        }
        return false;
        e.preventDefault();
    });

    let payment_link = $('.payment_link');

    $(window).scroll(function() {
        if ($(window).scrollTop() >= $(window).height()*0.45) {
            payment_link.addClass('visible__btn');
        } else {
            payment_link.removeClass('visible__btn');
        }
    });
});



