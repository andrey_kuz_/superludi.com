import {hideLoader, showLoader} from "./loader";

$(function () {

    $('#presale_button').click(function (e) {
        e.preventDefault();

        let $elementForLoader = $(this).parent();
        let form = $('#presale_form');
        let rows = form.find('input');
        let hasError = false;

        $.each(rows, function (index, row) {
            let error_message = '';

            if (row.type === 'text' || row.type === 'email' || row.type === 'tel') {

                error_message = getErrorMessage(row);
            }

            if (row.type === 'tel') {
                $(this).parent().next().show();
                $(this).parent().next().text(error_message);
            }

            if (error_message) {
                $(this).next().show();
                $(this).next().text(error_message);
            }
            row.onfocus = function() {
                $(this).next().hide();
                $(this).next().empty();
            };

            if (error_message) {
                hasError = true;
            }
        });

        if(hasError === true) {
            return;
        } else{
            form.submit();
            showLoader($elementForLoader, true);
        }

    });

    function getErrorMessage(elem)
    {
        let msg = '';

        if (elem.name === 'name') {
            if (elem.value === '') {
                msg = 'Введите имя';
                return msg;
            }
        }

        if (elem.name === 'email') {
            if (elem.value === '') {
                msg = 'Введите email';
                return msg;
            } else if (!isEmail(elem.value)) {
                msg = 'Некорректный  e-mail.';
                return msg;
            }
        }

        if (elem.name === 'phone') {
            if (elem.value === '') {
                msg = 'Введите номер телефона';
                return msg;
            } else if (elem.value.length < 12 ) {
                msg = 'Некорректный номер телефона';
                return msg;
            }
        }
    }

    function isEmail(email) {
        let regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

});