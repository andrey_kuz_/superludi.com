import * as Rellax from 'rellax';

$(function () {

    let rellaxClass = $('.rellax');
    let rellax;
    let isDestroy = true;


    if (rellaxClass.length > 0) {

        $(window).resize(function () {
            if ($(window).width() <= 1024) {
                if (!isDestroy) {
                    rellax.destroy();
                    rellax = undefined;
                    isDestroy = true;
                }
            } else {
                if (isDestroy) {
                    rellax = new Rellax('.rellax');
                    isDestroy = false;
                }
            }
        });

        $(window).trigger('resize');

    };


});
