import 'slick-carousel';

$(function () {

    $('.slider').slick({
        'arrows': true,
        'dots': true,
        'slidesToShow': 1,
        'speed': 600,
        'infinite': true,
        'swipeToSlide': true

    });
});
