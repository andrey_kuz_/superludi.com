$(function () {

    $('#show_more_posts').click(function () {
        let url = '/api/front/blog/show_more';
        let items = $(this).parents('.post-list-wrapper').find('.post_item');
        let count_items = items.length;
        let show_more_button = $(this);

        $.ajax({
            type: "POST",
            url: url,
            data: {count: count_items},
            success: function (response) {
                show_more_button.before(response.posts);
                if(response.count === 0) {
                    show_more_button.remove();
                } else {
                    show_more_button.find('sup').text(response.count);
                }
            }
        });
    });

});