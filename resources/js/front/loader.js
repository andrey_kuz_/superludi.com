$(function () {
    module.exports.getLoader = getLoader;
    module.exports.showLoader = showLoader;
    module.exports.hideLoader = hideLoader;

    function getLoader(darkLoader, fixed=false) {
        return '<div class="loader ' + (darkLoader === true ? ' dark' : '') + (fixed === true ? ' fixed' : '') + '" style="z-index: 10000">' +
                    '<svg class="circular" viewBox="25 25 50 50">\n' +
                        '<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>\n' +
                    '</svg>' +
                '</div>';
    }

    function showLoader($el, darkLoader, fixed=false) {
        let loader = getLoader(darkLoader, fixed);

        $el.append(loader);
        $el.css('position', 'relative');
        $el.find('input[type="submit"], input[type="button"],textarea, input,  button').prop('disabled', true);

    }

    function hideLoader($el) {
        let $loader = $el.find('.loader');


        $el.find('input[type="submit"], input[type="button"],textarea, input, button').prop('disabled', false);
        $loader.remove();
    }

    /*function strncmp(str1, str2, n) {
        n = n || Math.min(str1.length, str2.length);

        return str1.substring(0, n) === str2.substring(0, n);
    }*/

    function handlerLoader(callback, settings, darkLoader) {
        if ('$elementForLoader' in settings) {
            let requests = [/\/api\//];
            //let requests = ['/api/'];
            let isLoader = false;

            requests.forEach(function (request) {
                if (isLoader) {
                    return;
                }

                isLoader = request.test(settings.url);
                //isLoader = strncmp(settings.url, request);
            });

            if (isLoader) {
                callback(settings.$elementForLoader, darkLoader);
            }
        }
    }

    $(document).bind('ajaxSend', function (e, xhr, settings) {
        // console.log('ajaxSend(): ', settings);

        handlerLoader(showLoader, settings, true);
    });

    $(document).bind('ajaxComplete', function (e, xhr, settings) {
        // console.log('ajaxComplete(): ', settings);

        handlerLoader(hideLoader, settings);
    });
});
