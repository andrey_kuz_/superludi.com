import {showLoader, hideLoader} from './loader';

$(document).ready(function(){

    $('#logout').click(function () {

        showLoader($('body'), true, true);

        let url = '/api/auth/logout';
        $.post({
            url: url,
            dataType: 'json',
            success: function () {
                localStorage.removeItem('token');
                window.location.replace('/');
            }
        });
    });
});
