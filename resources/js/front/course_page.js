$(function () {

    // TODO: should be refactored to not use CSS classes

    let current_lesson_id = $('.current_video_player').attr('id');
    let base_url = '/api/front/course_lesson/';
    let jwtToken = localStorage.getItem('token');

    if (current_lesson_id) {
        if (jwtToken) {
            $.ajaxSetup({
                headers: {
                    'authorization': 'Bearer ' + jwtToken
                }
            });
            getStartLessonTime(current_lesson_id);
        }
    }

    $('.course_lesson').click(function(){
        
        if ($(this).parents('.about-main-sidebar').hasClass('about-main-sidebar-mobile')) {
            let destination = $('#show_seasons').offset().top;
            $('html').animate({ scrollTop: destination }, 1100);
            setTimeout(function(){
                $('.about-main-sidebar-mobile').removeClass('open');
            }, 1000);
        }

        let lesson_id = $(this).attr('id');
        let url = base_url + lesson_id;

        if (!$(this).attr('data-blocked')) {
            $.ajax({
                type: "GET",
                url: url,
                dataType: 'json',
                success: function (response) {
                    let course_content = $('.about-main-content.course_content');
                    let lesson_content = $('.about-main-content.lesson_content');
                    if (response !== 'access_false') {
                        if (course_content.length) {
                            lesson_content.show();
                            course_content.replaceWith(lesson_content);
                        }
                        $('.comments').replaceWith(response.comments);
                        replaceLessonData(response.lesson, response.course_materials, response.lesson_materials);
                    }
                },
                '$elementForLoader': $(this)
            });
        }
    });

    $('.current-video-content .play-button').click(function(){
        $('.current-video-content').hide();
        $('.current_lesson_preview').hide();
        $('.current-video img.gag').hide();
        $('.current_video_player').show();
    });

    $('body').on('click', '#show_lessons', function () {
        $('.about-main-sidebar-mobile').toggleClass('open');
    });

    function replaceLessonData(current_lesson, course_materials, lesson_materials) {

        if (jwtToken) {
            getStartLessonTime(current_lesson.id);
        }

        $('.current_video_player').attr('id', current_lesson.id);

        $('.current-video-content').show();
        $('.current_lesson_preview').show();
        $('.current-video img.gag').show();
        $('.current_video_player').hide();

        $('.current_lesson_title').html('Урок '+current_lesson.ord+': '+current_lesson.title);
        $('.current_lesson_description').html(current_lesson.description);
        if (current_lesson.preview_image) {
            $('.current_lesson_preview').attr('src', current_lesson.preview_image['src']);
        }
        $('.lesson_content .current-video iframe').attr('src', current_lesson.video_link);
        $('[data-materials=course] .materials-item').remove();
        $('[data-materials=lesson] .materials-item').remove();

        if (course_materials.length) {

            $.each(course_materials, function(key, value ) {
                let file_src = value.file['src'];
                let file_size = value.size;
                if (file_size) {
                    $('[data-materials=course]').append(
                        "<div class='materials-item'>" +
                        "<img src='/images/pdf.png' alt='alt'>" +
                        "<a href='" + file_src + "' target='_blank'>" + value.title + "</a>" +
                        "<sup>" + value.size + "MB</sup>" +
                        "</div>"
                    );
                }
            });

            $('[data-materials=course]').show();

        } else {

            $('[data-materials=course]').hide();
        }

        if (lesson_materials.length) {

            $.each(lesson_materials, function(key, value ) {
                let file_src = value.file['src'];
                let file_size = value.size;
                if (file_size) {
                    $('[data-materials=lesson]').append(
                        "<div class='materials-item'>" +
                        "<img src='/images/pdf.png' alt='alt'>" +
                        "<a href='" + file_src + "' target='_blank'>" + value.title + "</a>" +
                        "<sup>" + value.size + "MB</sup>" +
                        "</div>"
                    );
                }
            });

            $('[data-materials=lesson]').show();

        } else {

            $('[data-materials=lesson]').hide();
        }
    }

    window._wq = window._wq || [];

    _wq.push({ id: "_all", onReady: function(video) {

            if (jwtToken) {
                // video.bind("pause", function () {
                //     let lesson_id = $('.lesson_content .current_video_player').attr('id');
                //     let time = Math.round(video.time());
                //     if (lesson_id) {
                //         saveTime(time, lesson_id);
                //         getStartLessonTime(lesson_id);
                //     }
                // });
                video.bind("timechange", function(t) {
                    let lesson_id = $('.lesson_content .current_video_player').attr('id');
                    let time = Math.round(t);
                    if (lesson_id) {
                        $('.lesson_content .current_video_player').attr('data-start-time', time);
                        if (video.state() === "paused") {
                            saveTime(time, lesson_id);
                        }
                        if (video.state() === "ended") {
                            $('.lesson_content .current_video_player').attr('data-start-time', 0);
                            saveTime(0, lesson_id);
                        }
                    }
                });
            }
            video.bind("play", function() {
                let time = $('.lesson_content .current_video_player').attr('data-start-time');
                video.time(time);
            });
        }});

    function saveTime(time, lesson_id)
    {
        let url = base_url + lesson_id + '/state';
        $.ajax({
            type: "POST",
            url: url,
            data: {time: time},
            dataType: 'json',
            success: function (response) {

            }
        });
    }

    function getStartLessonTime(lesson_id)
    {
        let url = base_url + lesson_id + '/state';
        $.ajax({
            type: "GET",
            url: url,
            dataType: 'json',
            success: function (start_time) {
                $('.lesson_content .current_video_player').attr('data-start-time', start_time);
            }
        });
    }

});
