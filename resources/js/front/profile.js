$(document).ready(function () {

    let base_url = '/api/front/profile';

    //actions ------------------------
    $('#update_profile_email').on('click', function () {
        $('.form__mail-new').toggleClass('open');
    });

    $('#update_profile_password').on('click', function () {
        $('.form__password-new').toggleClass('open');
    });

    $('.profile__setting .setting-close ').on('click', function () {
        $('.form__mail-new').removeClass('open');
        $('.form__password-new').removeClass('open');
    });

    $('#infoValidity').on('click', function () {
        $(this).toggleClass('open');
    });

    $('.btn-setting, .setting-close').on('click', function () {
        $('#profileSetting').toggleClass('open');
    });

    $('.header__content-profile').on('click', function () {
        $('.header__content-profile').toggleClass('active');

    });
    $('.header__content-profile-btn a, .header__content-profile-btn button').on('click', function () {
        $('.header__content-profile').removeClass('active');

    });

    $(document).mouseup(function (e){

        let elem = $('.header__content-profile');

        if (!elem.is(e.target) && elem.has(e.target).length === 0) {
            if (elem.hasClass('active')) {
                elem.removeClass('active');
            }
        }
    });


    $('.info-name .edit').click(function(){

        if ($('.info-name p').length > 0) {
            let elem = $('#profileInputName');
            let val = elem.text().trim();
            elem.replaceWith('<input id="profileInputName" type="text" name="full_name" value="' + val + '">');
        }
    });

    $('.info-city .edit').click(function () {
        if ($('.info-city p').length > 0) {
            let elem = $('#profileInputCity');
            let val = elem.text().trim();

            elem.replaceWith('<input id="profileInputCity" type="text" name="city" value="' + val + '">');
        }
    });
    //actions end ------------------------


    //update email ------------------------
    $('#edit_user_email').submit(function (e) {
        e.preventDefault();

        let email = $(this).find('.form__mail-new input[name="email"]');
        let email_confirm = $(this).find('.form__mail-new input[name="email_confirm"]');
        let error_message = '';

        if (email.val().trim() === '') {
            error_message = 'Введите email';
        } else if (!isEmail(email.val())) {
            error_message = 'Некорректный email';
        } else if( email.val() !== email_confirm.val() ) {
            error_message = 'Почта не совпадает';
        }

        onFocus(email);

        if (error_message) {
            showErrors(email, error_message);
            return;
        } else {
            let url = $(this).attr('action');
            let data = $(this).serialize();

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                dataType: 'json',
                success: function (response) {
                    hideErrors(email);
                    email.parent().hide();
                    $('.form__mail #response_user_email').val(response);
                    $('.info-mail').text(response);
                },
                error: function(response){
                    let errors = response.responseJSON.errors;
                    $.each( errors, function( key, value ) {
                        serverErrors(key, value[0]);
                    });
                },
                '$elementForLoader': $(this).parent()
            });
        }
    });
    //update email end------------------------


    //update password ------------------------
    $('#edit_user_password').submit(function (e) {
        e.preventDefault();

        let password = $(this).find('.form__password-new input[name="password"]');
        let password_confirm = $(this).find('.form__password-new input[name="password_confirm"]');
        let error_message = '';

        if (password.val().trim() === '') {
            error_message = 'Введите пароль';
        } else if (password.val().length < 8) {
            error_message = 'Минимум 8 символов';
        } else if( password.val() !== password_confirm.val() ) {
            error_message = 'Новые пароли не совпадают';
        }

        onFocus(password);

        if (error_message) {
            showErrors(password, error_message);
            return;
        } else {
            let url = $(this).attr('action');
            let data = $(this).serialize();

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                dataType: 'json',
                success: function (response) {
                    hideErrors(password);
                    password.parent().hide();
                    $('.form__password #response_user_password').val(response);
                },
                error: function(response){
                    let errors = response.responseJSON.errors;
                    $.each( errors, function( key, value ) {
                        serverErrors(key, value[0]);
                    });
                },
                '$elementForLoader': $(this).parent()
            });
        }
    });
    //update password end------------------------


    //update name ------------------------
    $('body').on('keydown', '.profile__container input[name="full_name"]', function (e) {

        if(e.keyCode === 13) {
            let full_name = $(this).val().trim();
            validateName($(this), full_name);
            onFocus($(this));
        }
    });

    $('body').on('focusout', '.profile__container input[name="full_name"]', function () {

        let full_name = $(this).val().trim();
        validateName($(this), full_name);
        onFocus($(this));
    });

    function updateName(elem, data)
    {
        let url = base_url + '/update_name';

        $.ajax({
            type: "POST",
            url: url,
            data: {full_name: data},
            dataType: 'json',
            success: function (response) {
                hideErrors(elem);
                elem.replaceWith('<p id="profileInputName">' + response + '</p>');
            },
            error: function(response){
                let errors = response.responseJSON.errors;
                $.each( errors, function( key, value ) {
                    serverErrors(key, value[0]);
                });
            }
        });
    }
    //update name end------------------------


    //update city ------------------------
    $('body').on('keydown', '.profile__container input[name="city"]', function (e) {

        if(e.keyCode === 13) {
            let city = $(this).val().trim();
            validateCity($(this), city);
            onFocus($(this));
        }
    });

    $('body').on('focusout', '.profile__container input[name="city"]', function () {

        let city = $(this).val().trim();
        validateCity($(this), city);
        onFocus($(this));
    });

    function updateCity(elem, data)
    {
        let url = base_url + '/update_city';

        $.ajax({
            type: "POST",
            url: url,
            data: {city: data},
            dataType: 'json',
            success: function (response) {
                hideErrors(elem);
                elem.replaceWith('<p id="profileInputCity">' + response + '</p>');
            },
            error: function(response){
                let errors = response.responseJSON.errors;
                $.each( errors, function( key, value ) {
                    serverErrors(key, value[0]);
                });
            }
        });
    }
    //update city end------------------------


    //update avatar------------------------
    $('.profile__container-avatar input[name="avatar"]').on('change', function(){

        let form_data = new FormData();
        let file_data = this.files[0];
        form_data.append("avatar[]", file_data);

        let url = base_url + '/update_avatar';

        $.ajax({
            type: "POST",
            url: url,
            data: form_data,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                $('.profile__container-avatar img').attr('src', response);
            },
            error: function(response){
            }
        });
    });
    //update avatar end------------------------


    //secondary functions--------------------
    function isEmail(email) {
        let regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

    function serverErrors(field, msg)
    {
        let elem = $('.profile_data').find('input[name="' + field + '"]');
        elem.next().show();
        elem.next().text(msg);
        onFocus(elem);
    }

    function onFocus(elem)
    {
        elem.focus(function() {
            $(this).next().hide();
            $(this).next().empty();
        });
    }

    function showErrors(elem, msg)
    {
        elem.next().show();
        elem.next().text(msg);
    }

    function hideErrors(elem)
    {
        elem.next().hide();
        elem.next().empty();
    }

    function validateName(elem, data)
    {
        let error_message = '';

        if (data === '') {
            error_message = 'Введите имя';
        }

        if (error_message) {
            showErrors(elem, error_message);
        } else {
            updateName(elem, data);
        }
    }

    function validateCity(elem, data)
    {
        let error_message = '';

        if (data === '') {
            error_message = 'Введите город';
        }

        if (error_message) {
            showErrors(elem, error_message);
        } else {
            updateCity(elem, data);
        }
    }
    //secondary functions end--------------------






});