import russianMessages from 'ra-language-russian';
import { mergeTranslations } from 'react-admin';

export const messages = {
    ...mergeTranslations(russianMessages),
    general: {
        list: {
            search: 'Поиск',
        },
        action: {
            save: 'Сохранить',
            close: 'Закрыть',
            resetViews: 'Обновить',
        },
    },
    locales: {
        form: {
            ru: 'Русский',
            uk: 'Украинский'
        },
    },
    resources: {
        user: {
            name: 'Пользователь |||| Пользователи',
            fields: {
                first_name: 'Имя',
                last_name: 'Фамилия',
                role: 'Роль',
                password: 'Пароль',
                password_confirm: 'Подтвердить пароль'
            },
        },
        info: {
            name: 'Информация |||| Информация',
            title: 'Информация',
            fields: {
                slug: 'Slug',
                title: 'Название',
                description: 'Описание',
                keywords: 'Keywords',
                content: 'Контент'
            }
        },
        comment: {
            name: 'Комментарий |||| Комментарии',
            title: 'Комментарий',
            fields: {
                content: 'Контент',
                is_published: 'Опубликован'
            }
        },
        course: {
            name: 'Курс |||| Курсы',
            fields: {
                slug: 'Slug',
                title: 'Название',
                speaker_name: 'Cпикер',
                price: 'Цена',
                description: 'Описание',
                is_published: 'Опубликован',
                banner_image: 'Изображение баннера',
                preview_image: 'Превью курса',
                banner_video_code: 'Код видео баннера',
                teaser_video_code: 'Код видео тизера',
                keywords: 'Keywords',
                season_id: 'Сезон',
                speaker_id: 'Спикер'
            },
        },
        course_lesson: {
            name: 'Урок |||| Уроки',
            fields: {
                course_id: 'Курс',
                title: 'Название',
                description: 'Описание',
                video_code: 'Код видео-ролика',
                preview_image: 'Изображение урока',
                ord: 'Порядок сортировки'
            },
        },
        course_lesson_material: {
            name: 'Материал |||| Материалы',
            fields: {
                course_id: 'Курс',
                course_lesson_id: 'Урок',
                title: 'Название',
                file: 'Файл'
            },
        },
        blog_category: {
            name: 'Категория блога |||| Категории блога',
            fields: {
                slug: 'Slug',
                title: 'Заголовок',
                description: 'Описание',
                keywords: 'Keywords'
            },
        },
        blog_post: {
            name: 'Пост блога |||| Посты блога',
            fields: {
                slug: 'Slug',
                title: 'Название статьи',
                author: 'Автор статьи',
                description: 'Описание',
                image_thumbnail: 'Заглавное изображение',
                hero_image: 'Большое изображение',
                html: 'Контент',
                category: 'Категория',
                is_draft: 'Сохранить как черновик',
                published_at: 'Время создания',
                content: 'Контент',
                tags: 'Tags',
                keywords: 'Keywords'
            },
        },
        blog_tag: {
            name: 'Тег |||| Теги',
            fields: {
                slug: 'Slug',
                title: 'Название Тега',
                post: 'Посты блога',
                description: 'Описание',
                keywords: 'Keywords'
            },
        },
        mail_template: {
            name: 'Шаблон письма |||| Шаблоны писем',
            fields: {
                name: 'Название',
                subject: 'Тема',
                content: 'Содержание',
            },
        },
        payment_type: {
            name: 'Cпособ оплаты |||| Cпособы оплаты',
            fields: {
                name: 'Cпособ',
                title: 'Название',
            },
        },
        payment_status: {
            name: 'Статус оплаты |||| Статусы оплаты',
            fields: {
                name: 'Статус',
                title: 'Название',
            },
        },
        payment: {
            name: 'Платеж |||| Платежи',
            fields: {
                user_id: 'Пользователь',
                type_id: 'Тип оплаты',
                status_id: 'Статус оплаты',
                paid_at: 'Дата платежа',
                comment: 'Комментарий'
            },
        },
        season: {
            name: 'Сезон |||| Сезоны',
            fields: {
                title: 'Название',
                title_short: 'Сокращенное название',
                subscription_types: 'Типы подписок'
            },
        },
        speaker: {
            name: 'Спикер |||| Спикеры',
            fields: {
                name: 'Имя',
                description: 'Описание',
                position:'Должность',
                avatar: 'Аватар'
            },
        },
        subscription_type: {
            name: 'Тип подписки |||| Типы подписок',
            fields: {
                name: 'Тип',
                period: 'Период',
                price: 'Стоимость',
                sale_price: 'Цена со скидкой',
                title: 'Название',
                order_desc: 'Наименование заказа',
                seasons: 'Сезоны',
                start_at: 'Начало действия подписки',
                end_at: 'Конец действия подписки',
                active_at: 'Дата активации подписки'
            },
        },
        subscription: {
            name: 'Управление подпиской |||| Управление подписками',
            fields: {
                user: 'Пользователь',
                type: 'Тип',
                is_active: 'Активирован',
                expiring_at: 'Дата завершения',
                expiring_at_start: 'Дата завершения от',
                expiring_at_end: 'Дата завершения до',
                price: 'Стоимость',
                title: 'Название',
                seasons: 'Сезоны',
                start_at: 'Начало действия подписки',
                end_at: 'Конец действия подписки'
            },
        },
    },
    blog_tag: {
        edit: {
            title: 'Тег "%{title}"',
        },
    },
    user: {
        form: {
            summary: 'Общая информация',
            security: 'Безопасность',
            profile: 'Профиль',
        },
        edit: {
            title: 'Пользователь "%{title}"',
        },
    },
    comment: {
        form: {
            general: 'Информация о комментарии',
        },
    },
    course: {
        form: {
            general: 'Общие настройки',
            media: 'Медиа-файлы',
        },
        edit: {
            title: 'Курс "%{title}"',
        },
    },
    course_lesson: {
        form: {
            general: 'Общие настройки',
            media: 'Медиа-файлы',
        },
        edit: {
            title: 'Урок "%{title}"',
        },
    },
    course_lesson_material: {
        form: {
            general: 'Общие настройки',
            media: 'Медиа-файлы',
        },
        edit: {
            title: 'Материал "%{title}"',
        },
    },
    mail_template: {
        form: {
            general: 'Общие настройки',
        },
        edit: {
            name: 'Шаблон "%{name}"',
        },
    },
    payment_type: {
        form: {
            general: 'Общие настройки',
        },
        edit: {
            name: 'Способ оплаты "%{name}"',
        },
    },
    payment_status: {
        form: {
            general: 'Общие настройки',
        },
        edit: {
            name: 'Статус оплаты "%{name}"',
        },
    },
    payment: {
        show: {
            id: 'Платеж "%{id}"',
        }
    },
    season: {
        form: {
            general: 'Общие настройки',
        },
        edit: {
            title: 'Сезон "%{title}"',
        },
    },
    speaker: {
        form: {
            general: 'Общие настройки',
            media: 'Медиа-файлы',
        },
        edit: {
            name: 'Спикер "%{name}"',
        },
    },
    subscription_type: {
        form: {
            general: 'Общие настройки',
        },
        edit: {
            name: 'Тип "%{name}"',
        },
    },
    subscription: {
        form: {
            general: 'Общие настройки',
        },
        edit: {
            name: 'Подписка с id %{name}',
        },
    },
};

export default messages;
