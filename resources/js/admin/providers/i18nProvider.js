import russianMessages from './../i18n/ru';

// const messages = {
//     ru: () => import(/* webpackChunkName:"i18n/ru" */ './../i18n/ru.js').then(messages => messages.default),
// };

export default locale => {

    // if (locale === 'ru') {
    //     return messages[locale]();
    // }

    // Always fallback on english
    return russianMessages;
};
