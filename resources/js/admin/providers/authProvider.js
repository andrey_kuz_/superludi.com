import {
    AUTH_GET_PERMISSIONS,
    AUTH_LOGIN,
    AUTH_LOGOUT,
    AUTH_ERROR,
    AUTH_CHECK,
} from 'react-admin'; // eslint-disable-line import/no-unresolved

// Authenticatd by default
export default (type, params) => {
    if (type === AUTH_LOGIN) {
        const { username, password } = params;
        const request = new Request('/api/auth/login', {
            method: 'POST',
            body: JSON.stringify({ email: username, password: password }),
            headers: new Headers({
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }),
        });
        return fetch(request)
            .then(response => {
                if (response.status < 200 || response.status >= 300) {
                    throw new Error(response.statusText);
                }
                return response.json();
            })
            .then(({ token, permissions }) => {
                localStorage.setItem('token', token);
                localStorage.setItem('permissions', JSON.stringify(permissions));
            });
    }
    if (type === AUTH_LOGOUT) {
        const token = localStorage.getItem('token');
        const request = new Request('/api/auth/logout', {
            method: 'POST',
            headers: new Headers({
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }),
        });
        return fetch(request)
            .then(response => {
                if (response.status < 200 || response.status >= 300) {
                    throw new Error(response.statusText);
                }
                return response.json();
            })
            .then(({ token, permissions }) => {
                localStorage.removeItem('token');
                localStorage.removeItem('permissions');
            })
            .catch(() => {
                localStorage.removeItem('token');
                localStorage.removeItem('permissions');
            })
    }
    if (type === AUTH_ERROR) {
        const { status } = params;
        return status === 400 || status === 401 || status === 403
            ? Promise.reject()
            : Promise.resolve();
    }
    if (type === AUTH_CHECK) {
        return localStorage.getItem('token')
            ? Promise.resolve()
            : Promise.reject();
    }
    if (type === AUTH_GET_PERMISSIONS) {
        const permissions = JSON.parse(localStorage.getItem('permissions'));
        return Promise.resolve(permissions);
    }

    return Promise.reject('Unknown method');
};
