import { fetchUtils } from 'react-admin';
import simpleRestProvider from 'ra-data-simple-rest';
import dataProviderUploads from './dataProviderUploads';

const httpClient = (url, options = {}) => {
    if (!options.headers) {
        options.headers = new Headers({ Accept: 'application/json' });
    }
    const token = localStorage.getItem('token');
    options.headers.set('Authorization', `Bearer ${token}`);
    return fetchUtils.fetchJson(url, options);
};

const restDataProvider = simpleRestProvider('/api/admin', httpClient);
const dataProvider = dataProviderUploads(restDataProvider);

export default dataProvider;
