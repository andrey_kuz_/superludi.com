const convertFileToBase64 = file =>
    new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file.rawFile);

        reader.onload = () => resolve(reader.result);
        reader.onerror = reject;
    });

const dataProviderUploads = requestHandler => (type, resource, params) => {
    switch (type) {
        case 'CREATE':
        case 'UPDATE':
            let toUpload = [];

            Object.keys(params.data).forEach(key => {

                let value = params.data[key];

                if (value && value.rawFile instanceof File) {
                    toUpload.push({
                        ...value,
                        key: key,
                        multiple: false
                    });
                }
            });

            if (toUpload.length) {

                return Promise.all(toUpload.map(convertFileToBase64))
                    .then(base64Files =>
                        base64Files.map((file64, i) => ({
                            ...toUpload[i],
                            data: file64,
                        }))
                    )
                    .then(transformedNewFiles => {
                        transformedNewFiles.map(file => {
                            params.data[file.key] = file;
                        });
                        return requestHandler(type, resource, params);
                    });
            }

            break;
        default:
            break;
    }

    return requestHandler(type, resource, params);
};

export default dataProviderUploads;
