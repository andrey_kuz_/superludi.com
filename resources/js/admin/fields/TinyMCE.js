import React from "react";
import {
    Field
} from "redux-form";
import {
    TextInput,
    FieldTitle
} from "react-admin"; // eslint-disable-line import/no-unresolved
import {
    withStyles
} from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormLabel from '@material-ui/core/FormLabel';

import "tinymce";
import "tinymce/themes/modern/theme";
import {
    Editor
} from "@tinymce/tinymce-react";


const styles = {
    "root": {
        "margin-top": "16px"
    },
    "label": {
        "font-size": "12px",
        "margin-bottom": "10px"
    }
};


class RenderTinyMCE extends React.Component {
    constructor(props) {

        super(props);

        this.changed = false;

        this.state = {
            "value": props.input.value
        };
    }

    componentWillUpdate(nextProps) {
        if(this.state.value !== nextProps.input.value) {
            this.changed = true;
        }
    }

    onChange(e) {
        let value = e.target.getContent();

        this.setState({
            "value": value
        });

        this.props.input.onChange(value);
    }

    render() {
        const {
            input,
            helperText,
            source,
            resource,
            label,
            isRequired,
            fullWidth,
            meta: {
                error
            },
            classes,
            ...custom
        } = this.props;

        let changed = this.changed;
        this.changed = false;
        custom.init.relative_urls = false;
        return (
            <FormControl error={error !== null && error !== undefined} fullWidth={fullWidth}>
                <FormLabel component="legend" className={classes.label}>
                    <FieldTitle
                        label={label}
                        source={source}
                        resource={resource}
                        isRequired={isRequired}
                    />
                </FormLabel>

                <Editor
                    {
                        ...(
                            changed ? {
                                "value": input.value,
                                "initialValue": input.value
                            } : {
                                "initialValue": input.value
                            }
                        )
                    }
                    onChange={(e) => {this.onChange(e);}}
                    init={custom.init || {}}
                />

                {error && <FormHelperText error>{error}</FormHelperText>}
                {helperText && <FormHelperText>{helperText}</FormHelperText>}
            </FormControl>
        );
    }
}

class TinyMCE extends React.Component {
    constructor(props) {
        super(props);

        if ('init' in props) {
            props.init.file_browser_callback = (field_name, url, type, win) => {
                const x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                const y = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeight;

                let cmsURL = '/laravel-filemanager?field_name=' + field_name + '&token=' + localStorage.getItem('token');
                if (type === 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file: cmsURL,
                    title: 'Filemanager',
                    width: x * 0.8,
                    height: y * 0.8,
                    resizable: "yes",
                    close_previous: "no"
                });
            }
        }
    }

    render() {
        let props = {
            ...this.props,
            fullWidth: this.props.fullWidth !== undefined ? this.props.fullWidth : true
        };

        return (
            <div className={this.props.classes.root}>
                <Field
                    name={this.props.source}
                    label={this.props.label}
                    component={RenderTinyMCE}
                    {...props}
                />
            </div>
        );
    }
}


export default withStyles(styles)(TinyMCE);