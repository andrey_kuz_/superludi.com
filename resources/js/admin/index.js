/* eslint react/jsx-key: off */
import React from 'react';
import { Admin, Resource } from 'react-admin'; // eslint-disable-line import/no-unresolved
import { render } from 'react-dom';
import { Route } from 'react-router';

import authProvider from './providers/authProvider';
import dataProvider from './providers/dataProvider';
import i18nProvider from './providers/i18nProvider';
import user from './modules/user';
import course from './modules/course';
import course_lesson from './modules/course_lesson';
import mail_template from './modules/mail_template';
import category from './modules/blog_category';
import post from './modules/blog_post';
import tag from './modules/blog_tag';
import payment_status from './modules/payment_status';
import payment from './modules/payment';
import season from './modules/season';
import speaker from './modules/speaker';
import info from './modules/info';
import comment from './modules/comment';
import course_lesson_material from './modules/course_lesson_material';
import subscription_type from './modules/subscription_type';
import subscription from './modules/subscription';


render(
    <Admin
        authProvider={authProvider}
        dataProvider={dataProvider}
        i18nProvider={i18nProvider}
        title="Панель управления"
        locale="ru"
    >
        {permissions => [
            (permissions.roles.indexOf('superadmin') !== -1) ? <Resource name="user" {...user} /> : <Resource name="user" />,
            (permissions.roles.indexOf('superadmin') !== -1) ? <Resource name="role" /> : <Resource name="role" />,
            (permissions.roles.indexOf('superadmin') !== -1) ? <Resource name="speaker" {...speaker} /> : <Resource name="speaker" />,
            (permissions.roles.indexOf('superadmin') !== -1) ? <Resource name="season" {...season} /> : <Resource name="season" />,
            (permissions.roles.indexOf('superadmin') !== -1) ? <Resource name="course" {...course} /> : <Resource name="course" />,
            (permissions.roles.indexOf('superadmin') !== -1) ? <Resource name="course_lesson" {...course_lesson} /> : <Resource name="course_lesson" />,
            (permissions.roles.indexOf('superadmin') !== -1) ? <Resource name="course_lesson_material" {...course_lesson_material} /> : <Resource name="course_lesson_material" />,
            (permissions.roles.indexOf('superadmin') !== -1) ? <Resource name="payment" {...payment} /> : <Resource name="payment" />,
            (permissions.roles.indexOf('superadmin') !== -1) ? <Resource name="payment_status" {...payment_status} /> : <Resource name="payment_status" />,
            (permissions.roles.indexOf('superadmin') !== -1) ? <Resource name="blog_category" {...category} /> : <Resource name="blog_category" />,
            (permissions.roles.indexOf('superadmin') !== -1) ? <Resource name="blog_post" {...post} /> : <Resource name="blog_post" />,
            (permissions.roles.indexOf('superadmin') !== -1) ? <Resource name="blog_tag" {...tag} /> : <Resource name="blog_tag" />,
            (permissions.roles.indexOf('superadmin') !== -1) ? <Resource name="mail_template" {...mail_template} /> : <Resource name="mail_template" />,
            (permissions.roles.indexOf('superadmin') !== -1) ? <Resource name="season" {...season} /> : <Resource name="season" />,
            (permissions.roles.indexOf('superadmin') !== -1) ? <Resource name="info" {...info} /> : <Resource name="info" />,
            (permissions.roles.indexOf('superadmin') !== -1) ? <Resource name="comment" {...comment} /> : <Resource name="comment" />,
            (permissions.roles.indexOf('superadmin') !== -1) ? <Resource name="subscription_type" {...subscription_type} /> : <Resource name="subscription_type" />,
            (permissions.roles.indexOf('superadmin') !== -1) ? <Resource name="subscription" {...subscription} /> : <Resource name="subscription" />,
        ]}
    </Admin>,
    document.getElementById('root')
);
