import LocationOnIcon from '@material-ui/icons/LocationOn';
import { withStyles } from '@material-ui/core/styles';
import React, { Children, cloneElement } from 'react';
import {
    Datagrid,
    DateField,
    NumberField,
    EditButton,
    Filter,
    List,
    Responsive,
    SearchInput,
    DeleteButton,
    SimpleList,
    TextField,
    BooleanField,
    ReferenceInput,
    SelectInput,
    ReferenceArrayField,
    SingleFieldList,
    ChipField,
} from 'react-admin'; // eslint-disable-line import/no-unresolved

const listStyles = {};

const InfoFilter = props => (
    <Filter {...props}>
        <SearchInput source="q" alwaysOn />
    </Filter>
);

const InfoListActionToolbar = withStyles({
    toolbar: {
        alignItems: 'center',
        display: 'flex',
    },
})(({ classes, children, ...props }) => (
    <div className={classes.toolbar}>
        {Children.map(children, button => cloneElement(button, props))}
    </div>
));

const InfoList = withStyles(listStyles)(({ classes, ...props }) => (
    <List
        {...props}
        bulkActionButtons={false}
        filters={<InfoFilter />}
        sort={{ field: 'id', order: 'ASC' }}
    >
        <Responsive
            small={
                <SimpleList
                    secondaryText={record => record.code}
                />
            }
            medium={
                <Datagrid rowClick="edit">
                    <NumberField source="id" cellClassName={classes.number} />
                    <TextField source="slug" />
                    <TextField source="translations_array.ru.title"  label="resources.blog_post.fields.title"/>
                    <DateField source="created_at" showTime cellClassName={classes.date} />
                    <InfoListActionToolbar>
                        <EditButton />
                        <DeleteButton />
                    </InfoListActionToolbar>
                </Datagrid>
            }
        />
    </List>
));

export default InfoList;
