import React from 'react';
import {
    Edit,
    FormTab,
    TabbedForm,
    TextInput,
    BooleanInput,
    NumberInput,
    LongTextInput,
    ImageInput,
    ImageField,
    required,
    translate,
    TextField,
    ReferenceInput,
    SelectInput,
    AutocompleteArrayInput,
    ReferenceArrayInput,
    SelectArrayInput,
} from 'react-admin'; // eslint-disable-line import/no-unresolved

import TinyMCE from "./../../fields/TinyMCE";
import "tinymce/plugins/code/plugin";
import "tinymce/plugins/link/plugin";
import "tinymce/plugins/image/plugin";

import { withStyles } from '@material-ui/core/styles';

const formStyles = {};

const UserTitle = translate(({ record, translate }) => (
    <span>
        {record ? translate('resources.info.title') : ''}
    </span>
));

const InfoEdit = withStyles(formStyles)(({ classes, ...props }) => (
    <Edit title={<UserTitle />} {...props}>
        <TabbedForm>
            <FormTab label="course.form.general" path="">
                <TextInput
                    source="slug"
                    validate={required()}
                    label="resources.info.fields.slug"
                />
            </FormTab>
            <FormTab label="locales.form.ru" path="ru">
                <TextInput
                    source="translations_array.ru.title"
                    validate={required()}
                    label="resources.info.fields.title"
                />
                <LongTextInput
                    source="translations_array.ru.description"
                    label="resources.blog_post.fields.description"
                />

                <LongTextInput
                    source="translations_array.ru.keywords"
                    label="resources.info.fields.keywords"
                />

                <TinyMCE
                    source="translations_array.ru.content"
                    label="resources.info.fields.content"
                    init={{
                        plugins: 'link image code',
                        toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code'
                    }}
                    validation={{ required: true }}
                />
            </FormTab>

            <FormTab label="locales.form.uk" path="uk">
                <TextInput
                    source="translations_array.uk.title"
                    label="resources.info.fields.title"
                />
                <LongTextInput
                    source="translations_array.uk.description"
                    label="resources.blog_post.fields.description"
                />

                <LongTextInput
                    source="translations_array.uk.keywords"
                    label="resources.info.fields.keywords"
                />

                <TinyMCE
                    source="translations_array.uk.content"
                    label="resources.info.fields.content"
                    init={{
                        plugins: 'link image code',
                        toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code'
                    }}
                    validation={{ required: true }}
                />
            </FormTab>
        </TabbedForm>
    </Edit>
));

export default InfoEdit;
