import Info from '@material-ui/icons/Info';
import InfoEdit from './InfoEdit';
import InfoList from './InfoList';

export default {
    list: InfoList,
    edit: InfoEdit,
    icon: Info,
};
