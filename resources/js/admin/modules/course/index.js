import LocalLibrary from '@material-ui/icons/LocalLibrary';
import CourseCreate from './CourseCreate';
import CourseEdit from './CourseEdit';
import CourseList from './CourseList';

export default {
    list: CourseList,
    create: CourseCreate,
    edit: CourseEdit,
    icon: LocalLibrary,
};
