import { withStyles } from '@material-ui/core/styles';
import React, { Children, cloneElement } from 'react';
import {
    Datagrid,
    DateField,
    NumberField,
    EditButton,
    Filter,
    List,
    Responsive,
    SearchInput,
    DeleteButton,
    SimpleList,
    TextField,
    BooleanField,
    ReferenceField,
    ReferenceInput,
    SelectInput
} from 'react-admin'; // eslint-disable-line import/no-unresolved

const listStyles = {};

const CourseFilter = props => (
    <Filter {...props}>
        <ReferenceInput
            label="resources.course.fields.season_id"
            source="season_id"
            reference="season" alwaysOn
        >
            <SelectInput optionText="title" />
        </ReferenceInput>

        <ReferenceInput
            label="resources.course.fields.speaker_id"
            source="speaker_id"
            reference="speaker" alwaysOn
        >
            <SelectInput optionText="name" />
        </ReferenceInput>

        <SearchInput source="q" alwaysOn />
    </Filter>
);

const CourseListActionToolbar = withStyles({
    toolbar: {
        alignItems: 'center',
        display: 'flex',
    },
})(({ classes, children, ...props }) => (
    <div className={classes.toolbar}>
        {Children.map(children, button => cloneElement(button, props))}
    </div>
));

const CourseList = withStyles(listStyles)(({ classes, ...props }) => (
    <List
        {...props}
        bulkActionButtons={false}
        filters={<CourseFilter />}
        sort={{ field: 'id', order: 'ASC' }}
    >
        <Responsive
            small={
                <SimpleList
                    primaryText={record => record.title}
                    secondaryText={record => record.code}
                />
            }
            medium={
                <Datagrid rowClick="edit">
                    <NumberField source="id" cellClassName={classes.number} />
                    <BooleanField source="is_published" />
                    <ReferenceField label="resources.course.fields.season_id" source="season_id" reference="season" allowEmpty sortable={false}>
                        <TextField source="title" />
                    </ReferenceField>
                    <ReferenceField label="resources.course.fields.speaker_id" source="speaker_id" reference="speaker" allowEmpty sortable={false}>
                        <TextField source="name" />
                    </ReferenceField>
                    <TextField source="title" sortable={false}/>
                    <NumberField source="price" />
                    <DateField source="created_at" showTime cellClassName={classes.date} />
                    <CourseListActionToolbar>
                        <EditButton />
                        <DeleteButton />
                    </CourseListActionToolbar>
                </Datagrid>
            }
        />
    </List>
));

export default CourseList;
