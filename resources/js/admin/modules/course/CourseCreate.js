import React from 'react';
import {
    Create,
    SaveButton,
    SimpleForm,
    TextInput,
    NumberInput,
    LongTextInput,
    Toolbar,
    required,
    ReferenceInput,
    SelectInput
} from 'react-admin'; // eslint-disable-line import/no-unresolved
import { withStyles } from '@material-ui/core/styles';

const CourseCreateToolbar = props => (
    <Toolbar {...props}>
        <SaveButton
            label="general.action.save"
            redirect="edit"
            submitOnEnter={true}
        />
    </Toolbar>
);

const formStyles = {};

const CourseCreate = withStyles(formStyles)(({ classes, ...props }) => (
    <Create {...props}>
        <SimpleForm toolbar={<CourseCreateToolbar />}>

            <ReferenceInput
                label="resources.course.fields.season_id"
                source="season_id"
                reference="season"
                validate={required()}>
                <SelectInput optionText="title" />
            </ReferenceInput>

            <ReferenceInput
                label="resources.course.fields.speaker_id"
                source="speaker_id"
                reference="speaker"
                validate={required()}>
                <SelectInput optionText="name" />
            </ReferenceInput>

            <TextInput
                source="slug"
                validate={required()}
                label="resources.course.fields.slug"
            />
            <TextInput
                source="translations_array.ru.title"
                validate={required()}
                label="resources.course.fields.title"
            />
            <NumberInput
                source="price"
                validate={required()}
            />
        </SimpleForm>
    </Create>
));

export default CourseCreate;
