import React from 'react';
import {
    Edit,
    FormTab,
    TabbedForm,
    TextInput,
    BooleanInput,
    NumberInput,
    LongTextInput,
    ImageInput,
    ImageField,
    required,
    translate,
    ReferenceInput,
    SelectInput
} from 'react-admin'; // eslint-disable-line import/no-unresolved

import { withStyles } from '@material-ui/core/styles';

const formStyles = {};

const CourseTitle = translate(({ record, translate }) => (
    <span>
        {record ? translate('course.edit.title', { title: record.title }) : ''}
    </span>
));

const CourseEdit = withStyles(formStyles)(({ classes, ...props }) => (
    <Edit title={<CourseTitle />} {...props}>
        <TabbedForm>
            <FormTab label="course.form.general" path="">
                <ReferenceInput
                    label="resources.course.fields.season_id"
                    source="season_id"
                    reference="season"
                    validate={required()}>
                    <SelectInput optionText="title" />
                </ReferenceInput>
                <ReferenceInput
                    label="resources.course.fields.speaker_id"
                    source="speaker_id"
                    reference="speaker"
                    validate={required()}>
                    <SelectInput optionText="name" />
                </ReferenceInput>
                <TextInput
                    source="slug"
                    validate={required()}
                    label="resources.course.fields.slug"
                />
                <NumberInput
                    source="price"
                    validate={required()}
                />
                <BooleanInput
                    source="is_published"
                    validate={required()}
                />
            </FormTab>
            <FormTab label="course.form.media" path="">
                <ImageInput source="banner_image" label="resources.course.fields.banner_image" accept="image/*">
                    <ImageField source="src" title="title" />
                </ImageInput>
                <ImageInput source="preview_image" label="resources.course.fields.preview_image" accept="image/*">
                    <ImageField source="src" title="title" />
                </ImageInput>
                <LongTextInput
                    source="banner_video_code"
                />
                <LongTextInput
                    source="teaser_video_code"
                />
            </FormTab>
            <FormTab label="locales.form.ru" path="ru">
                <TextInput
                    source="translations_array.ru.title"
                    validate={required()}
                    label="resources.course.fields.title"
                />
                <LongTextInput
                    source="translations_array.ru.description"
                    label="resources.course.fields.description"
                />
                <LongTextInput
                    source="translations_array.ru.keywords"
                    label="resources.course.fields.keywords"
                />
            </FormTab>
            <FormTab label="locales.form.uk" path="uk">
                <TextInput
                    source="translations_array.uk.title"
                    label="resources.course.fields.title"
                />
                <LongTextInput
                    source="translations_array.uk.description"
                    label="resources.course.fields.description"
                />
                <LongTextInput
                    source="translations_array.ru.keywords"
                    label="resources.course.fields.keywords"
                />
            </FormTab>
        </TabbedForm>
    </Edit>
));

export default CourseEdit;
