import RecordVoiceOver from '@material-ui/icons/RecordVoiceOver';
import SpeakerCreate from './SpeakerCreate';
import SpeakerEdit from './SpeakerEdit';
import SpeakerList from './SpeakerList';

export default {
    list: SpeakerList,
    create: SpeakerCreate,
    edit: SpeakerEdit,
    icon: RecordVoiceOver
};
