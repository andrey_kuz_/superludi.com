import React from 'react';
import {
    Edit,
    FormTab,
    TabbedForm,
    TextInput,
    ImageInput,
    ImageField,
    required,
    translate,
    DisabledInput,
    LongTextInput
} from 'react-admin'; // eslint-disable-line import/no-unresolved

import { withStyles } from '@material-ui/core/styles';

const formStyles = {};

const SpeakerTitle = translate(({ record, translate }) => (
    <span>
        {record ? translate('speaker.edit.name', { name: record.name }) : ''}
    </span>
));

const SpeakerEdit = withStyles(formStyles)(({ classes, ...props }) => (
    <Edit title={<SpeakerTitle />} {...props}>
        <TabbedForm>
            <FormTab label="speaker.form.general" path="">
                <DisabledInput source="id" />
            </FormTab>
            <FormTab label="speaker.form.media" path="">
                <ImageInput source="avatar" label="resources.speaker.fields.avatar" accept="image/*">
                    <ImageField source="src" title="title" />
                </ImageInput>
            </FormTab>
            <FormTab label="locales.form.ru" path="ru">
                <TextInput
                    source="translations_array.ru.name"
                    validate={required()}
                    label="resources.speaker.fields.name"
                />
                <LongTextInput
                    source="translations_array.ru.description"
                    label="resources.speaker.fields.description"
                />
                <TextInput
                    source="translations_array.ru.position"
                    label="resources.speaker.fields.position"
                />
            </FormTab>
            <FormTab label="locales.form.uk" path="uk">
                <TextInput
                    source="translations_array.uk.name"
                    label="resources.speaker.fields.name"
                />
                <LongTextInput
                    source="translations_array.uk.description"
                    label="resources.speaker.fields.description"
                />
                <TextInput
                    source="translations_array.uk.position"
                    label="resources.speaker.fields.position"
                />
            </FormTab>
        </TabbedForm>
    </Edit>
));

export default SpeakerEdit;
