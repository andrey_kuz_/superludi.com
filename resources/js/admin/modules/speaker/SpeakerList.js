import { withStyles } from '@material-ui/core/styles';
import React, { Children, cloneElement } from 'react';
import {
    Datagrid,
    DateField,
    NumberField,
    EditButton,
    Filter,
    List,
    Responsive,
    SearchInput,
    DeleteButton,
    SimpleList,
    TextField,
    BooleanField,
    ReferenceField,
    ReferenceInput,
    SelectInput
} from 'react-admin'; // eslint-disable-line import/no-unresolved

const listStyles = {};

const SpeakerFilter = props => (
    <Filter {...props}>
        <SearchInput source="q" alwaysOn />
    </Filter>
);

const SpeakerListActionToolbar = withStyles({
    toolbar: {
        alignItems: 'center',
        display: 'flex',
    },
})(({ classes, children, ...props }) => (
    <div className={classes.toolbar}>
        {Children.map(children, button => cloneElement(button, props))}
    </div>
));

const SpeakerList = withStyles(listStyles)(({ classes, ...props }) => (
    <List
        {...props}
        bulkActionButtons={false}
        filters={<SpeakerFilter />}
        sort={{ field: 'id', order: 'ASC' }}
    >
        <Responsive
            small={
                <SimpleList
                    primaryText={record => record.title}
                    secondaryText={record => record.code}
                />
            }
            medium={
                <Datagrid rowClick="edit">
                    <NumberField source="id" cellClassName={classes.number} />
                    <TextField source="name" sortable={false}/>
                    <DateField source="created_at" showTime cellClassName={classes.date} />
                    <SpeakerListActionToolbar>
                        <EditButton />
                        <DeleteButton />
                    </SpeakerListActionToolbar>
                </Datagrid>
            }
        />
    </List>
));

export default SpeakerList;
