import LocationOnIcon from '@material-ui/icons/LocationOn';
import { withStyles } from '@material-ui/core/styles';
import React, { Children, cloneElement } from 'react';
import {
    Datagrid,
    DateField,
    NumberField,
    EditButton,
    Filter,
    List,
    Responsive,
    SearchInput,
    SimpleList,
    TextField
} from 'react-admin'; // eslint-disable-line import/no-unresolved

const listStyles = {};

const PaymentStatusFilter = props => (
    <Filter {...props}>
        <SearchInput source="q" alwaysOn />
    </Filter>
);

const PaymentStatusActionToolbar = withStyles({
    toolbar: {
        alignItems: 'center',
        display: 'flex',
    },
})(({ classes, children, ...props }) => (
    <div className={classes.toolbar}>
        {Children.map(children, button => cloneElement(button, props))}
    </div>
));

const PaymentStatusList = withStyles(listStyles)(({ classes, ...props }) => (
    <List
        {...props}
        bulkActionButtons={false}
        filters={<PaymentStatusFilter />}
        sort={{ field: 'id', order: 'ASC' }}
    >
        <Responsive
            small={
                <SimpleList
                    primaryText={record => record.name}
                    secondaryText={record => record.title}
                />
            }
            medium={
                <Datagrid>
                    <NumberField source="id" cellClassName={classes.number} />
                    <TextField source="name" sortable={false}/>
                    <TextField source="title" sortable={false}/>
                    <DateField source="created_at" showTime cellClassName={classes.date} />
                    <PaymentStatusActionToolbar>
                        <EditButton />
                    </PaymentStatusActionToolbar>
                </Datagrid>
            }
        />
    </List>
));

export default PaymentStatusList;
