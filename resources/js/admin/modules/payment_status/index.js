import CheckBox from '@material-ui/icons/CheckBox';
import PaymentStatusList from './PaymentStatusList';
import PaymentStatusEdit from './PaymentStatusEdit';

export default {
    list: PaymentStatusList,
    edit: PaymentStatusEdit,
    icon: CheckBox
};
