import React from 'react';
import {
    Edit,
    FormTab,
    TabbedForm,
    TextInput,
    translate,
    Toolbar,
    SaveButton,
    DisabledInput

} from 'react-admin'; // eslint-disable-line import/no-unresolved

import { withStyles } from '@material-ui/core/styles';

const formStyles = {};

const PaymentStatusTitle = translate(({ record, translate }) => (
    <span>
         {record ? translate('payment_status.edit.name', { name: record.name }) : ''}
    </span>
));

const PaymentStatusToolbar = props => (
    <Toolbar {...props} >
        <SaveButton />
    </Toolbar>
);

const PaymentStatusEdit = withStyles(formStyles)(({ classes, ...props }) => (
    <Edit title={<PaymentStatusTitle />} {...props}>
        <TabbedForm toolbar={<PaymentStatusToolbar />}>
            <FormTab label="payment_status.form.general" path="">
                <DisabledInput
                    source="name"
                    label="resources.payment_status.fields.name"
                />
            </FormTab>
            <FormTab label="locales.form.ru" path="ru">
                <TextInput
                    source="translations_array.ru.title"
                    label="resources.payment_status.fields.title"
                />
            </FormTab>
            <FormTab label="locales.form.uk" path="uk">
                <TextInput
                    source="translations_array.uk.title"
                    label="resources.payment_status.fields.title"
                />
            </FormTab>
        </TabbedForm>
    </Edit>
));

export default PaymentStatusEdit;
