import PermMedia from '@material-ui/icons/PermMedia';
import SeasonCreate from './SeasonCreate';
import SeasonEdit from './SeasonEdit';
import SeasonList from './SeasonList';

export default {
    list: SeasonList,
    create: SeasonCreate,
    edit: SeasonEdit,
    icon: PermMedia
};
