import React from 'react';
import {
    Edit,
    FormTab,
    TabbedForm,
    TextInput,
    required,
    translate,
    DisabledInput
} from 'react-admin'; // eslint-disable-line import/no-unresolved

import { withStyles } from '@material-ui/core/styles';

const formStyles = {};

const SeasonTitle = translate(({ record, translate }) => (
    <span>
        {record ? translate('season.edit.title', { title: record.title }) : ''}
    </span>
));

const SeasonEdit = withStyles(formStyles)(({ classes, ...props }) => (
    <Edit title={<SeasonTitle />} {...props}>
        <TabbedForm>
            <FormTab label="course.form.general" path="">
                <DisabledInput source="id" />
            </FormTab>

            <FormTab label="locales.form.ru" path="ru">
                <TextInput
                    source="translations_array.ru.title"
                    validate={required()}
                    label="resources.season.fields.title"
                />

                <TextInput
                    source="translations_array.ru.title_short"
                    label="resources.season.fields.title_short"
                />
            </FormTab>
            <FormTab label="locales.form.uk" path="uk">
                <TextInput
                    source="translations_array.uk.title"
                    label="resources.season.fields.title"
                />

                <TextInput
                    source="translations_array.uk.title_short"
                    label="resources.season.fields.title_short"
                />
            </FormTab>
        </TabbedForm>
    </Edit>
));

export default SeasonEdit;
