// import LocationOnIcon from '@material-ui/icons/LocationOn';
import { withStyles } from '@material-ui/core/styles';
import React, { Children, cloneElement } from 'react';
import {
    Datagrid,
    DateField,
    NumberField,
    EditButton,
    Filter,
    List,
    Responsive,
    SearchInput,
    DeleteButton,
    SimpleList,
    TextField,
    ReferenceInput,
    SelectInput
} from 'react-admin'; // eslint-disable-line import/no-unresolved

const listStyles = {};

const SeasonFilter = props => (
    <Filter {...props}>
        <ReferenceInput
            label="resources.season.fields.subscription_types"
            source="subscription_type_id"
            reference="subscription_type" alwaysOn
        >
            <SelectInput optionText="title" />
        </ReferenceInput>
        <SearchInput source="q" alwaysOn />
    </Filter>
);

const SeasonListActionToolbar = withStyles({
    toolbar: {
        alignItems: 'center',
        display: 'flex',
    },
})(({ classes, children, ...props }) => (
    <div className={classes.toolbar}>
        {Children.map(children, button => cloneElement(button, props))}
    </div>
));

const SeasonList = withStyles(listStyles)(({ classes, ...props }) => (
    <List
        {...props}
        bulkActionButtons={false}
        filters={<SeasonFilter />}
        sort={{ field: 'id', order: 'ASC' }}
    >
        <Responsive
            small={
                <SimpleList
                    primaryText={record => record.title}
                    secondaryText={record => record.code}
                />
            }
            medium={
                <Datagrid rowClick="edit">
                    <NumberField source="id" cellClassName={classes.number} />
                    <TextField source="title" />
                    <DateField source="created_at" showTime cellClassName={classes.date} />
                    <SeasonListActionToolbar>
                        <EditButton />
                        <DeleteButton />
                    </SeasonListActionToolbar>
                </Datagrid>
            }
        />
    </List>
));

export default SeasonList;
