import React from 'react';
import {
    Create,
    SaveButton,
    SimpleForm,
    TextInput,
    Toolbar,
    required,
} from 'react-admin'; // eslint-disable-line import/no-unresolved
import { withStyles } from '@material-ui/core/styles';

const SeasonCreateToolbar = props => (
    <Toolbar {...props}>
        <SaveButton
            label="general.action.save"
            redirect="edit"
            submitOnEnter={true}
        />
    </Toolbar>
);

const formStyles = {};

const SeasonCreate = withStyles(formStyles)(({ classes, ...props }) => (
    <Create {...props}>
        <SimpleForm toolbar={<SeasonCreateToolbar />}>

            <TextInput
                source="translations_array.ru.title"
                validate={required()}
                label="resources.season.fields.title"
            />

            <TextInput
                source="translations_array.ru.title_short"
                label="resources.season.fields.title_short"
            />

        </SimpleForm>
    </Create>
));

export default SeasonCreate;
