import React from 'react';
import {
    Edit,
    FormTab,
    TabbedForm,
    TextInput,
    BooleanInput,
    NumberInput,
    LongTextInput,
    ImageInput,
    ImageField,
    required,
    translate,
    TextField,
    ReferenceInput,
    SelectInput,
    AutocompleteArrayInput,
    ReferenceArrayInput,
    SelectArrayInput,
} from 'react-admin'; // eslint-disable-line import/no-unresolved

import RichTextInput from 'ra-input-rich-text';

import { withStyles } from '@material-ui/core/styles';

const formStyles = {};

const UserTitle = translate(({ record, translate }) => (
    <span>
        {record ? translate('resources.comment.title') : ''}
    </span>
));

const CommentEdit = withStyles(formStyles)(({ classes, ...props }) => (
    <Edit title={<UserTitle />} {...props}>
        <TabbedForm>
            <FormTab label="comment.form.general" path="">
                <BooleanInput
                    source="is_published"
                    validate={required()}
                />
                <LongTextInput source="content" label="resources.comment.fields.content" validation={{ required: true }} />
            </FormTab>
        </TabbedForm>
    </Edit>
));

export default CommentEdit;
