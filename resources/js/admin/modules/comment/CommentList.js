import { withStyles } from '@material-ui/core/styles';
import React, { Children, cloneElement } from 'react';
import {
    Datagrid,
    DateField,
    NumberField,
    EditButton,
    Filter,
    List,
    Responsive,
    SearchInput,
    DeleteButton,
    SimpleList,
    TextField,
    BooleanField,
    ReferenceInput,
    SelectInput,
    SingleFieldList,
    ChipField,
    TextInput,
    ReferenceField
} from 'react-admin'; // eslint-disable-line import/no-unresolved

const listStyles = {};

const CommentFilter = props => (
    <Filter {...props}>
        <SearchInput source="q" alwaysOn />
        <SelectInput source="is_published" label="resources.comment.fields.is_published" choices={[
            { id: 1, name: 'Опубликован' },
            { id: 0, name: 'Не опубликован' },
        ]} alwaysOn />
        <SelectInput source="type" label="Относится" choices={[
            { id: 'App/BlogPost', name: 'Пост' },
            { id: 'App/CourseLesson', name: 'Видео' },
        ]} alwaysOn />
        <SearchInput source="object_id" alwaysOn label="id объекта"/>
    </Filter>
);

const CommentListActionToolbar = withStyles({
    toolbar: {
        alignItems: 'center',
        display: 'flex',
    },
})(({ classes, children, ...props }) => (
    <div className={classes.toolbar}>
        {Children.map(children, button => cloneElement(button, props))}
    </div>
));

const CommentList = withStyles(listStyles)(({ classes, ...props }) => (
    <List
        {...props}
        bulkActionButtons={false}
        filters={<CommentFilter />}
        sort={{ field: 'id', order: 'ASC' }}
    >
        <Responsive
            small={
                <SimpleList
                    secondaryText={record => record.code}
                />
            }
            medium={
                <Datagrid rowClick="edit">
                    <NumberField source="id" cellClassName={classes.number} />
                    <TextField source="content" />
                    <NumberField source="parent_id" cellClassName={classes.number} />
                    <BooleanField source="is_published" />
                    <DateField source="created_at" showTime cellClassName={classes.date} />
                    <CommentListActionToolbar>
                        <EditButton />
                        <DeleteButton />
                    </CommentListActionToolbar>
                </Datagrid>
            }
        />
    </List>
));

export default CommentList;
