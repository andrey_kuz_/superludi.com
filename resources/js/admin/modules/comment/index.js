import Comment from '@material-ui/icons/Comment';
import CommentEdit from './CommentEdit';
import CommentList from './CommentList';

export default {
    list: CommentList,
    edit: CommentEdit,
    icon: Comment,
};
