import VideoLibrary from '@material-ui/icons/VideoLibrary';
import CourseLessonCreate from './CourseLessonCreate';
import CourseLessonEdit from './CourseLessonEdit';
import CourseLessonList from './CourseLessonList';

export default {
    list: CourseLessonList,
    create: CourseLessonCreate,
    edit: CourseLessonEdit,
    icon: VideoLibrary,
};
