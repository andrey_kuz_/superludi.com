import React from 'react';
import {
    Edit,
    FormTab,
    TabbedForm,
    TextInput,
    BooleanInput,
    NumberInput,
    LongTextInput,
    ImageInput,
    ImageField,
    required,
    translate,
    ReferenceInput,
    SelectInput
} from 'react-admin'; // eslint-disable-line import/no-unresolved

import TinyMCE from "./../../fields/TinyMCE";
import "tinymce/plugins/code/plugin";
import "tinymce/plugins/link/plugin";
import "tinymce/plugins/image/plugin";
import "tinymce/plugins/media/plugin";
import "tinymce/plugins/template/plugin";

import { withStyles } from '@material-ui/core/styles';

const formStyles = {};

const CourseLessonTitle = translate(({ record, translate }) => (
    <span>
        {record ? translate('course_lesson.edit.title', { title: record.title }) : ''}
    </span>
));

const CourseLessonEdit = withStyles(formStyles)(({ classes, ...props }) => (
    <Edit title={<CourseLessonTitle />} {...props}>
        <TabbedForm>
            <FormTab label="course_lesson.form.general" path="">
                <ReferenceInput
                    label="resources.course_lesson.fields.course_id"
                    source="course_id"
                    reference="course"
                    validate={required()}>
                    <SelectInput optionText="title" />
                </ReferenceInput>
                <NumberInput
                    label="resources.course_lesson.fields.ord"
                    source="ord"
                    validate={required()}
                />
            </FormTab>
            <FormTab label="course_lesson.form.media" path="">
                <ImageInput source="preview_image" label="resources.course_lesson.fields.preview_image" accept="image/*">
                    <ImageField source="src" title="title" />
                </ImageInput>
                <LongTextInput
                    source="video_code"
                    validate={required()}
                    label="resources.course_lesson.fields.video_code"
                />
            </FormTab>
            <FormTab label="locales.form.ru" path="ru">
                <TextInput
                    source="translations_array.ru.title"
                    validate={required()}
                    label="resources.course_lesson.fields.title"
                />
                <TinyMCE
                    source="translations_array.ru.description"
                    label="resources.course_lesson.fields.description"
                    init={{
                        plugins: [
                            "link",
                            "image",
                            "code",
                            "insertdatetime media table contextmenu paste",
                            "media",
                            "template"
                        ],
                        toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | bullist numlist | code | link image | media'
                    }}
                    validation={{ required: true }}
                />
            </FormTab>
            <FormTab label="locales.form.uk" path="uk">
                <TextInput
                    source="translations_array.uk.title"
                    label="resources.course_lesson.fields.title"
                />
                <TinyMCE
                    source="translations_array.uk.description"
                    label="resources.course_lesson.fields.description"
                    init={{
                        plugins: [
                            "link",
                            "image",
                            "code",
                            "insertdatetime media table contextmenu paste",
                            "media",
                            "template"
                        ],
                        toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | bullist numlist | code | link image | media'
                    }}
                    validation={{ required: true }}
                />
            </FormTab>
        </TabbedForm>
    </Edit>
));

export default CourseLessonEdit;
