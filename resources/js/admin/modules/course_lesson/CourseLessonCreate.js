import React from 'react';
import {
    Create,
    SaveButton,
    SimpleForm,
    TextInput,
    NumberInput,
    LongTextInput,
    Toolbar,
    required,
    ReferenceInput,
    SelectInput,
} from 'react-admin'; // eslint-disable-line import/no-unresolved
import { withStyles } from '@material-ui/core/styles';

const CourseLessonCreateToolbar = props => (
    <Toolbar {...props}>
        <SaveButton
            label="general.action.save"
            redirect="edit"
            submitOnEnter={true}
        />
    </Toolbar>
);

const formStyles = {};

const CourseLessonCreate = withStyles(formStyles)(({ classes, ...props }) => (
    <Create {...props}>
        <SimpleForm toolbar={<CourseLessonCreateToolbar />}>
            <ReferenceInput
                label="resources.course_lesson.fields.course_id"
                source="course_id"
                reference="course"
                validate={required()}>
                <SelectInput optionText="title" />
            </ReferenceInput>
            <TextInput
                source="translations_array.ru.title"
                validate={required()}
                label="resources.course_lesson.fields.title"
            />
            <LongTextInput
                source="translations_array.ru.description"
                label="resources.course_lesson.fields.description"
            />
            <LongTextInput
                source="video_code"
                validate={required()}
                label="resources.course_lesson.fields.video_code"
            />
            <NumberInput
                label="resources.course_lesson.fields.ord"
                source="ord"
                validate={required()}
            />

        </SimpleForm>
    </Create>
));

export default CourseLessonCreate;
