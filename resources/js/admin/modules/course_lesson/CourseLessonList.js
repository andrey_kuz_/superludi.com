import LocationOnIcon from '@material-ui/icons/LocationOn';
import { withStyles } from '@material-ui/core/styles';
import React, { Children, cloneElement } from 'react';
import {
    Datagrid,
    DateField,
    NumberField,
    EditButton,
    Filter,
    List,
    Responsive,
    SearchInput,
    DeleteButton,
    SimpleList,
    TextField,
    ReferenceField,
    ReferenceInput,
    SelectInput,
    TextInput,
    required
} from 'react-admin'; // eslint-disable-line import/no-unresolved

const listStyles = {};

const CourseLessonFilter = props => (
    <Filter {...props}>
        <ReferenceInput
            label="resources.course_lesson.fields.course_id"
            source="course_id"
            reference="course" alwaysOn
          >
            <SelectInput optionText="title" />
        </ReferenceInput>

        <SearchInput source="q" alwaysOn />
    </Filter>
);

const CourseLessonListActionToolbar = withStyles({
    toolbar: {
        alignItems: 'center',
        display: 'flex',
    },
})(({ classes, children, ...props }) => (
    <div className={classes.toolbar}>
        {Children.map(children, button => cloneElement(button, props))}
    </div>
));

const CourseLessonList = withStyles(listStyles)(({ classes, ...props }) => (
    <List
        {...props}
        bulkActionButtons={false}
        filters={<CourseLessonFilter />}
        sort={{ field: 'ord', order: 'ASC' }}
    >
        <Responsive
            small={
                <SimpleList
                    primaryText={record => record.title}
                    secondaryText={record => record.code}
                />
            }
            medium={
                <Datagrid>
                    <NumberField source="id" cellClassName={classes.number} />
                    <ReferenceField label="resources.course_lesson.fields.course_id" source="course_id" reference="course">
                        <TextField source="title" />
                    </ReferenceField>
                    <TextField source="title" sortable={false}/>
                    <TextField source="ord" />
                    <DateField source="created_at" showTime cellClassName={classes.date} />
                    <CourseLessonListActionToolbar>
                        <EditButton />
                        <DeleteButton />
                    </CourseLessonListActionToolbar>
                </Datagrid>
            }
        />
    </List>
));

export default CourseLessonList;
