// import VideoLibrary from '@material-ui/icons/VideoLibrary';
import CourseLessonMaterialCreate from './CourseLessonMaterialCreate';
import CourseLessonMaterialEdit from './CourseLessonMaterialEdit';
import CourseLessonMaterialList from './CourseLessonMaterialList';

export default {
    list: CourseLessonMaterialList,
    create: CourseLessonMaterialCreate,
    edit: CourseLessonMaterialEdit,
    // icon: VideoLibrary,
};
