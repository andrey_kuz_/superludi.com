import React from 'react';
import {
    Edit,
    FormTab,
    TabbedForm,
    TextInput,
    BooleanInput,
    NumberInput,
    LongTextInput,
    ImageInput,
    ImageField,
    required,
    translate,
    ReferenceInput,
    SelectInput,
    FileInput,
    FileField
} from 'react-admin'; // eslint-disable-line import/no-unresolved

import { withStyles } from '@material-ui/core/styles';

const formStyles = {};

const CourseLessonMaterialTitle = translate(({ record, translate }) => (
    <span>
        {record ? translate('course_lesson_material.edit.title', { title: record.title }) : ''}
    </span>
));

const CourseLessonMaterialEdit = withStyles(formStyles)(({ classes, ...props }) => (
    <Edit title={<CourseLessonMaterialTitle />} {...props}>
        <TabbedForm>
            <FormTab label="course_lesson_material.form.general" path="">
                <ReferenceInput
                    label="resources.course_lesson_material.fields.course_id"
                    source="course_id"
                    reference="course">
                    <SelectInput optionText="title" />
                </ReferenceInput>
                <ReferenceInput
                    label="resources.course_lesson_material.fields.course_lesson_id"
                    source="course_lesson_id"
                    reference="course_lesson">
                    <SelectInput optionText="title" />
                </ReferenceInput>
                <TextInput
                    source="title"
                    validate={required()}
                    label="resources.course_lesson_material.fields.title"
                />
            </FormTab>

            <FormTab label="course_lesson_material.form.media" path="">
                <FileInput source="file"
                           label="resources.course_lesson_material.fields.file"
                           accept="application/pdf, application/vnd.ms-excel"
                           placeholder={<p>Допустимые форматы: PDF и XLS</p>}
                >
                    <FileField source="src" title="title" />
                </FileInput>
            </FormTab>

        </TabbedForm>
    </Edit>
));

export default CourseLessonMaterialEdit;
