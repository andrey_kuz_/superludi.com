import React from 'react';
import {
    Create,
    SaveButton,
    SimpleForm,
    TextInput,
    NumberInput,
    LongTextInput,
    Toolbar,
    required,
    ReferenceInput,
    SelectInput,
} from 'react-admin'; // eslint-disable-line import/no-unresolved
import { withStyles } from '@material-ui/core/styles';

const CourseLessonMaterialCreateToolbar = props => (
    <Toolbar {...props}>
        <SaveButton
            label="general.action.save"
            redirect="edit"
            submitOnEnter={true}
        />
    </Toolbar>
);

const formStyles = {};

const CourseLessonMaterialCreate = withStyles(formStyles)(({ classes, ...props }) => (
    <Create {...props}>
        <SimpleForm toolbar={<CourseLessonMaterialCreateToolbar />}>
            <ReferenceInput
                label="resources.course_lesson_material.fields.course_id"
                source="course_id"
                reference="course">
                <SelectInput optionText="title" />
            </ReferenceInput>
            <ReferenceInput
                label="resources.course_lesson_material.fields.course_lesson_id"
                source="course_lesson_id"
                reference="course_lesson">
                <SelectInput optionText="title" />
            </ReferenceInput>
            <TextInput
                source="title"
                validate={required()}
                label="resources.course_lesson_material.fields.title"
            />

        </SimpleForm>
    </Create>
));

export default CourseLessonMaterialCreate;
