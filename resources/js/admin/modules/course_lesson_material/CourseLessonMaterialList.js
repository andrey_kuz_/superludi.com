import LocationOnIcon from '@material-ui/icons/LocationOn';
import { withStyles } from '@material-ui/core/styles';
import React, { Children, cloneElement } from 'react';
import {
    Datagrid,
    DateField,
    NumberField,
    EditButton,
    Filter,
    List,
    Responsive,
    SearchInput,
    DeleteButton,
    SimpleList,
    TextField,
    ReferenceField,
    ReferenceInput,
    SelectInput,
    required
} from 'react-admin'; // eslint-disable-line import/no-unresolved

const listStyles = {};

const CourseLessonMaterialFilter = props => (
    <Filter {...props}>
        <ReferenceInput
            source="course_id"
            reference="course" alwaysOn
        >
            <SelectInput optionText="title" />
        </ReferenceInput>
        <ReferenceInput
            source="course_lesson_id"
            reference="course_lesson" alwaysOn
        >
            <SelectInput optionText="title" />
        </ReferenceInput>

        <SearchInput source="q" alwaysOn />
    </Filter>
);

const CourseLessonMaterialListActionToolbar = withStyles({
    toolbar: {
        alignItems: 'center',
        display: 'flex',
    },
})(({ classes, children, ...props }) => (
    <div className={classes.toolbar}>
        {Children.map(children, button => cloneElement(button, props))}
    </div>
));

const CourseLessonMaterialList = withStyles(listStyles)(({ classes, ...props }) => (
    <List
        {...props}
        bulkActionButtons={false}
        filters={<CourseLessonMaterialFilter />}
        sort={{ field: 'id', order: 'ASC' }}
    >
        <Responsive
            small={
                <SimpleList
                    primaryText={record => record.title}
                    secondaryText={record => record.code}
                />
            }
            medium={
                <Datagrid>
                    <NumberField source="id" cellClassName={classes.number} />
                    <ReferenceField source="course_id" reference="course" allowEmpty>
                        <TextField source="title" />
                    </ReferenceField>
                    <ReferenceField source="course_lesson_id" reference="course_lesson" allowEmpty>
                        <TextField source="title" />
                    </ReferenceField>
                    <TextField source="title" sortable={false}/>
                    <DateField source="created_at" showTime cellClassName={classes.date} />
                    <CourseLessonMaterialListActionToolbar>
                        <EditButton />
                        <DeleteButton />
                    </CourseLessonMaterialListActionToolbar>
                </Datagrid>
            }
        />
    </List>
));

export default CourseLessonMaterialList;
