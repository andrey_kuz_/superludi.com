import LocationOnIcon from '@material-ui/icons/LocationOn';
import { withStyles } from '@material-ui/core/styles';
import React, { Children, cloneElement } from 'react';
import {
    Datagrid,
    DateField,
    NumberField,
    EditButton,
    Filter,
    List,
    Responsive,
    SearchInput,
    SimpleList,
    TextField
} from 'react-admin'; // eslint-disable-line import/no-unresolved

const listStyles = {};

const MailTemplateFilter = props => (
    <Filter {...props}>
        <SearchInput source="q" alwaysOn />
    </Filter>
);

const MailTemplateActionToolbar = withStyles({
    toolbar: {
        alignItems: 'center',
        display: 'flex',
    },
})(({ classes, children, ...props }) => (
    <div className={classes.toolbar}>
        {Children.map(children, button => cloneElement(button, props))}
    </div>
));

const MailTemplateList = withStyles(listStyles)(({ classes, ...props }) => (
    <List
        {...props}
        bulkActionButtons={false}
        filters={<MailTemplateFilter />}
        sort={{ field: 'id', order: 'ASC' }}
    >
        <Responsive
            small={
                <SimpleList
                    primaryText={record => record.title}
                    secondaryText={record => record.code}
                />
            }
            medium={
                <Datagrid>
                    <NumberField source="id" cellClassName={classes.number} />
                    <TextField source="name" sortable={false}/>
                    <TextField source="subject" sortable={false}/>
                    <DateField source="created_at" showTime cellClassName={classes.date} />
                    <MailTemplateActionToolbar>
                        <EditButton />
                    </MailTemplateActionToolbar>
                </Datagrid>
            }
        />
    </List>
));

export default MailTemplateList;
