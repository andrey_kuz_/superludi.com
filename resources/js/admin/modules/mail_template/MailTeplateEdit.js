import React from 'react';
import {
    Edit,
    FormTab,
    TabbedForm,
    TextInput,
    NumberInput,
    LongTextInput,
    required,
    translate,
    Toolbar,
    SaveButton,
    DisabledInput
} from 'react-admin'; // eslint-disable-line import/no-unresolved


import TinyMCE from "./../../fields/TinyMCE";
import "tinymce/plugins/code/plugin";
import "tinymce/plugins/link/plugin";
import "tinymce/plugins/image/plugin";

import { withStyles } from '@material-ui/core/styles';

const formStyles = {};

const MailTemplateTitle = translate(({ record, translate }) => (
    <span>
         {record ? translate('mail_template.edit.name', { name: record.name }) : ''}
    </span>
));

const MailTemplateToolbar = props => (
    <Toolbar {...props} >
        <SaveButton />
    </Toolbar>
);

const MailTemplateEdit = withStyles(formStyles)(({ classes, ...props }) => (
    <Edit title={<MailTemplateTitle />} {...props}>
        <TabbedForm toolbar={<MailTemplateToolbar />}>
            <FormTab label="mail_template.form.general" path="">
                <DisabledInput
                    label="resources.mail_template.fields.name"
                    source="name"
                />
            </FormTab>
            <FormTab label="locales.form.ru" path="ru">
                <TextInput
                    source="translations_array.ru.subject"
                    label="resources.mail_template.fields.subject"
                />

                <TinyMCE
                    source="translations_array.ru.content"
                    label="resources.mail_template.fields.content"
                    init={{
                        plugins: 'link image code',
                        toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code'
                    }}
                 />
            </FormTab>
            <FormTab label="locales.form.uk" path="uk">
                <TextInput
                    source="translations_array.uk.subject"
                    label="resources.mail_template.fields.subject"
                />

                <TinyMCE
                    source="translations_array.uk.content"
                    label="resources.mail_template.fields.content"
                    init={{
                        plugins: 'link image code',
                        toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code'
                    }}
                />
            </FormTab>
        </TabbedForm>
    </Edit>
));

export default MailTemplateEdit;
