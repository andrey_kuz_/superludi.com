import Email from '@material-ui/icons/Email';
import MailTemplateEdit from './MailTeplateEdit';
import MailTemplateList from './MailTemplateList';

export default {
    list: MailTemplateList,
    edit: MailTemplateEdit,
    icon: Email,
};
