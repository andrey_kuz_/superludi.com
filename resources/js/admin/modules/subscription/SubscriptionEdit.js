import React from 'react';
import {
    Edit,
    FormTab,
    TabbedForm,
    TextInput,
    required,
    translate,
    NumberInput,
    DateInput,
    ReferenceArrayInput,
    SelectArrayInput,
    ReferenceInput,
    SelectInput,
    BooleanInput,
    AutocompleteInput
} from 'react-admin'; // eslint-disable-line import/no-unresolved

import { withStyles } from '@material-ui/core/styles';

const formStyles = {};

const SubscriptionTypeTitle = translate(({ record, translate }) => (
    <span>
        {record ? translate('subscription.edit.name', { name: record.id }) : ''}
    </span>
));

const SubscriptionTypeEdit = withStyles(formStyles)(({ classes, ...props }) => (
    <Edit title={<SubscriptionTypeTitle />} {...props}>
        <TabbedForm>

            <FormTab label="subscription.form.general" path="">
                <ReferenceInput label="resources.subscription.fields.user" source="user_id" reference="user" inputValueMatcher={() => null} validate={required()}>
                    <AutocompleteInput optionText="email" />
                </ReferenceInput>

                <ReferenceInput
                    label="resources.subscription.fields.type"
                    source="type_id"
                    reference="subscription_type"
                    validate={required()}>
                    <SelectInput optionText="name" />
                </ReferenceInput>

                <BooleanInput
                    label="resources.subscription.fields.is_active"
                    source="is_active"
                />

                <DateInput
                    label="resources.subscription.fields.expiring_at"
                    source="expiring_at"
                />

            </FormTab>

        </TabbedForm>
    </Edit>
));

export default SubscriptionTypeEdit;
