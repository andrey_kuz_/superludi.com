import React from 'react';
import {
    Create,
    SimpleForm,
    Toolbar,
    SaveButton,
    TabbedForm,
    TextInput,
    required,
    translate,
    NumberInput,
    DateInput,
    ReferenceArrayInput,
    SelectArrayInput,
    ReferenceInput,
    SelectInput,
    BooleanInput,
    AutocompleteInput
} from 'react-admin'; // eslint-disable-line import/no-unresolved

import { withStyles } from '@material-ui/core/styles';

const formStyles = {};

const SubscriptionCreateToolbar = props => (
    <Toolbar {...props}>
        <SaveButton
            label="general.action.save"
        />
    </Toolbar>
);

const SubscriptionTypeCreate = withStyles(formStyles)(({ classes, ...props }) => (
    <Create {...props}>
        <SimpleForm toolbar={<SubscriptionCreateToolbar />}>
            <ReferenceInput label="resources.subscription.fields.user" source="user_id" reference="user" inputValueMatcher={() => null} validate={required()}>
                <AutocompleteInput optionText="email" />
            </ReferenceInput>

            <ReferenceInput
                    label="resources.subscription.fields.type"
                    source="type_id"
                    reference="subscription_type"
                    validate={required()}>
                    <SelectInput optionText="name" />
                </ReferenceInput>

                <BooleanInput
                    label="resources.subscription.fields.is_active"
                    source="is_active"
                />

                <DateInput
                    label="resources.subscription.fields.expiring_at"
                    source="expiring_at"
                />
                </SimpleForm>

            </Create>
));

export default SubscriptionTypeCreate;
