import { withStyles } from '@material-ui/core/styles';
import React, { Children, cloneElement } from 'react';
import {
    Datagrid,
    DateField,
    NumberField,
    EditButton,
    Filter,
    List,
    Responsive,
    SearchInput,
    DeleteButton,
    SimpleList,
    TextField,
    ReferenceArrayField,
    SingleFieldList,
    ChipField,
    ReferenceInput,
    SelectInput,
    BooleanField,
    ReferenceField,
    DateTimeInput,
    DateInput,
    AutocompleteInput
} from 'react-admin'; // eslint-disable-line import/no-unresolved

const listStyles = {};

const SubscriptionTypeFilter = props => (
    <Filter {...props}>
        <SearchInput source="q" alwaysOn />
        <ReferenceInput label="resources.subscription.fields.user" source="user_id" reference="user" inputValueMatcher={() => null} alwaysOn>
            <AutocompleteInput optionText="email" />
        </ReferenceInput>
        <DateInput label="resources.subscription.fields.expiring_at_start" source="expiring_at-start"  alwaysOn/>
        <DateInput label="resources.subscription.fields.expiring_at_end" source="expiring_at-end" alwaysOn/>

    </Filter>
);

const SubscriptionTypeListActionToolbar = withStyles({
    toolbar: {
        alignItems: 'center',
        display: 'flex',
    },
})(({ classes, children, ...props }) => (
    <div className={classes.toolbar}>
        {Children.map(children, button => cloneElement(button, props))}
    </div>
));

const SubscriptionTypeList = withStyles(listStyles)(({ classes, ...props }) => (
    <List
        {...props}
        bulkActionButtons={false}
        filters={<SubscriptionTypeFilter />}
        sort={{ field: 'id', order: 'ASC' }}
    >
        <Responsive
            small={
                <SimpleList
                    primaryText={record => record.title}
                    secondaryText={record => record.code}
                />
            }
            medium={
                <Datagrid>
                    <NumberField source="id" cellClassName={classes.number} />
                    <ReferenceField label="resources.subscription.fields.user" source = "user_id" reference = "user" linkType = {false} >
                        <TextField source = "email" />
                    </ReferenceField>
                    <ReferenceField label="resources.subscription.fields.type" source = "type_id" reference = "subscription_type"  linkType = {false} >
                        <TextField source = "name" />
                    </ReferenceField>

                    <BooleanField label="resources.subscription.fields.is_active"  source="is_active" />
                    <DateField label="resources.subscription.fields.expiring_at"  source="expiring_at" showTime cellClassName={classes.date} />
                    <SubscriptionTypeListActionToolbar>
                        <EditButton />
                        <DeleteButton />
                    </SubscriptionTypeListActionToolbar>
                </Datagrid>
            }
        />
    </List>
));

export default SubscriptionTypeList;
