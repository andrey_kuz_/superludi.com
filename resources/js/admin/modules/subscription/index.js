import SubscriptionEdit from './SubscriptionEdit';
import SubscriptionList from './SubscriptionList';
import SubscriptionCreate from './SubscriptionCreate';

export default {
    create: SubscriptionCreate,
    list: SubscriptionList,
    edit: SubscriptionEdit,
};
