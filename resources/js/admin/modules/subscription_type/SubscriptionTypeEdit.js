import React from 'react';
import {
    Edit,
    FormTab,
    TabbedForm,
    TextInput,
    required,
    translate,
    NumberInput,
    DateInput,
    ReferenceArrayInput,
    SelectArrayInput
} from 'react-admin'; // eslint-disable-line import/no-unresolved

import { withStyles } from '@material-ui/core/styles';

const formStyles = {};

const SubscriptionTypeTitle = translate(({ record, translate }) => (
    <span>
        {record ? translate('subscription_type.edit.name', { name: record.name }) : ''}
    </span>
));

const SubscriptionTypeEdit = withStyles(formStyles)(({ classes, ...props }) => (
    <Edit title={<SubscriptionTypeTitle />} {...props}>
        <TabbedForm>

            <FormTab label="subscription_type.form.general" path="">
                <TextInput
                    source="name"
                    validate={required()}
                    label="resources.subscription_type.fields.name"
                />

                <NumberInput
                    source="period"
                    validate={required()}
                    label="resources.subscription_type.fields.period"
                />

                <NumberInput
                    source="price"
                    validate={required()}
                    label="resources.subscription_type.fields.price"
                />

                <NumberInput
                    source="sale_price"
                    label="resources.subscription_type.fields.sale_price"
                />

                <ReferenceArrayInput label="resources.subscription_type.fields.seasons" reference="season" source="seasons_ids">
                    <SelectArrayInput optionText="title" />
                </ReferenceArrayInput>

                <DateInput source="start_at"
                   label="resources.subscription_type.fields.start_at"
                />

                <DateInput source="end_at"
                   label="resources.subscription_type.fields.end_at"
                />

                <DateInput source="active_at"
                           label="resources.subscription_type.fields.active_at"
                />

            </FormTab>

            <FormTab label="locales.form.ru" path="ru">

                <TextInput
                    source="translations_array.ru.title"
                    validate={required()}
                    label="resources.subscription_type.fields.title"
                />

                <TextInput
                    source="translations_array.ru.order_desc"
                    validate={required()}
                    label="resources.subscription_type.fields.order_desc"
                />

            </FormTab>

            <FormTab label="locales.form.uk" path="uk">

                <TextInput
                    source="translations_array.uk.title"
                    label="resources.subscription_type.fields.title"
                />

                <TextInput
                    source="translations_array.ru.order_desc"
                    validate={required()}
                    label="resources.subscription_type.fields.order_desc"
                />

            </FormTab>

        </TabbedForm>
    </Edit>
));

export default SubscriptionTypeEdit;
