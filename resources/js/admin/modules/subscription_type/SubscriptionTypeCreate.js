import React from 'react';
import {
    Create,
    SaveButton,
    SimpleForm,
    TextInput,
    Toolbar,
    required,
    NumberInput,
    DateInput
} from 'react-admin'; // eslint-disable-line import/no-unresolved
import { withStyles } from '@material-ui/core/styles';

const SubscriptionTypeCreateToolbar = props => (
    <Toolbar {...props}>
        <SaveButton
            label="general.action.save"
            redirect="edit"
            submitOnEnter={true}
        />
    </Toolbar>
);

const formStyles = {};

const SubscriptionTypeCreate = withStyles(formStyles)(({ classes, ...props }) => (
    <Create {...props}>
        <SimpleForm toolbar={<SubscriptionTypeCreateToolbar />}>

            <TextInput
                source="name"
                validate={required()}
                label="resources.subscription_type.fields.name"
            />

            <NumberInput
                source="period"
                validate={required()}
                label="resources.subscription_type.fields.period"
            />

            <NumberInput
                source="price"
                validate={required()}
                label="resources.subscription_type.fields.price"
            />

            <TextInput
                source="translations_array.ru.title"
                validate={required()}
                label="resources.subscription_type.fields.title"
            />

            <DateInput source="start_at"
               label="resources.subscription_type.fields.start_at"
            />

            <DateInput source="end_at"
               label="resources.subscription_type.fields.end_at"
            />

            <DateInput source="active_at"
               label="resources.subscription_type.fields.active_at"
            />

        </SimpleForm>
    </Create>
));

export default SubscriptionTypeCreate;
