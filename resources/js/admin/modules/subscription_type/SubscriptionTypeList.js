import { withStyles } from '@material-ui/core/styles';
import React, { Children, cloneElement } from 'react';
import {
    Datagrid,
    DateField,
    NumberField,
    EditButton,
    Filter,
    List,
    Responsive,
    SearchInput,
    DeleteButton,
    SimpleList,
    TextField,
    ReferenceArrayField,
    SingleFieldList,
    ChipField,
    ReferenceInput,
    SelectInput
} from 'react-admin'; // eslint-disable-line import/no-unresolved

const listStyles = {};

const SubscriptionTypeFilter = props => (
    <Filter {...props}>
        <ReferenceInput
            label="resources.subscription_type.fields.seasons"
            source="season_id"
            reference="season" alwaysOn
        >
            <SelectInput optionText="title" />
        </ReferenceInput>
        <SearchInput source="q" alwaysOn />
    </Filter>
);

const SubscriptionTypeListActionToolbar = withStyles({
    toolbar: {
        alignItems: 'center',
        display: 'flex',
    },
})(({ classes, children, ...props }) => (
    <div className={classes.toolbar}>
        {Children.map(children, button => cloneElement(button, props))}
    </div>
));

const SubscriptionTypeList = withStyles(listStyles)(({ classes, ...props }) => (
    <List
        {...props}
        bulkActionButtons={false}
        filters={<SubscriptionTypeFilter />}
        sort={{ field: 'id', order: 'ASC' }}
    >
        <Responsive
            small={
                <SimpleList
                    primaryText={record => record.title}
                    secondaryText={record => record.code}
                />
            }
            medium={
                <Datagrid rowClick="edit">
                    <NumberField source="id" cellClassName={classes.number} />
                    <TextField source="name" sortable={false}/>
                    <TextField source="title" sortable={false}/>
                    <ReferenceArrayField
                        label="resources.subscription_type.fields.seasons"
                        reference="season"
                        source="seasons_ids"
                        cellClassName={classes.hiddenOnSmallScreens}
                        headerClassName={classes.hiddenOnSmallScreens}
                    >
                        <SingleFieldList>
                            <ChipField source="title" />
                        </SingleFieldList>
                    </ReferenceArrayField>
                    <DateField source="start_at" cellClassName={classes.date} />
                    <DateField source="end_at" cellClassName={classes.date} />
                    <DateField source="active_at" cellClassName={classes.date} />
                    <SubscriptionTypeListActionToolbar>
                        <EditButton />
                        <DeleteButton />
                    </SubscriptionTypeListActionToolbar>
                </Datagrid>
            }
        />
    </List>
));

export default SubscriptionTypeList;
