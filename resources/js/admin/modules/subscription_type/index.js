import SubscriptionTypeCreate from './SubscriptionTypeCreate';
import SubscriptionTypeEdit from './SubscriptionTypeEdit';
import SubscriptionTypeList from './SubscriptionTypeList';

export default {
    list: SubscriptionTypeList,
    create: SubscriptionTypeCreate,
    edit: SubscriptionTypeEdit,
};
