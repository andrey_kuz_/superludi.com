import React from 'react';
import {
    Create,
    SaveButton,
    SimpleForm,
    TextInput,
    Toolbar,
    required
} from 'react-admin'; // eslint-disable-line import/no-unresolved
import { withStyles } from '@material-ui/core/styles';

const UserCreateToolbar = props => (
    <Toolbar {...props}>
        <SaveButton
            label="general.action.save"
            redirect="edit"
            submitOnEnter={true}
        />
    </Toolbar>
);

const createStyles = {
    firstName: { display: 'inline-block' },
    lastName: { display: 'inline-block', marginLeft: 32 },
};

const UserCreate = withStyles(createStyles)(({ classes, ...props }) => (
    <Create {...props}>
        <SimpleForm toolbar={<UserCreateToolbar />}>
            <TextInput
                source="email"
                type="email"
                validate={required()}
            />
            <TextInput
                source="password"
                type="password"
                validate={required()}
            />
        </SimpleForm>
    </Create>
));

export default UserCreate;
