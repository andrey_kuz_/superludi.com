import React from 'react';
import {
    DisabledInput,
    Edit,
    FormTab,
    TabbedForm,
    TextInput,
    required,
    translate,
    ReferenceArrayInput,
    CheckboxGroupInput,
} from 'react-admin'; // eslint-disable-line import/no-unresolved

import { withStyles } from '@material-ui/core/styles';

const UserTitle = translate(({ record, translate }) => (
    <span>
        {record ? translate('user.edit.title', { title: record.email }) : ''}
    </span>
));

const editStyles = {
    firstName: { display: 'inline-block' },
    lastName: { display: 'inline-block', marginLeft: 32 },
};

const UserEdit = withStyles(editStyles)(({ classes, ...props }) => (
    <Edit title={<UserTitle />} {...props}>
        <TabbedForm>
            <FormTab label="user.form.summary" path="">
                <DisabledInput source="id" />
                <TextInput
                    source="email"
                    type="email"
                    validate={required()}
                />

                <ReferenceArrayInput
                    fullWidth={true}
                    label="resources.user.fields.role"
                    source="roles_ids"
                    reference="role"
                    validate={required()}
                >
                    <CheckboxGroupInput optionText="name" optionValue="id" />
                </ReferenceArrayInput>
            </FormTab>

            <FormTab label="user.form.security" path="security">
                <TextInput
                    source="password"
                    type="password"
                    label="resources.user.fields.password"
                />

                <TextInput
                    source="password_confirm"
                    label="resources.user.fields.password_confirm"
                    type="password"
                />
            </FormTab>
            <FormTab label="user.form.profile" path="profile">
                <TextInput
                    source="info.full_name"
                    label="resources.user.fields.full_name"
                />
                <TextInput
                    source="info.city"
                    label="resources.user.fields.city"
                />
                <TextInput
                    source="info.phone"
                    label="resources.user.fields.phone"
                    validate={required()}
                />
            </FormTab>
        </TabbedForm>
    </Edit>
));

export default UserEdit;
