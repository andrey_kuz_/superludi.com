import LocationOnIcon from '@material-ui/icons/LocationOn';
import { withStyles } from '@material-ui/core/styles';
import React, { Children, cloneElement } from 'react';
import {
    Datagrid,
    DateField,
    NumberField,
    EditButton,
    Filter,
    List,
    Responsive,
    SearchInput,
    DeleteButton,
    SimpleList,
    TextField,
    ReferenceArrayField,
    SingleFieldList,
    ChipField
} from 'react-admin'; // eslint-disable-line import/no-unresolved

const UserFilter = props => (
    <Filter {...props}>
        <SearchInput source="q" alwaysOn />
    </Filter>
);

const styles = theme => ({
    number: {
        width: '4em'
    },
    hiddenOnSmallScreens: {
        [theme.breakpoints.down('md')]: {
            display: 'none',
        },
    },
    date: {
        width: '20em',
        fontStyle: 'italic'
    },
});

const UserListActionToolbar = withStyles({
    toolbar: {
        alignItems: 'center',
        display: 'flex',
    },
})(({ classes, children, ...props }) => (
    <div className={classes.toolbar}>
        {Children.map(children, button => cloneElement(button, props))}
    </div>
));

const UserList = withStyles(styles)(({ classes, ...props }) => (
    <List
        {...props}
        bulkActionButtons={false}
        filters={<UserFilter />}
        sort={{ field: 'id', order: 'ASC' }}
    >
        <Responsive
            small={
                <SimpleList
                    primaryText={record => record.title}
                    secondaryText={record => record.code}
                />
            }
            medium={
                <Datagrid rowClick="edit">
                    <NumberField source="id" cellClassName={classes.number} />
                    <TextField source="name" />
                    <TextField source="email" />
                    <ReferenceArrayField
                        reference="role"
                        source="roles_ids"
                    >
                        <SingleFieldList>
                            <ChipField source="name" />
                        </SingleFieldList>
                    </ReferenceArrayField>
                    <DateField source="created_at" showTime cellClassName={classes.date} />
                    <UserListActionToolbar>
                        <EditButton />
                        <DeleteButton />
                    </UserListActionToolbar>
                </Datagrid>
            }
        />
    </List>
));

export default UserList;
