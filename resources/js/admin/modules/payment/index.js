import Payment from '@material-ui/icons/Payment';
import PaymentList from './PaymentList';
import PaymentShow from './PaymentShow';

export default {
    list: PaymentList,
    show: PaymentShow,
    icon: Payment,
};
