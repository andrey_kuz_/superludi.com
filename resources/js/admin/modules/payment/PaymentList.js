import LocationOnIcon from '@material-ui/icons/LocationOn';
import { withStyles } from '@material-ui/core/styles';
import React, { Children, cloneElement } from 'react';
import {
    Datagrid,
    DateField,
    NumberField,
    Filter,
    List,
    Responsive,
    SearchInput,
    SimpleList,
    TextField,
    ShowButton,
    ReferenceField,
    ReferenceInput,
    SelectInput
} from 'react-admin'; // eslint-disable-line import/no-unresolved

const listStyles = {};

const PaymentFilter = props => (
    <Filter {...props}>
        <ReferenceInput
            label="resources.payment.fields.status_id"
            source="status_id"
            reference="payment_status" alwaysOn
        >
            <SelectInput optionText="name" />
        </ReferenceInput>
        <SearchInput source="q" alwaysOn />
    </Filter>
);

const PaymentActionToolbar = withStyles({
    toolbar: {
        alignItems: 'center',
        display: 'flex',
    },
})(({ classes, children, ...props }) => (
    <div className={classes.toolbar}>
        {Children.map(children, button => cloneElement(button, props))}
    </div>
));

const PaymentList = withStyles(listStyles)(({ classes, ...props }) => (
    <List
        {...props}
        bulkActionButtons={false}
        filters={<PaymentFilter />}
        sort={{ field: 'id', order: 'ASC' }}
    >
        <Responsive
            small={
                <SimpleList
                    primaryText={record => record.name}
                    secondaryText={record => record.title}
                />
            }
            medium={
                <Datagrid>
                    <NumberField source="id" cellClassName={classes.number} />
                    <ReferenceField label="resources.payment.fields.user_id" source="user_id" reference="user" sortable={false}>
                        <TextField source="email" />
                    </ReferenceField>
                    <ReferenceField label="resources.payment.fields.status_id" source="status_id" reference="payment_status" sortable={false} allowEmpty>
                        <TextField source="name" />
                    </ReferenceField>
                    <DateField source="created_at" showTime cellClassName={classes.date} />
                    <PaymentActionToolbar>
                        <ShowButton />
                    </PaymentActionToolbar>
                </Datagrid>
            }
        />
    </List>
));

export default PaymentList;
