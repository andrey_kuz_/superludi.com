import React from 'react';
import {
    Show,
    SimpleShowLayout,
    TextField,
    DateField,
    RichTextField,
    translate,
    ReferenceField
} from 'react-admin';

const PaymentTitle = translate(({ record, translate }) => (
    <span>
        {record ? translate('payment.show.id', { id: record.id }) : ''}
    </span>
));

const PaymentShow = (props) => (
    <Show title={<PaymentTitle />} {...props}>
        <SimpleShowLayout>
            <DateField label="resources.payment.fields.paid_at" source="paid_at" />
            <ReferenceField label="resources.payment.fields.user_id" source="user_id" reference="user">
                <TextField source="email" />
            </ReferenceField>
            <ReferenceField label="resources.payment.fields.status_id" source="status_id" reference="payment_status" allowEmpty>
                <TextField source="title" />
            </ReferenceField>
            <RichTextField label="resources.payment.fields.comment" source="comment" />
        </SimpleShowLayout>
    </Show>
);

export default PaymentShow;