import React from 'react';
import {
    Create,
    SaveButton,
    SimpleForm,
    TextInput,
    NumberInput,
    LongTextInput,
    ReferenceInput,
    SelectInput,
    Toolbar,
    required,
    BooleanInput,
} from 'react-admin'; // eslint-disable-line import/no-unresolved
import { withStyles } from '@material-ui/core/styles';

const BlogTagCreateToolbar = props => (
    <Toolbar {...props}>
        <SaveButton
            label="general.action.save"
            redirect="edit"
            submitOnEnter={true}
        />
    </Toolbar>
);

const formStyles = {};

const BlogTagCreate = withStyles(formStyles)(({ classes, ...props }) => (
    <Create {...props}>
        <SimpleForm toolbar={<BlogTagCreateToolbar />}>
            <TextInput
                source="translations_array.ru.title"
                validate={required()}
                label="resources.blog_tag.fields.title"
            />
            <TextInput
                source="slug"
                validate={required()}
                label="resources.blog_tag.fields.slug"
            />
            </SimpleForm>
    </Create>
));

export default BlogTagCreate;
