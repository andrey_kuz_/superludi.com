import LocationOnIcon from '@material-ui/icons/LocationOn';
import { withStyles } from '@material-ui/core/styles';
import React, { Children, cloneElement } from 'react';
import {
    Datagrid,
    DateField,
    NumberField,
    EditButton,
    Filter,
    List,
    Responsive,
    SearchInput,
    DeleteButton,
    SimpleList,
    TextField,
    BooleanField,
} from 'react-admin'; // eslint-disable-line import/no-unresolved

const listStyles = {};

const TagFilter = props => (
    <Filter {...props}>

        <SearchInput source="q" alwaysOn />
    </Filter>
);

const BlogTagListActionToolbar = withStyles({
    toolbar: {
        alignItems: 'center',
        display: 'flex',
    },
})(({ classes, children, ...props }) => (
    <div className={classes.toolbar}>
        {Children.map(children, button => cloneElement(button, props))}
    </div>
));

const BlogTagList = withStyles(listStyles)(({ classes, ...props }) => (
    <List
        {...props}
        bulkActionButtons={false}
        filters={<TagFilter />}
        sort={{ field: 'id', order: 'ASC' }}
    >
        <Responsive
            small={
                <SimpleList
                    secondaryText={record => record.code}
                />
            }
            medium={
                <Datagrid rowClick="edit">
                    <NumberField source="id" cellClassName={classes.number} />
                    <TextField source="slug" />
                    <TextField source="translations_array.ru.title"  label="resources.blog_tag.fields.title"/>
                    <DateField source="created_at" showTime cellClassName={classes.date} />
                    <BlogTagListActionToolbar>
                        <EditButton />
                        <DeleteButton />
                    </BlogTagListActionToolbar>
                </Datagrid>
            }
        />
    </List>
));

export default BlogTagList;
