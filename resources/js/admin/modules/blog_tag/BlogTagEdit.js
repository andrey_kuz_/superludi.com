import React from 'react';
import {
    Edit,
    FormTab,
    TabbedForm,
    TextInput,
    BooleanInput,
    NumberInput,
    LongTextInput,
    ImageInput,
    ImageField,
    required,
    translate,
    TextField,
    ReferenceInput,
    SelectInput,
} from 'react-admin'; // eslint-disable-line import/no-unresolved

import RichTextInput from 'ra-input-rich-text';

import { withStyles } from '@material-ui/core/styles';

const formStyles = {};

const UserTitle = translate(({ record, translate }) => (
    <span>
        {record ? translate('blog_tag.edit.title', { title: record.title }) : ''}
    </span>
));

const BlogTagEdit = withStyles(formStyles)(({ classes, ...props }) => (
    <Edit title={<UserTitle />} {...props}>
        <TabbedForm>
            <FormTab label="course.form.general" path="">
                <TextInput
                    source="slug"
                    validate={required()}
                    label="resources.blog_tag.fields.slug"
                />
            </FormTab>
            <FormTab label="locales.form.ru" path="ru">
                <TextInput
                    source="translations_array.ru.title"
                    validate={required()}
                    label="resources.blog_tag.fields.title"
                />
                <LongTextInput
                    source="translations_array.ru.description"
                    label="resources.course.fields.description"
                />
                <LongTextInput
                    source="translations_array.ru.keywords"
                    label="resources.course.fields.keywords"
                />
            </FormTab>
            <FormTab label="locales.form.uk" path="uk">
                <TextInput
                    source="translations_array.uk.title"
                    label="resources.blog_tag.fields.title"
                />

                <LongTextInput
                    source="translations_array.ru.description"
                    label="resources.course.fields.description"
                />
                <LongTextInput
                    source="translations_array.ru.keywords"
                    label="resources.course.fields.keywords"
                />
            </FormTab>
        </TabbedForm>
    </Edit>
));

export default BlogTagEdit;
