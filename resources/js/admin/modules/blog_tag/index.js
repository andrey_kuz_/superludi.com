import TagFaces from '@material-ui/icons/TagFaces';
import BlogTagCreate from './BlogTagCreate';
import BlogTagEdit from './BlogTagEdit';
import BlogTagList from './BlogTagList';


export default {
    list: BlogTagList,
    create: BlogTagCreate,
    edit: BlogTagEdit,
    icon: TagFaces,
};
