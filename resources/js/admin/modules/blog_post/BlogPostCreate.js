import React from 'react';
import {
    Create,
    SaveButton,
    SimpleForm,
    TextInput,
    NumberInput,
    LongTextInput,
    ReferenceInput,
    SelectInput,
    Toolbar,
    required,
    BooleanInput,
} from 'react-admin'; // eslint-disable-line import/no-unresolved
import { withStyles } from '@material-ui/core/styles';

const BlogPostCreateToolbar = props => (
    <Toolbar {...props}>
        <SaveButton
            label="general.action.save"
            redirect="edit"
            submitOnEnter={true}
        />
    </Toolbar>
);

const formStyles = {};

const BlogPostCreate = withStyles(formStyles)(({ classes, ...props }) => (
    <Create {...props}>
        <SimpleForm toolbar={<BlogPostCreateToolbar />}>
            <TextInput
                source="translations_array.ru.title"
                validate={required()}
                label="resources.blog_post.fields.title"
            />
            <TextInput
                source="translations_array.ru.author_name"
                validate={required()}
                label="resources.blog_post.fields.author"
            />
            <TextInput
                source="slug"
                validate={required()}
                label="resources.blog_post.fields.slug"
            />
            <ReferenceInput
                label="resources.blog_post.fields.category"
                source="category_id"
                reference="blog_category"
                validate={required()}>
                <SelectInput optionText="title" />
            </ReferenceInput>

            </SimpleForm>
    </Create>
));

export default BlogPostCreate;
