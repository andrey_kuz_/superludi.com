import React from 'react';
import {
    Edit,
    FormTab,
    TabbedForm,
    TextInput,
    BooleanInput,
    NumberInput,
    LongTextInput,
    ImageInput,
    ImageField,
    required,
    translate,
    TextField,
    ReferenceInput,
    SelectInput,
    AutocompleteArrayInput,
    ReferenceArrayInput,
    SelectArrayInput,
} from 'react-admin'; // eslint-disable-line import/no-unresolved

import TinyMCE from "./../../fields/TinyMCE";
import "tinymce/plugins/code/plugin";
import "tinymce/plugins/link/plugin";
import "tinymce/plugins/image/plugin";
import "tinymce/plugins/media/plugin";
import "tinymce/plugins/template/plugin";

import { withStyles } from '@material-ui/core/styles';

const formStyles = {};

const UserTitle = translate(({ record, translate }) => (
    <span>
        {record ? translate('course.edit.title', { title: record.title }) : ''}
    </span>
));

const BlogPostEdit = withStyles(formStyles)(({ classes, ...props }) => (
    <Edit title={<UserTitle />} {...props}>
        <TabbedForm>
            <FormTab label="course.form.general" path="">
                <ReferenceInput
                    label="resources.blog_post.fields.category"
                    source="category_id"
                    reference="blog_category"
                    validate={required()}>
                    <SelectInput optionText="title" />
                </ReferenceInput>
                <TextInput
                    source="slug"
                    validate={required()}
                    label="resources.blog_post.fields.slug"
                />
                <ReferenceArrayInput label="resources.blog_post.fields.tags" reference="blog_tag" source="tags_ids">
                    <SelectArrayInput optionText="title" />
                </ReferenceArrayInput>

                <BooleanInput
                    source="is_published"
                    validate={required()}
                />
            </FormTab>
            <FormTab label="course.form.media" path="">
                <ImageInput source="thumbnail_image" label="resources.blog_post.fields.image_thumbnail" accept="image/*">
                    <ImageField source="src" title="title" />
                </ImageInput>
                <ImageInput source="hero_image" label="resources.blog_post.fields.hero_image" accept="image/*">
                    <ImageField source="src" title="title" />
                </ImageInput>
            </FormTab>
            <FormTab label="locales.form.ru" path="ru">
                <TextInput
                    source="translations_array.ru.title"
                    validate={required()}
                    label="resources.blog_post.fields.title"
                />
                <TextInput
                    source="translations_array.ru.author_name"
                    validate={required()}
                    label="resources.blog_post.fields.author"
                />
                <LongTextInput
                    source="translations_array.ru.description"
                    label="resources.blog_post.fields.description"
                />

                <TinyMCE
                    source="translations_array.ru.content"
                    label="resources.blog_post.fields.content"
                    init={{
                        plugins: [
                            "link",
                            "image",
                            "code",
                            "insertdatetime media table contextmenu paste",
                            "media",
                            "template"
                            ],
                        toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | bullist numlist | code | link image | media | template',
                        templates: [
                            {title: 'Добавить блок', description: 'Добавить блок с фото и описанием', content: '<div class="content-block"><div class="content-block__text"><p>Замените на текст</p></div><div class="content-block__photo rellax" data-rellax-speed="-1" data-rellax-percentage="0.9"><img src="введите правильную ссылку на изображение" alt="image" ></div> </div>'},
                            {title: 'Добавить цитату', description: 'Добавить блок с цитатой', content: '<div class="content-quote"><div class="content-quote__text"><p>Введите текст цитаты</p></div><div class="content-quote__autor"><p>Введите имя автора</p></div> </div>'}
                        ]
                    }}
                    validation={{ required: true }}
                />

                <LongTextInput
                    source="translations_array.ru.keywords"
                    label="resources.course.fields.keywords"
                />
            </FormTab>
            <FormTab label="locales.form.uk" path="uk">
                <TextInput
                    source="translations_array.uk.title"
                    label="resources.blog_post.fields.title"
                />
                <TextInput
                    source="translations_array.uk.author_name"
                    label="resources.blog_post.fields.author"
                />
                <LongTextInput
                    source="translations_array.uk.description"
                    label="resources.blog_post.fields.description"
                />

                <TinyMCE
                    source="translations_array.uk.content"
                    label="resources.blog_post.fields.content"
                    init={{
                        plugins: [
                            "link",
                            "image",
                            "code",
                            "insertdatetime media table contextmenu paste",
                            "media",
                            "template"
                        ],
                        toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | bullist numlist | code | link image | media | template',
                        templates: [
                            {title: 'Добавить блок', description: 'Добавить блок с фото и описанием', content: '<div class="content-block"><div class="content-block__text"><p>Замените на текст</p></div><div class="content-block__photo"><img src="введите правильную ссылку на изображение" alt="image" ></div> </div>'},
                            {title: 'Добавить цитату', description: 'Добавить блок с цитатой', content: '<div class="content-quote"><div class="content-quote__text"><p>Введите текст цитаты</p></div><div class="content-quote__autor"><p>Введите имя автора</p></div> </div>'}
                        ]
                    }}
                    validation={{ required: true }}
                />

                <LongTextInput
                    source="translations_array.ru.keywords"
                    label="resources.course.fields.keywords"
                />
            </FormTab>
        </TabbedForm>
    </Edit>
));

export default BlogPostEdit;
