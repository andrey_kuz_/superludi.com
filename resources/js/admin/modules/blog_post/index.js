import Book from '@material-ui/icons/Book';
import BlogPostCreate from './BlogPostCreate';
import BlogPostEdit from './BlogPostEdit';
import BlogPostList from './BlogPostList';

export default {
    list: BlogPostList,
    create: BlogPostCreate,
    edit: BlogPostEdit,
    icon: Book,
};
