import React from 'react';
import {
    Edit,
    FormTab,
    TabbedForm,
    TextInput,
    BooleanInput,
    NumberInput,
    LongTextInput,
    ImageInput,
    ImageField,
    required,
    translate,
} from 'react-admin'; // eslint-disable-line import/no-unresolved

import { withStyles } from '@material-ui/core/styles';

const formStyles = {};

const BlogCategoryTitle = translate(({ record, translate }) => (
    <span>
        {record ? translate('resources.blog_category.name', { title: record.title }) : ''}
    </span>
));

const BlogCategoryEdit = withStyles(formStyles)(({ classes, ...props }) => (
    <Edit title={<BlogCategoryTitle />} {...props}>
        <TabbedForm>
            <FormTab label="resources.blog_category.fields.slug" path="">
                <TextInput
                    source="slug"
                    validate={required()}
                    label="resources.blog_category.fields.slug"
                />
            </FormTab>

            <FormTab label="locales.form.ru" path="ru">
                <TextInput
                    source="translations_array.ru.title"
                    validate={required()}
                    label="resources.blog_category.fields.title"
                />

                <LongTextInput
                    source="translations_array.ru.description"
                    label="resources.course.fields.description"
                />
                <LongTextInput
                    source="translations_array.ru.keywords"
                    label="resources.course.fields.keywords"
                />
            </FormTab>

            <FormTab label="locales.form.uk" path="uk">
                <TextInput
                    source="translations_array.uk.title"
                    label="resources.blog_category.fields.title"
                />

                <LongTextInput
                    source="translations_array.ru.description"
                    label="resources.course.fields.description"
                />
                <LongTextInput
                    source="translations_array.ru.keywords"
                    label="resources.course.fields.keywords"
                />
            </FormTab>
        </TabbedForm>
    </Edit>
));

export default BlogCategoryEdit;
