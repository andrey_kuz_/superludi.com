import React from 'react';
import {
    Create,
    SaveButton,
    SimpleForm,
    TextInput,
    NumberInput,
    LongTextInput,
    Toolbar,
    required,
} from 'react-admin'; // eslint-disable-line import/no-unresolved
import { withStyles } from '@material-ui/core/styles';

const BlogCategoryCreateToolbar = props => (
    <Toolbar {...props}>
        <SaveButton
            label="general.action.save"
            redirect="edit"
            submitOnEnter={true}
        />
    </Toolbar>
);

const formStyles = {};

const BlogCategoryCreate = withStyles(formStyles)(({ classes, ...props }) => (
    <Create {...props}>
        <SimpleForm toolbar={<BlogCategoryCreateToolbar />}>
            <TextInput
                source="translations_array.ru.title"
                validate={required()}
                label="resources.blog_category.fields.title"
            />
            <TextInput
                source="slug"
                validate={required()}
                label="resources.blog_category.fields.slug"
            />
        </SimpleForm>
    </Create>
));

export default BlogCategoryCreate;
