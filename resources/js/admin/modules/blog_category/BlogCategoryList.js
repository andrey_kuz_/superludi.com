import LocationOnIcon from '@material-ui/icons/LocationOn';
import { withStyles } from '@material-ui/core/styles';
import React, { Children, cloneElement } from 'react';
import {
    Datagrid,
    DateField,
    NumberField,
    EditButton,
    Filter,
    List,
    Responsive,
    SearchInput,
    DeleteButton,
    SimpleList,
    TextField,
    BooleanField
} from 'react-admin'; // eslint-disable-line import/no-unresolved

const listStyles = {};

const BlogCategoryFilter = props => (
    <Filter {...props}>
        <SearchInput source="q" alwaysOn />
    </Filter>
);

const BlogCategoryListActionToolbar = withStyles({
    toolbar: {
        alignItems: 'center',
        display: 'flex',
    },
})(({ classes, children, ...props }) => (
    <div className={classes.toolbar}>
        {Children.map(children, button => cloneElement(button, props))}
    </div>
));

const BlogCategoryList = withStyles(listStyles)(({ classes, ...props }) => (
    <List
        {...props}
        bulkActionButtons={false}
        filters={<BlogCategoryFilter />}
        sort={{ field: 'id', order: 'ASC' }}
    >
        <Responsive
            small={
                <SimpleList
                    primaryText={record => record.title}
                    secondaryText={record => record.code}
                />
            }
            medium={
                <Datagrid rowClick="edit">
                    <NumberField source="id" cellClassName={classes.number} />
                    <TextField source="slug" />
                    <TextField source="translations_array.ru.title"  label="resources.blog_category.fields.title"/>
                    <BlogCategoryListActionToolbar>
                        <EditButton />
                        <DeleteButton />
                    </BlogCategoryListActionToolbar>
                </Datagrid>
            }
        />
    </List>
));

export default BlogCategoryList;
