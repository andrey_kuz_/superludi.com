import Bookmark from '@material-ui/icons/Bookmark';
import BlogCategoryCreate from './BlogCategoryCreate';
import BlogCategoryEdit from './BlogCategoryEdit';
import BlogCategoryList from './BlogCategoryList';

export default {
    list: BlogCategoryList,
    create: BlogCategoryCreate,
    edit: BlogCategoryEdit,
    icon: Bookmark,
};
