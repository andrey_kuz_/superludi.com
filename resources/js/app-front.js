import './libs/jquery';
import './libs/cookies';
import './libs/jquery.mask';

import './front/foundation';
import './front/loader';
import './front/csrf';
import './front/actions';
import './front/accordion';
import './front/sliders';
import './front/scroll';
import './front/rellax';
import './front/register';
import './front/login';
import './front/auth';
import './front/course_page';
import './front/sort_courses';
import './front/profile';
import './front/subscribe';
import './front/comments';
import './front/logout';
import './front/init';
import './front/course_promo';
import './front/forgot_password';
import './front/blog';
import './front/form';


