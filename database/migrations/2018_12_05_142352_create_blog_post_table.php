<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogPostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_post', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->string('slug')->unique();
            $table->string('thumbnail_image_url');
            $table->boolean('is_published');

            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('blog_category')->onDelete('cascade');
        });

        Schema::create('blog_post_translation', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('post_id')->unsigned();
            $table->string('title')->nullable()->index();
            $table->string('author_name')->nullable()->index();
            $table->text('description')->nullable();
            $table->longText('content')->nullable();
            $table->string('locale')->index();

            $table->unique(['post_id', 'locale']);
            $table->foreign('post_id')->references('id')->on('blog_post')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_post_translation');

        Schema::dropIfExists('blog_post');
    }
}
