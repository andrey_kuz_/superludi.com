<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCourseIdToCourseLessonMaterial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('course_lesson_material', function (Blueprint $table) {

            $table->integer('course_lesson_id')->unsigned()->nullable()->change();

            $table->integer('course_id')->unsigned()->nullable()->after('id');
            $table->foreign('course_id')->references('id')->on('course')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_lesson_material', function (Blueprint $table) {

            $table->dropIndex('course_lesson_material_course_id_foreign');
            $table->dropColumn('course_id');

            $table->integer('course_lesson_id')->unsigned()->change();
        });
    }
}
