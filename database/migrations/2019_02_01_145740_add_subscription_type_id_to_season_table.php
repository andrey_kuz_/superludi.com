<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubscriptionTypeIdToSeasonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('season', function (Blueprint $table) {
            $table->integer('subscription_type_id')->unsigned()->nullable()->after('id');
            $table->foreign('subscription_type_id')->references('id')->on('subscription_type')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('season', function (Blueprint $table) {
            $table->dropColumn('subscription_type_id');
        });
    }
}
