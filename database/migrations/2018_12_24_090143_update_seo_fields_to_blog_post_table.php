<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSeoFieldsToBlogPostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('blog_post_translation', function (Blueprint $table) {
            $table->renameColumn('seo_keywords', 'keywords');

            $table->dropColumn('seo_title');
            $table->dropColumn('seo_description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('blog_post_translation', function (Blueprint $table) {
            $table->renameColumn('keywords', 'seo_keywords');

            $table->string('seo_title');
            $table->text('seo_description');
        });
    }
}
