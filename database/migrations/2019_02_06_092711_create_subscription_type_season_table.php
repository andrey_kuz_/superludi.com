<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionTypeSeasonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription_type_season', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subscription_type_id')->unsigned();
            $table->integer('season_id')->unsigned();
            $table->foreign('subscription_type_id')->references('id')->on('subscription_type');
            $table->foreign('season_id')->references('id')->on('season');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription_type_season');
    }
}
