<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropSubscriptionTypeIdFromSeasonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('season', function (Blueprint $table) {
            $table->dropForeign('season_subscription_type_id_foreign');
            $table->dropColumn('subscription_type_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('season', function (Blueprint $table) {
            //
        });
    }
}
