<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->integer('period');
            $table->decimal('price', 8,2);
            $table->timestamps();
        });

        Schema::create('subscription_type_translation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subscription_type_id')->unsigned();
            $table->string('title')->nullable()->index();
            $table->string('locale')->index();

            $table->unique(['subscription_type_id', 'locale']);
            $table->foreign('subscription_type_id')->references('id')->on('subscription_type')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription_type_translation');

        Schema::dropIfExists('subscription_type');

    }
}
