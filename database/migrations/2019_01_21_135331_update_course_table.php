<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCourseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('course', function (Blueprint $table) {
            $table->dropColumn('speaker_image_url');
        });

        Schema::table('course_translation', function (Blueprint $table) {
            $table->dropColumn('speaker_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course', function (Blueprint $table) {
            $table->string('speaker_image_url')->nullable();
        });

        Schema::table('course_translation', function (Blueprint $table) {
            $table->string('speaker_name')->nullable()->index();
        });
    }
}
