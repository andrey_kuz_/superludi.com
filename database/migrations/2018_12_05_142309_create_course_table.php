<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course', function (Blueprint $table) {

            $table->increments('id');
            $table->decimal('price', 8, 2);
            $table->string('speaker_image_url')->nullable();
            $table->string('banner_image_url')->nullable();
            $table->text('banner_video_code')->nullable();
            $table->boolean('is_published')->default(0);

            $table->timestamps();
        });

        Schema::create('course_translation', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('course_id')->unsigned();
            $table->string('title')->nullable()->index();
            $table->string('speaker_name')->nullable()->index();
            $table->text('description')->nullable();
            $table->string('locale')->index();

            $table->unique(['course_id', 'locale']);
            $table->foreign('course_id')->references('id')->on('course')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_translation');

        Schema::dropIfExists('course');
    }
}
