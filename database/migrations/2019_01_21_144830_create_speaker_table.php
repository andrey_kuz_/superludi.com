<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpeakerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('speaker', function (Blueprint $table) {
            $table->increments('id');
            $table->string('avatar_url')->nullable();

            $table->timestamps();
        });

        Schema::create('speaker_translation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('speaker_id')->unsigned();
            $table->string('name')->nullable()->index();
            $table->string('locale')->index();

            $table->unique(['speaker_id', 'locale']);
            $table->foreign('speaker_id')->references('id')->on('speaker')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('speaker_translation');
        Schema::dropIfExists('speaker');
    }
}
