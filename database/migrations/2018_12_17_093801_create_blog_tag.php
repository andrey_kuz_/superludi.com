<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogTag extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_tag', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->unique();
            $table->timestamps();
        });

        Schema::create('blog_tag_translation', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('tag_id')->unsigned();
            $table->string('name')->nullable()->index();
            $table->string('locale')->index();

            $table->unique(['tag_id', 'locale']);
            $table->foreign('tag_id')->references('id')->on('blog_tag')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_tag');
        Schema::dropIfExists('blog_tag_translation');
    }
}
