<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('info', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->timestamps();
        });
        Schema::create('info_translation', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('info_id')->unsigned();
            $table->string('title')->nullable()->index();
            $table->text('description')->nullable();
            $table->text('keywords')->nullable();
            $table->longText('content')->nullable();
            $table->string('locale')->index();

            $table->unique(['info_id', 'locale']);
            $table->foreign('info_id')->references('id')->on('info')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('info_translation');
        Schema::dropIfExists('info');
    }
}
