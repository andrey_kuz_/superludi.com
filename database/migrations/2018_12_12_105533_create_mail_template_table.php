<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailTemplateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mail_template', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->timestamps();
        });

        Schema::create('mail_template_translation', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('mail_id')->unsigned();
            $table->string('subject')->nullable();
            $table->text('content')->nullable();
            $table->string('locale')->index();

            $table->unique(['mail_id', 'locale']);
            $table->foreign('mail_id')->references('id')->on('mail_template')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mail_template');
        Schema::dropIfExists('mail_template_translation');
    }
}
