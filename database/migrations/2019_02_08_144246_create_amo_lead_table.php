<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmoLeadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amo_lead', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lead_id')->nullable()->index();
            $table->boolean('is_synced')->default(0)->index();
            $table->text('data')->nullable();
            $table->integer('leadable_id');
            $table->string('leadable_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amo_lead');
    }
}
