<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSeoFieldsToBlogTagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('blog_tag_translation', function (Blueprint $table) {
            $table->renameColumn('seo_keywords', 'keywords');
            $table->renameColumn('seo_description', 'description');

            $table->dropColumn('seo_title');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('blog_tag_translation', function (Blueprint $table) {
            $table->renameColumn('keywords', 'seo_keywords');
            $table->renameColumn('description', 'seo_description');

            $table->string('seo_title');
        });
    }
}
