<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSocialCountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('social_count', function(Blueprint $table) {
            $table->renameColumn('social_count_table_id', 'socialcountable_id');
            $table->renameColumn('social_count_table_type', 'socialcountable_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('social_count', function(Blueprint $table) {
            $table->renameColumn('socialcountable_id', 'social_count_table_id');
            $table->renameColumn('socialcountable_type', 'social_count_table_type');
        });
    }
}
