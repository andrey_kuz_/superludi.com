<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialCountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('social_count', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('view_count')->unsigned()->default(0);
            $table->integer('share_count')->unsigned()->default(0);
            $table->integer('social_count_table_id');
            $table->string('social_count_table_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('social_count');
    }
}
