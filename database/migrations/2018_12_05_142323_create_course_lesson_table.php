<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseLessonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_lesson', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('course_id')->unsigned();
            $table->text('video_code');
            $table->integer('ord')->unsigned();

            $table->timestamps();

            $table->foreign('course_id')->references('id')->on('course')->onDelete('cascade');
        });

        Schema::create('course_lesson_translation', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('lesson_id')->unsigned();
            $table->string('title')->nullable()->index();
            $table->text('description')->nullable();
            $table->string('locale')->index();

            $table->unique(['lesson_id', 'locale']);
            $table->foreign('lesson_id')->references('id')->on('course_lesson')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_lesson_translation');

        Schema::dropIfExists('course_lesson');
    }
}
