<?php

use Illuminate\Database\Seeder;
use App\MailTemplate;


class MailTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $each = [
            'user_password_reset' => [
                'subject' => 'Reset Password',
                'content' => '<p style="box-sizing: border-box; outline: 0px; margin: 0px; padding: 0px; line-height: inherit; float: none;">&nbsp;</p>
<p style="box-sizing: border-box; outline: 0px; margin: 0px; padding: 0px; line-height: normal; float: none; font-family: \'formular\', sans-serif; font-size: 26px;">Забыли пароль?Не беда.</p>
<p style="box-sizing: border-box; outline: 0px; margin: 0px; padding: 0px; line-height: inherit; float: none;">&nbsp;</p>
<p style="box-sizing: border-box; outline: 0px; margin: 0px; padding: 0px; line-height: normal; float: none; font-family: \'formular\', sans-serif; font-size: 26px;">Мы создали для вас новый пароль, который автоматически активируется, если вы его используете при входе.</p>
<p style="box-sizing: border-box; outline: 0px; margin: 0px; padding: 0px; line-height: inherit; float: none;">&nbsp;</p>
<p style="box-sizing: border-box; outline: 0px; margin: 0px; padding: 0px; line-height: normal; float: none; font-family: \'formular\', sans-serif; font-size: 26px;">{{$password}}</p>
<p style="box-sizing: border-box; outline: 0px; margin: 0px; padding: 0px; line-height: inherit; float: none;">&nbsp;</p>
<p style="box-sizing: border-box; outline: 0px; margin: 0px; padding: 0px; line-height: inherit; float: none;">&nbsp;</p>
            ']
        ];

        foreach ($each as $name => $data) {

            $mail_template = MailTemplate::firstOrNew(['name' => $name]);

            if (!$mail_template->id) {

                $mail_template->name = $name;
                $mail_template->{'subject:ru'} = $data['subject'];
                $mail_template->{'content:ru'} = $data['content'];

                $mail_template->save();
            }
        }
    }
}
