<?php

use Illuminate\Database\Seeder;
use App\Info;
use App\InfoTranslation;

class InfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $slugs = [
            'terms'         => [
                'title' => 'Публичный договор о пользовании услугами компании СУПЕРЛЮДИ'
            ],
            'privacy'       => [
                'title' => 'Политика конфиденциальности компании СУПЕРЛЮДИ'
            ],
            'disclaimer'    => [
                'title' => 'Предупреждение об авторском праве'
            ]
        ];

        foreach ($slugs as $slug => $data) {

            $info =  Info::firstOrNew(['slug' => $slug]);

            $info->slug = $slug;
            $info->{'title:ru'} = $data['title'];

            $info->save();
        }
    }
}
