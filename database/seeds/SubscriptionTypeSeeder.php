<?php

use Illuminate\Database\Seeder;
use App\SubscriptionType;
use Carbon\Carbon;

class SubscriptionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $start_at = Carbon::now();
        $now = Carbon::parse($start_at);
        $each = [
            'period_90' => [
                'title' => '90 дней',
                'period' => '90',
                'price' => 200,
                'start_at' => $start_at,
                'end_at' => $now->addDays(90)->toDateTimeString()
            ],
            'presale' => [
                'title' => 'presale',
                'period' => '90',
                'price' => 200,
                'start_at' => $start_at,
                'end_at' => $now->addDays(90)->toDateTimeString()
            ]
        ];

        foreach ($each as $name => $data) {

            $subscription_type = SubscriptionType::firstOrNew(['name' => $name]);

            if (!$subscription_type->id) {

                $subscription_type->name = $name;
                $subscription_type->{'title:ru'} = $data['title'];
                $subscription_type->period = $data['period'];
                $subscription_type->price = $data['price'];
                $subscription_type->start_at = $data['start_at'];
                $subscription_type->end_at = $data['end_at'];
                $subscription_type->save();
            }
        }
    }
}
