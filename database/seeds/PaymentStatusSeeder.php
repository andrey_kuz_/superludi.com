<?php

use Illuminate\Database\Seeder;
use App\PaymentStatus;

class PaymentStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $each = [
            PaymentStatus::PENDING     => ['name' => 'pending', 'title' => 'В обработке'],
            PaymentStatus::SUCCESS     => ['name' => 'success', 'title' => 'Оплачено'],
            PaymentStatus::FAILED     => ['name' => 'failed', 'title' => 'Ошибка'],
            PaymentStatus::CANCELLED     => ['name' => 'cancelled', 'title' => 'Отменен']
        ];
        foreach ($each as $id => $item) {

            $status = PaymentStatus::find($id);

            if (!$status) {
                $status = new PaymentStatus();
                $status->id = $id;
                $status->name = $item['name'];
                $status->{'title:ru'} = $item['title'];
            }
            else {
                $status->name = $item['name'];
                $status->{'title:ru'} = $item['title'];
            }

            $status->save();
        }

    }
}
