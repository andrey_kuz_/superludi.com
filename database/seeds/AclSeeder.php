<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class AclSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()['cache']->forget('spatie.permission.cache');

        Role::firstOrCreate(['name' => 'superadmin']);

        Role::firstOrCreate(['name' => 'user']);

        Role::firstOrCreate(['name' => 'customer']);

        Role::firstOrCreate(['name' => 'presale']);

        $userAdmin = User::where('email', '=', 'admin@superludi.com')->first();

        if (!$userAdmin) {

            $userAdmin = new User;

            $userAdmin->email = 'admin@superludi.com';
            $userAdmin->password = 'admin';

            $userAdmin->save();
        }

        if ($userAdmin && !$userAdmin->hasRole('superadmin')) {

            $userAdmin->assignRole('superadmin');
        }
    }
}
