<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\PaymentStatus
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $translations_array
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\PaymentStatusTranslation[] $translations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentStatus filtered($filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentStatus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel orWhereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel orWhereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel orderByTranslation($key, $sortmethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentStatus ordered($sortOrder)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentStatus query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentStatus whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentStatus whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentStatus whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel withTranslation()
 */
	class PaymentStatus extends \Eloquent {}
}

namespace App{
/**
 * App\CourseLesson
 *
 * @property int $id
 * @property int $course_id
 * @property string $video_code
 * @property string|null $preview_image_url
 * @property int $ord
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Comment[] $comments
 * @property-read \App\Course $course
 * @property mixed $preview_image
 * @property-read mixed $translations_array
 * @property-read mixed $video_link
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CourseLessonMaterial[] $materials
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CourseLessonTranslation[] $translations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLesson filtered($filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLesson filteredTrait($filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLesson newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLesson newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel orWhereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel orWhereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel orderByTranslation($key, $sortmethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLesson ordered($sortOrder)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLesson query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLesson whereCourseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLesson whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLesson whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLesson whereOrd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLesson wherePreviewImageUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLesson whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLesson whereVideoCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel withTranslation()
 */
	class CourseLesson extends \Eloquent {}
}

namespace App{
/**
 * App\UserInfo
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $full_name
 * @property string|null $city
 * @property string|null $phone
 * @property string|null $avatar_url
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property mixed $avatar
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserInfo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserInfo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserInfo query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserInfo whereAvatarUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserInfo whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserInfo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserInfo whereFullName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserInfo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserInfo wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserInfo whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserInfo whereUserId($value)
 */
	class UserInfo extends \Eloquent {}
}

namespace App{
/**
 * App\UserLessonState
 *
 * @property int $id
 * @property int $user_id
 * @property int $course_lesson_id
 * @property int|null $time_start
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\CourseLesson $course_lesson
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserLessonState newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserLessonState newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserLessonState query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserLessonState whereCourseLessonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserLessonState whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserLessonState whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserLessonState whereTimeStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserLessonState whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserLessonState whereUserId($value)
 */
	class UserLessonState extends \Eloquent {}
}

namespace App{
/**
 * App\SubscriptionTypeTranslation
 *
 * @property int $id
 * @property int $subscription_type_id
 * @property string|null $title
 * @property string $locale
 * @property-read \App\SubscriptionType $subscription_type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubscriptionTypeTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubscriptionTypeTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubscriptionTypeTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubscriptionTypeTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubscriptionTypeTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubscriptionTypeTranslation whereSubscriptionTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubscriptionTypeTranslation whereTitle($value)
 */
	class SubscriptionTypeTranslation extends \Eloquent {}
}

namespace App{
/**
 * App\BlogPost
 *
 * @property int $id
 * @property int $category_id
 * @property string $slug
 * @property string|null $thumbnail_image_url
 * @property string|null $hero_image_url
 * @property bool $is_published
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\BlogCategory $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Comment[] $comments
 * @property mixed $hero_image
 * @property mixed $tags_ids
 * @property mixed $thumbnail_image
 * @property-read mixed $translations_array
 * @property-read \App\SocialCount $social_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\BlogTag[] $tags
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\BlogPostTranslation[] $translations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPost filtered($filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPost newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPost newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel orWhereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel orWhereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel orderByTranslation($key, $sortmethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPost ordered($sortOrder)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPost published()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPost query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPost whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPost whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPost whereHeroImageUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPost whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPost whereIsPublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPost whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPost whereThumbnailImageUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPost whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel withTranslation()
 */
	class BlogPost extends \Eloquent {}
}

namespace App{
/**
 * App\SpeakerTranslation
 *
 * @property int $id
 * @property int $speaker_id
 * @property string|null $name
 * @property string|null $description
 * @property string|null $position
 * @property string $locale
 * @property-read \App\Speaker $speaker
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SpeakerTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SpeakerTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SpeakerTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SpeakerTranslation whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SpeakerTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SpeakerTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SpeakerTranslation whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SpeakerTranslation wherePosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SpeakerTranslation whereSpeakerId($value)
 */
	class SpeakerTranslation extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property int $id
 * @property string $email
 * @property string|null $email_verified_at
 * @property string|null $password
 * @property string|null $fb_id
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\AmoContact $amo_contact
 * @property-read \App\AmoLead $amo_lead
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Comment[] $comments
 * @property-read mixed $name
 * @property-read mixed $phone
 * @property-read \App\UserInfo $info
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Role[] $roles
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Subscription[] $subscriptions
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User filtered($filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User ordered($sortOrder)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User role($roles)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereFbId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

namespace App{
/**
 * App\InfoTranslation
 *
 * @property int $id
 * @property int $info_id
 * @property string|null $title
 * @property string|null $description
 * @property string|null $keywords
 * @property string|null $content
 * @property string $locale
 * @property-read \App\Info $post
 * @method static \Illuminate\Database\Eloquent\Builder|\App\InfoTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\InfoTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\InfoTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\InfoTranslation whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\InfoTranslation whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\InfoTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\InfoTranslation whereInfoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\InfoTranslation whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\InfoTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\InfoTranslation whereTitle($value)
 */
	class InfoTranslation extends \Eloquent {}
}

namespace App{
/**
 * App\SeasonTranslation
 *
 * @property int $id
 * @property int $season_id
 * @property string|null $title
 * @property string|null $title_short
 * @property string $locale
 * @property-read \App\Season $season
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SeasonTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SeasonTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SeasonTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SeasonTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SeasonTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SeasonTranslation whereSeasonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SeasonTranslation whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SeasonTranslation whereTitleShort($value)
 */
	class SeasonTranslation extends \Eloquent {}
}

namespace App{
/**
 * App\CourseLessonTranslation
 *
 * @property int $id
 * @property int $lesson_id
 * @property string|null $title
 * @property string|null $description
 * @property string $locale
 * @property-read \App\CourseLesson $lesson
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLessonTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLessonTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLessonTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLessonTranslation whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLessonTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLessonTranslation whereLessonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLessonTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLessonTranslation whereTitle($value)
 */
	class CourseLessonTranslation extends \Eloquent {}
}

namespace App{
/**
 * App\Comment
 *
 * @property int $id
 * @property int $parent_id
 * @property int|null $user_id
 * @property string $content
 * @property bool $is_published
 * @property int $commentable_id
 * @property string $commentable_type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Comment[] $childrens
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $commentable
 * @property-read \App\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment filtered($filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment filteredTrait($filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment ordered($sortOrder)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment published()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereCommentableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereCommentableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereIsPublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereUserId($value)
 */
	class Comment extends \Eloquent {}
}

namespace App{
/**
 * App\MailTemplateTranslation
 *
 * @property int $id
 * @property int $mail_id
 * @property string|null $subject
 * @property string|null $content
 * @property string $locale
 * @property-read \App\MailTemplate $mail
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MailTemplateTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MailTemplateTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MailTemplateTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MailTemplateTranslation whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MailTemplateTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MailTemplateTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MailTemplateTranslation whereMailId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MailTemplateTranslation whereSubject($value)
 */
	class MailTemplateTranslation extends \Eloquent {}
}

namespace App{
/**
 * App\Payment
 *
 * @property int $id
 * @property int $user_id
 * @property int|null $status_id
 * @property string|null $paid_at
 * @property string|null $comment
 * @property array|null $data
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment filtered($filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment filteredTrait($filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment ordered($sortOrder)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment wherePaidAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Payment whereUserId($value)
 */
	class Payment extends \Eloquent {}
}

namespace App{
/**
 * App\MailTemplate
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $translations_array
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\MailTemplateTranslation[] $translations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MailTemplate filtered($filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MailTemplate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MailTemplate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel orWhereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel orWhereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel orderByTranslation($key, $sortmethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MailTemplate ordered($sortOrder)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MailTemplate query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MailTemplate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MailTemplate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MailTemplate whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MailTemplate whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel withTranslation()
 */
	class MailTemplate extends \Eloquent {}
}

namespace App{
/**
 * App\Season
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Course[] $courses
 * @property-read mixed $translations_array
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\SubscriptionType[] $subscription_types
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\SeasonTranslation[] $translations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Season filtered($filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Season filteredTrait($filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Season newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Season newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel orWhereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel orWhereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel orderByTranslation($key, $sortmethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Season ordered($sortOrder)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Season query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Season whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Season whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Season whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel withTranslation()
 */
	class Season extends \Eloquent {}
}

namespace App{
/**
 * App\Subscription
 *
 * @property int $id
 * @property int $user_id
 * @property int $type_id
 * @property int $is_active
 * @property string|null $expiring_at
 * @property string|null $rectoken
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\SubscriptionType $subscription_type
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscription active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscription newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscription newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscription query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscription whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscription whereExpiringAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscription whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscription whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscription whereRectoken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscription whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscription whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Subscription whereUserId($value)
 */
	class Subscription extends \Eloquent {}
}

namespace App{
/**
 * App\SocialCount
 *
 * @property int $id
 * @property int $view_count
 * @property int $share_count
 * @property int $socialcountable_id
 * @property string $socialcountable_type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $socialcountable
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SocialCount filtered($filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SocialCount filteredTrait($filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SocialCount newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SocialCount newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SocialCount ordered($sortOrder)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SocialCount query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SocialCount whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SocialCount whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SocialCount whereShareCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SocialCount whereSocialcountableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SocialCount whereSocialcountableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SocialCount whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SocialCount whereViewCount($value)
 */
	class SocialCount extends \Eloquent {}
}

namespace App{
/**
 * App\Course
 *
 * @property int $id
 * @property float $price
 * @property string|null $banner_image_url
 * @property string|null $preview_image_url
 * @property string|null $banner_video_code
 * @property bool $is_published
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $slug
 * @property int|null $season_id
 * @property int|null $speaker_id
 * @property mixed $banner_image
 * @property-read mixed $lessons_materials
 * @property mixed $preview_image
 * @property-read mixed $translations_array
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CourseLesson[] $lessons
 * @property-read \App\Season|null $season
 * @property-read \App\Speaker|null $speaker
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CourseTranslation[] $translations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course filtered($filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course filteredTrait($filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel orWhereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel orWhereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel orderByTranslation($key, $sortmethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course ordered($sortOrder)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course published()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereBannerImageUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereBannerVideoCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereIsPublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course wherePreviewImageUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereSeasonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereSpeakerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Course whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel withTranslation()
 */
	class Course extends \Eloquent {}
}

namespace App{
/**
 * App\BlogPostTag
 *
 * @property int $id
 * @property int $post_id
 * @property int $tag_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPostTag newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPostTag newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPostTag query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPostTag whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPostTag wherePostId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPostTag whereTagId($value)
 */
	class BlogPostTag extends \Eloquent {}
}

namespace App{
/**
 * App\Info
 *
 * @property int $id
 * @property string $slug
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $translations_array
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\InfoTranslation[] $translations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Info filtered($filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Info newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Info newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel orWhereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel orWhereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel orderByTranslation($key, $sortmethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Info ordered($sortOrder)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Info query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Info whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Info whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Info whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Info whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel withTranslation()
 */
	class Info extends \Eloquent {}
}

namespace App{
/**
 * App\BlogTagTranslation
 *
 * @property int $id
 * @property int $tag_id
 * @property string|null $title
 * @property string $locale
 * @property string|null $description
 * @property string|null $keywords
 * @property-read \App\BlogTag $tag
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogTagTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogTagTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogTagTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogTagTranslation whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogTagTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogTagTranslation whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogTagTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogTagTranslation whereTagId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogTagTranslation whereTitle($value)
 */
	class BlogTagTranslation extends \Eloquent {}
}

namespace App{
/**
 * App\CourseLessonMaterial
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $file_url
 * @property int $course_lesson_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\CourseLesson $course_lesson
 * @property mixed $file
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLessonMaterial filtered($filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLessonMaterial filteredTrait($filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLessonMaterial newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLessonMaterial newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLessonMaterial ordered($sortOrder)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLessonMaterial query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLessonMaterial whereCourseLessonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLessonMaterial whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLessonMaterial whereFileUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLessonMaterial whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLessonMaterial whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseLessonMaterial whereUpdatedAt($value)
 */
	class CourseLessonMaterial extends \Eloquent {}
}

namespace App{
/**
 * App\BlogCategoryTranslation
 *
 * @property int $id
 * @property int $category_id
 * @property string|null $title
 * @property string $locale
 * @property string|null $description
 * @property string|null $keywords
 * @property-read \App\BlogCategory $category
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogCategoryTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogCategoryTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogCategoryTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogCategoryTranslation whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogCategoryTranslation whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogCategoryTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogCategoryTranslation whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogCategoryTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogCategoryTranslation whereTitle($value)
 */
	class BlogCategoryTranslation extends \Eloquent {}
}

namespace App{
/**
 * App\SubscriptionTypeSeason
 *
 * @property int $id
 * @property int $subscription_type_id
 * @property int $season_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubscriptionTypeSeason newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubscriptionTypeSeason newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubscriptionTypeSeason query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubscriptionTypeSeason whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubscriptionTypeSeason whereSeasonId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubscriptionTypeSeason whereSubscriptionTypeId($value)
 */
	class SubscriptionTypeSeason extends \Eloquent {}
}

namespace App{
/**
 * App\PaymentStatusTranslation
 *
 * @property int $id
 * @property int $payment_status_id
 * @property string|null $title
 * @property string $locale
 * @property-read \App\PaymentStatus $payment_status
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentStatusTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentStatusTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentStatusTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentStatusTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentStatusTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentStatusTranslation wherePaymentStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\PaymentStatusTranslation whereTitle($value)
 */
	class PaymentStatusTranslation extends \Eloquent {}
}

namespace App{
/**
 * App\SubscriptionType
 *
 * @property int $id
 * @property string $name
 * @property int $period
 * @property float $price
 * @property string|null $start_at
 * @property string|null $end_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property mixed $seasons_ids
 * @property-read mixed $translations_array
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Season[] $seasons
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\SubscriptionTypeTranslation[] $translations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubscriptionType active()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubscriptionType filtered($filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubscriptionType filteredTrait($filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubscriptionType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubscriptionType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel orWhereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel orWhereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel orderByTranslation($key, $sortmethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubscriptionType ordered($sortOrder)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubscriptionType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubscriptionType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubscriptionType whereEndAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubscriptionType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubscriptionType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubscriptionType wherePeriod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubscriptionType wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubscriptionType whereStartAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SubscriptionType whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel withTranslation()
 */
	class SubscriptionType extends \Eloquent {}
}

namespace App{
/**
 * App\BlogCategory
 *
 * @property int $id
 * @property string $slug
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $translations_array
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\BlogPost[] $posts
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\BlogCategoryTranslation[] $translations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogCategory filtered($filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel orWhereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel orWhereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel orderByTranslation($key, $sortmethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogCategory ordered($sortOrder)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogCategory whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogCategory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel withTranslation()
 */
	class BlogCategory extends \Eloquent {}
}

namespace App{
/**
 * App\AmoLead
 *
 * @property int $id
 * @property int|null $lead_id
 * @property int $is_synced
 * @property array|null $data
 * @property int $leadable_id
 * @property string $leadable_type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $leadable
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AmoLead newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AmoLead newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AmoLead query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AmoLead whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AmoLead whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AmoLead whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AmoLead whereIsSynced($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AmoLead whereLeadId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AmoLead whereLeadableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AmoLead whereLeadableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AmoLead whereUpdatedAt($value)
 */
	class AmoLead extends \Eloquent {}
}

namespace App{
/**
 * App\AmoContact
 *
 * @property int $id
 * @property int|null $contact_id
 * @property int $is_synced
 * @property array|null $data
 * @property int $contactable_id
 * @property string $contactable_type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $contactable
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AmoContact newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AmoContact newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AmoContact query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AmoContact whereContactId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AmoContact whereContactableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AmoContact whereContactableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AmoContact whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AmoContact whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AmoContact whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AmoContact whereIsSynced($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\AmoContact whereUpdatedAt($value)
 */
	class AmoContact extends \Eloquent {}
}

namespace App{
/**
 * App\BlogPostTranslation
 *
 * @property int $id
 * @property int $post_id
 * @property string|null $title
 * @property string|null $author_name
 * @property string|null $description
 * @property string|null $content
 * @property string $locale
 * @property string|null $keywords
 * @property-read \App\BlogPost $post
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPostTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPostTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPostTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPostTranslation whereAuthorName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPostTranslation whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPostTranslation whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPostTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPostTranslation whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPostTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPostTranslation wherePostId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPostTranslation whereTitle($value)
 */
	class BlogPostTranslation extends \Eloquent {}
}

namespace App{
/**
 * App\Speaker
 *
 * @property int $id
 * @property string|null $avatar_url
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Course[] $courses
 * @property mixed $avatar
 * @property-read mixed $translations_array
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\SpeakerTranslation[] $translations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Speaker filtered($filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Speaker newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Speaker newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel orWhereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel orWhereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel orderByTranslation($key, $sortmethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Speaker ordered($sortOrder)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Speaker query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Speaker whereAvatarUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Speaker whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Speaker whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Speaker whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel withTranslation()
 */
	class Speaker extends \Eloquent {}
}

namespace App{
/**
 * App\BlogTag
 *
 * @property int $id
 * @property string $slug
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $posts_ids
 * @property-read mixed $translations_array
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\BlogPost[] $posts
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\BlogTagTranslation[] $translations
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogTag filtered($filters)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel listsTranslations($translationField)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogTag newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogTag newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel notTranslatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel orWhereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel orWhereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel orderByTranslation($key, $sortmethod = 'asc')
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogTag ordered($sortOrder)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogTag query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel translated()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel translatedIn($locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogTag whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogTag whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogTag whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel whereTranslation($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel whereTranslationLike($key, $value, $locale = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogTag whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Abstracts\Models\TranslatableModel withTranslation()
 */
	class BlogTag extends \Eloquent {}
}

namespace App{
/**
 * App\CourseTranslation
 *
 * @property int $id
 * @property int $course_id
 * @property string|null $title
 * @property string|null $description
 * @property string $locale
 * @property string|null $keywords
 * @property-read \App\Course $course
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseTranslation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseTranslation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseTranslation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseTranslation whereCourseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseTranslation whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseTranslation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseTranslation whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseTranslation whereLocale($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CourseTranslation whereTitle($value)
 */
	class CourseTranslation extends \Eloquent {}
}

