let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.setPublicPath('public')
    .sass('resources/sass/admin.scss', 'css')
    .sass('resources/sass/front.scss', 'css')
    .sass('resources/sass/front-presale.scss', 'css')
    .react('resources/js/app-front.js', 'js')
    .react('resources/js/app-admin.js', 'js')
    .react('resources/js/app-front-presale.js', 'js')
    .copyDirectory('resources/img', 'public/images');
