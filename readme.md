# Сайт Superludi Portal

## Установка локального окружения

Для работы с проектом используется окружение `laravel-local` на базе docker.  
http://git.ukrosoft.com.ua/docker-environments/laravel-local

Для установки проекта на локальную машину нужно выполнить следующий набор команд

```
cd ~/Local
git clone http://git.ukrosoft.com.ua/docker-environments/laravel-local superludi-portal
cd superludi-portal
./tools/init superludi-portal
```

Далее выполняем команды

```
./tools/npm install
docker-compose up -d
./tools/composer install
./tools/artisan db:seed --class=AclSeeder
./tools/artisan db:seed --class=MailTemplateSeeder
./tools/artisan db:seed --class=PaymentStatusSeeder
./tools/artisan db:seed --class=PaymentTypeSeeder
```

После этого сайт будет доступен по адресу http://localhost:4200

## Консоль сборки Webpack

Чтобы в процессе работы с кодом видеть консоль сборки JS и отслеживать ошибки, запустите в консоли команду

```
docker-compose logs -f nodejs
```

## Вход в admin-панель

http://localhost:4200/admin 
Login: admin@superludi.com 
Password: admin 

