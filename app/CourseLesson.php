<?php

namespace App;

use App\Abstracts\Models\TranslatableModel;
use App\Traits\Models\Filterable;
use App\Traits\Models\Uploadable;
use Illuminate\Database\Eloquent\Builder;
use Carbon\Carbon;

class CourseLesson extends TranslatableModel
{
    const VISIBLE_COMMENTS = 5;

    use Uploadable;

    use Filterable {
        scopeFiltered as scopeFilteredTrait;
    }

    protected $table = 'course_lesson';

    protected $translationForeignKey = 'lesson_id';

    public $fillable = ['video_code', 'ord', 'course_id', 'preview_image'];

    protected $appends = ['preview_image', 'video_link'];

    public $translatedAttributes = ['title', 'description'];

    protected $filterable_by_kw = ['title'];

    public static function getTableName()
    {
        return with(new static)->getTable();
    }

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function materials()
    {
        return $this->hasMany(CourseLessonMaterial::class);
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function setPreviewImageAttribute($image)
    {
        $this->uploadedImageSet($image, 'preview_image');
    }

    public function getPreviewImageAttribute()
    {
        return $this->uploadedImageGet('preview_image');
    }


    public function scopeFiltered(Builder $query, $filters)
    {
        if (isset($filters['course_id'])){
            $query->where('course_id', $filters['course_id']);
        }

        $this->scopeFilteredTrait($query, $filters);
    }

    public function getVideoLinkAttribute()
    {
        $url = 'https://fast.wistia.net/embed/iframe/' . $this->video_code . '?videoFoam=true';
        return $url;
    }

    public function hasAccess($user)
    {
        if ($user) {
            foreach ($user->active_courses as $course) {
                if ($course->lessons->contains('id', $this->id)) {
                    return true;
                }
            }
        }
        return false;
    }
}
