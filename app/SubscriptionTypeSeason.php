<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionTypeSeason extends Model
{
    protected $table = 'subscription_type_season';

    protected $fillable = ['subscription_type_id', 'season_id'];

    public $timestamps = false;
}
