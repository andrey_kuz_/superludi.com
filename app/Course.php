<?php

namespace App;

use App\Abstracts\Models\TranslatableModel;
use App\Traits\Models\Filterable;
use App\Traits\Models\Uploadable;
use Illuminate\Database\Eloquent\Builder;

class Course extends TranslatableModel
{
    use Uploadable;
    use Filterable {
        scopeFiltered as scopeFilteredTrait;
    }

    protected $table = 'course';

    public $fillable = ['price', 'banner_image', 'preview_image', 'teaser_video_code', 'banner_video_code', 'is_published', 'slug', 'season_id', 'speaker_id'];

    protected $appends = ['banner_image', 'preview_image', 'materials_count', 'lessons_materials_count', 'promo_link'];

    public $translatedAttributes = ['title', 'description', 'keywords'];

    protected $filterable_by_kw = ['title'];

    protected $casts = [
        'is_published' => 'boolean',
    ];

    public function speaker()
    {
        return $this->belongsTo(Speaker::class);
    }

    public function season()
    {
        return $this->belongsTo(Season::class);
    }

    public function lessons()
    {
        return $this->hasMany(CourseLesson::class)->orderBy('ord', 'asc');
    }

    public function materials()
    {
        return $this->hasMany(CourseLessonMaterial::class);
    }

    public function scopePublished(Builder $query) {

        $query->where($this->table . '.is_published', '=', 1);
    }

    public function setBannerImageAttribute($image)
    {
        $this->uploadedImageSet($image, 'banner_image');
    }

    public function getBannerImageAttribute()
    {
        return $this->uploadedImageGet('banner_image');
    }

    public function setPreviewImageAttribute($image)
    {
        $this->uploadedImageSet($image, 'preview_image');
    }

    public function getPreviewImageAttribute()
    {
        return $this->uploadedImageGet('preview_image');
    }

    public function getMaterialsCountAttribute()
    {
        $count_materials = $this->materials->count();

        return $count_materials + $this->lessons_materials_count;
    }

    public function getLessonsMaterialsCountAttribute()
    {
        $count_materials = 0;

        foreach ($this->lessons as $lesson) {
            $count_materials += $lesson->materials->count();
        }

        return $count_materials;
    }

    public function getPromoLinkAttribute()
    {
        $url = 'https://fast.wistia.net/embed/iframe/' . $this->teaser_video_code . '?seo=false&videoFoam=true';
        return $url;
    }

    public function getBannerLinkAttribute()
    {
        $url = 'https://fast.wistia.net/embed/iframe/' . $this->banner_video_code . '?seo=false&videoFoam=true';
        return $url;
    }

    public function scopeFiltered(Builder $query, $filters)
    {
        if (isset($filters['season_id'])){
            $query->where('season_id', $filters['season_id']);
        }
        if (isset($filters['speaker_id'])){
            $query->where('speaker_id', $filters['speaker_id']);
        }

        $this->scopeFilteredTrait($query, $filters);
    }

    public function hasAccess($user)
    {
        if ($user) {
            if ($user->active_courses->contains('id', $this->id)) {
                return true;
            }
        }
        return false;
    }
}
