<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\MailTemplate;

class ForgotPassword extends Mailable
{
    use Queueable, SerializesModels;

    public $password;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($password)
    {
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $html = MailTemplate::where('name','user_password_reset')->firstOrFail();
        $content =  preg_replace("/{{.password}}/m", $this->password, $html->content);
        $content =  str_replace(';">', ';color: #fff;">', $content);

        $this->view('emails.password-recovery')
            ->with([
                'content' => $content,
            ]);

    }
}
