<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionTypeTranslation extends Model
{
    protected $table = 'subscription_type_translation';

    public $timestamps = false;

    protected $fillable = ['title', 'order_desc'];

    public function subscription_type() {

        return $this->belongsTo(SubscriptionType::class, 'subscription_type_id', 'id');
    }
}
