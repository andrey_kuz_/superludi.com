<?php

namespace App;
use App\Abstracts\Models\TranslatableModel;
use App\Traits\Models\Filterable;
use App\Traits\Models\Uploadable;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;



class BlogPost extends TranslatableModel
{
    const VISIBLE_COMMENTS = 5;

    use Filterable, Uploadable;

    protected $table = 'blog_post';

    public $fillable = ['category_id', 'slug', 'thumbnail_image', 'hero_image', 'is_published', 'created_at', 'updated_at', 'tags_ids'];

    protected $appends = ['thumbnail_image','hero_image', 'tags_ids'];
    protected $translationForeignKey = 'post_id';

    public $translatedAttributes = ['title', 'author_name', 'description', 'content',  'keywords'];

    protected $filterable_by_kw = ['id', 'slug'];

    protected $casts = [
        'is_published' => 'boolean',
    ];

    public static function getTableName()
    {
        return with(new static)->getTable();
    }

    public function category()
    {
        return $this->hasOne(BlogCategory::class,'id','category_id');
    }

    public function scopePublished($query)
    {
       return $query->where('is_published', 1);
    }

    public function setThumbnailImageAttribute($image)
    {
        $this->uploadedImageSet($image, 'thumbnail_image');
    }

    public function getThumbnailImageAttribute()
    {
        return $this->uploadedImageGet('thumbnail_image');
    }

    public function setHeroImageAttribute($image)
    {
        $this->uploadedImageSet($image, 'hero_image');
    }

    public function getHeroImageAttribute()
    {
        return $this->uploadedImageGet('hero_image');
    }

    public function tags()
    {
        return $this->belongsToMany(BlogTag::class, 'blog_post_tag','post_id', 'tag_id');
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function social_count()
    {
        return $this->morphOne(SocialCount::class, 'socialcountable')->withDefault();
    }

    public function getTagsIdsAttribute()
    {
        return $this->tags->pluck('id')->toArray();
    }

    public function setTagsIdsAttribute($tags_ids)
    {
        if ($this->exists) {
            $old_tags_ids = $this->getTagsIdsAttribute();
            $this->tags()->detach(array_diff($old_tags_ids, $tags_ids));
            $this->tags()->attach(array_diff($tags_ids, $old_tags_ids));
        }
    }

}
