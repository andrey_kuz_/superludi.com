<?php

namespace App;

use App\Traits\Models\Filterable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;
use Carbon\Carbon;

class User extends Authenticatable
{
    use Notifiable, HasRoles, Filterable;

    protected $table = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'fb_id', 'utm_tags'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $filterable_by_kw = ['email'];

    public function setPasswordAttribute($pass)
    {
        $this->attributes['password'] = Hash::make($pass);
    }

    protected $appends = ['name', 'phone', 'roles_ids'];

    /**
     * Get the info associated with user
     */
    public function info()
    {
        return $this->hasOne(UserInfo::class)->withDefault();
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class);
    }

    public function lessons_states()
    {
        return $this->hasMany(UserLessonState::class);
    }

    public function amo_contact()
    {
        return $this->morphOne(AmoContact::class, 'contactable')->withDefault();
    }

    public function amo_lead()
    {
        return $this->morphOne(AmoLead::class, 'leadable')->withDefault();
    }

    public function getRolesIdsAttribute()
    {
        return $this->roles->pluck('id')->toArray();
    }

    public function getNameAttribute()
    {
        if ($this->info->full_name){
            $name = $this->info->full_name;
        } else {
            $name = $this->email;
        }
        return $name;
    }

    public function getPhoneAttribute()
    {
        return $this->info->phone;
    }

    public function getActiveSubscriptionsAttribute()
    {
        return $this->subscriptions()->active()->with('subscription_type')->get();
    }

    public function getActiveCoursesAttribute()
    {
        $courses = new Collection();

        foreach($this->active_subscriptions as $item) {
            $courses = $courses->merge($item->courses);
        }

        return $courses;
    }

    public function getViewedCoursesAttribute()
    {
        $courses = [];
        foreach ($this->lessons_states as $state) {
            $courses[] = $state->course_lesson->course;
            $courses = collect($courses)->unique('id')->values()->all();
        }
        return collect($courses);
    }

    public function getRecommendedCoursesAttribute()
    {
        $active_courses = $this->active_courses->pluck('id')->toArray();
        $viewed_courses = $this->viewed_courses->pluck('id')->toArray();
        $recommended_courses = array_values(array_diff($active_courses, $viewed_courses));

        return $recommended_courses;
    }

}
