<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Models\Filterable;
use Illuminate\Database\Eloquent\Builder;

class Payment extends Model
{
    use Filterable {
        scopeFiltered as scopeFilteredTrait;
    }

    protected $table = 'payment';

    public $fillable = ['user_id', 'status_id', 'comment', 'data'];

    protected $casts = ['data' => 'array'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function status()
    {
        return $this-> belongsTo(PaymentStatus::class);
    }

    public function paymentable()
    {
        return $this->morphTo();
    }

    public function scopeFiltered(Builder $query, $filters)
    {
        if (isset($filters['type_id'])){
            $query->where('type_id', $filters['type_id']);
        }
        if (isset($filters['status_id'])){
            $query->where('status_id', $filters['status_id']);
        }

        $this->scopeFilteredTrait($query, $filters);
    }

}
