<?php

namespace App\Traits\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Storage;

trait Uploadable {

    static $images_type_to_extension = [
        IMAGETYPE_GIF => 'gif',
        IMAGETYPE_JPEG => 'jpg',
        IMAGETYPE_PNG => 'png'
    ];

    static $mimes_type_to_extension = [
        'application/pdf' => 'pdf',
        'application/octet-stream' => 'xls',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => 'xlsx'
    ];

    protected function uploadedImageSet($image, $field) {

        if ($image) {

            if (isset($image['data'])) {

                if (preg_match('/^data:image\/\w+;base64,(.*)$/', $image['data'], $matches) && count($matches) == 2) {

                    $file_data = str_replace(' ', '+', $matches[1]);

                    $img_data = base64_decode($file_data);

                    list( , , $type) = getimagesizefromstring($img_data);

                    if (isset(self::$images_type_to_extension[$type])) {
                        $file_extension = self::$images_type_to_extension[$type];
                    } else {
                        throw new \Exception('Invalid image type');
                    }

                    $dir_path = $this->table . '/' . $field . '/' . date('Y') . '/' . date('m') ;

                    if (!empty($image['title'])) {
                        $file_name = preg_replace('/\.[^\.]+$/', '', $image['title']);
                    } else {
                        $file_name = uniqid();
                    }

                    $file_path = $dir_path . '/' . $file_name . '.' . $file_extension;
                    $i = 1;

                    while (Storage::disk('public')->exists($file_path)) {

                        $file_path = $dir_path . '/' . $file_name . " ({$i})" . '.' . $file_extension;
                        $i++;
                    }

                    Storage::disk('public')->put($file_path, $img_data);

                    $this->{$field . '_url'} = $file_path;
                }
            }

        } else {

            $this->{$field . '_url'} = '';
        }
    }

    protected function uploadedImageGet($field) {

        if ($this->{$field . '_url'}) {

            return [
                'src' => Storage::disk('public')->url($this->{$field . '_url'}),
                'path' => Storage::disk('public')->path($this->{$field . '_url'}),
                'title' => basename($this->{$field . '_url'})
            ];

        } else {

            return null;
        }
    }


    protected function uploadedFileSet($file, $field) {

        if ($file) {

            if (isset($file['data'])) {
                if ((preg_match('/^data:application\/\w+;base64,(.*)$/', $file['data'], $matches) && count($matches) == 2) || (preg_match('/^data:application\/vnd\.ms-excel;base64,(.*)$/', $file['data'], $matches) && count($matches) == 2)) {
                    $file_data = str_replace(' ', '+', $matches[1]);
                    $decode_file_data = base64_decode($file_data);

                    $f = finfo_open();
                    $mime_type = finfo_buffer($f, $decode_file_data, FILEINFO_MIME_TYPE);
                    if (isset(self::$mimes_type_to_extension[$mime_type])) {
                        $file_extension = self::$mimes_type_to_extension[$mime_type];
                    } else {
                        throw new \Exception('Invalid mime type');
                    }

                    $dir_path = $this->table . '/' . $field . '/' . date('Y') . '/' . date('m') ;

                    if (!empty($file['title'])) {
                        $file_name = preg_replace('/\.[^\.]+$/', '', $file['title']);
                    } else {
                        $file_name = uniqid();
                    }

                    $file_path = $dir_path . '/' . $file_name . '.' . $file_extension;
                    $i = 1;

                    while (Storage::disk('public')->exists($file_path)) {

                        $file_path = $dir_path . '/' . $file_name . " ({$i})" . '.' . $file_extension;
                        $i++;
                    }

                    Storage::disk('public')->put($file_path, $decode_file_data);

                    $this->{$field . '_url'} = $file_path;
                }
            }

        } else {

            $this->{$field . '_url'} = '';
        }
    }

    protected function uploadedFileGet($field) {

        if ($this->{$field . '_url'}) {

            return [
                'src' => Storage::disk('public')->url($this->{$field . '_url'}),
                'path' => Storage::disk('public')->path($this->{$field . '_url'}),
                'title' => basename($this->{$field . '_url'})
            ];

        } else {
            return null;
        }
    }

}
