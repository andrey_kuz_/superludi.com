<?php

namespace App\Traits\Models;

use Illuminate\Database\Eloquent\Builder;

trait Filterable {

    function scopeFiltered(Builder $query, $filters) {

        $joinTranslations = false;

        if (isset($filters['id'])) {

            $query->whereIn($this->table . '.id', $filters['id']);
        }

        if (isset($filters['q'])) {

            $query->where(function ($query) use ($filters, &$joinTranslations) {

                foreach ($this->filterable_by_kw as $field) {

                    if (is_array($field)) {
                        $field = join('.', $field);
                    }

                    if (isset($this->translatedAttributes) && in_array($field, $this->translatedAttributes)) {
                        $field = $this->table . '_translation.' . $field;
                        $joinTranslations = true;
                    }

                    $query->orWhere($field, 'like', '%' . $filters['q'] . '%');
                }
            });
        }

        if ($joinTranslations) {

            $query->select($this->table . '.*');

            $query->join($this->table . '_translation', $this->getRelationKey(), '=', $this->table . '.id');
        }
    }

    function scopeOrdered(Builder $query, $sortOrder) {

        $query->orderBy($this->table . '.' . $sortOrder[0], $sortOrder[1]);
    }
}
