<?php

namespace App\Services;

use App\AmoContact;
use App\AmoLead;
use App\User;
use Dotzero\LaravelAmoCrm\Facades\AmoCrm;
use Illuminate\Support\Facades\Log;


class AmoCrmService
{
    private $api = null;
    private $fields = [];
    private $statuses = [];
    private $analytics = [];

    public function __construct()
    {
        $this->api = AmoCrm::getClient();
        $this->fields = config('amocrm.fields');
        $this->statuses = config('amocrm.statuses');
        $this->analytics = config('amocrm.analytics');
    }


    public function syncContacts()
    {
        if ($this->api) {

            $contacts_add = [];
            $items_update_ids = [];

            $amo_contacts = AmoContact::where('is_synced', 0)->get();

            foreach ($amo_contacts as $item) {

                // TODO: role should be check in other place

                if ($item->contactable && $item->contactable->hasRole('customer')) {

                    $currentUser = $this->getUser($item->contactable->email);

                    $contact = clone $this->api->contact;

                    $contact['request_id'] = $item->id;

                    if ($item->contactable->name) {
                        $contact['name'] = $item->contactable->name;
                    }
                    if ($item->contactable->phone) {
                        $contact->addCustomField($this->fields['phone'],  [[$item->contactable->phone, 'WORK']]);
                    }
                    if ($item->contactable->email) {
                        $contact->addCustomField($this->fields['email'], [[$item->contactable->email, 'WORK']]);
                    }

                    if (empty($currentUser)) {
                        $item->contact_id = $contact->apiAdd();
                        Log::channel('amocrm')->info('add-contacts: '.print_r($contacts_add, true));
                    } else {
                        if (!$item->contact_id) {
                            $item->contact_id = $currentUser['id'];
                        }
                        $contact->apiUpdate((int)$currentUser['id'], 'now');
                        Log::channel('amocrm')->info('update-contacts: '.print_r($items_update_ids, true));
                    }

                    $item->is_synced = 1;
                    $item->save();

                }
            }
        }
    }

    public function syncLeads()
    {
        if ($this->api) {

            $amo_leads = AmoLead::where('is_synced', 0)->get();

            foreach ($amo_leads as $item) {

                $currentUser = $this->getUser($item->leadable->email);

                // TODO: role should be check in other place

                if ( !empty($currentUser) && $item->leadable && $item->leadable->hasRole('customer') ) {

                    $lead = clone $this->api->lead;

                    $lead['request_id'] = $item->id;

                    if (!$item->lead_id) {
                        $lead['status_id'] = $this->statuses['primary'];
                        $lead['name'] = 'Суперлюди регистрация';

                        if ($item->leadable->utm_tags) {
                            $utm_tags = json_decode($item->leadable->utm_tags, true);

                            if (array_key_exists('utm_source', $utm_tags)) {
                                $lead->addCustomField($this->analytics['utm_source'], $utm_tags['utm_source']);
                            }
                            if (array_key_exists('utm_medium', $utm_tags)) {
                                $lead->addCustomField($this->analytics['utm_medium'], $utm_tags['utm_medium']);
                            }
                            if (array_key_exists('utm_campaign', $utm_tags)) {
                                $lead->addCustomField($this->analytics['utm_campaign'], $utm_tags['utm_campaign']);
                            }
                            if (array_key_exists('utm_term', $utm_tags)) {
                                $lead->addCustomField($this->analytics['utm_term'], $utm_tags['utm_term']);
                            }
                            if (array_key_exists('utm_content', $utm_tags)) {
                                $lead->addCustomField($this->analytics['utm_content'], $utm_tags['utm_content']);
                            }
                        }
                    } else {
                        $lead['status_id'] = $this->statuses['success'];

                        //TODO: orderBy created_at
                        $user_subscription = $item->leadable->subscriptions()->where('is_active', 1)->orderBy('created_at', 'desc')->first();
                        $subscription_type_name = $user_subscription->subscription_type->name;
                        if ($subscription_type_name == 'presale') {
                            $lead['name'] = 'офер митап';
                            $lead->addCustomField($this->fields['advertising_type'], 'Meetup 27.02');
                        } else {
                            $lead['name'] = 'Суперлюди оплата';
                            $lead->addCustomField($this->fields['advertising_type'], '');
                        }
                        if ($user_subscription->custom_data) {
                            $data = json_decode($user_subscription->custom_data, true);
                            if ($data['promo']) {
                                $lead->addCustomField($this->fields['question'], $data['promo']);
                            }
                        }
                    }

                    $lead->addCustomField($this->fields['email'], [[$item->leadable->email, 'WORK']]);
                    $lead->addCustomField($this->fields['source_transaction'], 'Заказ');
                    $lead->addCustomField($this->fields['type_transaction'], 'Входящий');
                    $lead->addCustomField($this->fields['product'], 'Подписка');
                    $lead->addCustomField($this->fields['stream'], 1);


                    if ($item->lead_id) {
                        $lead->apiUpdate((int)$item->lead_id, 'now');
                    } else {
                        $item->lead_id = $lead->apiAdd();
                    }

                    $linked_ids = $currentUser['linked_leads_id'];
                    if (!$linked_ids) $linked_ids = [];
                    $linked_ids[] = $item->lead_id;
                    $contact = clone $this->api->contact;
                    $contact->setLinkedLeadsId($linked_ids);
                    $contact->apiUpdate((int)$currentUser['id'], 'now');

                    $item->is_synced = 1;
                    $item->save();

                }
            }
        }
    }

    public function existUser($email, $currentUser)
    {
        foreach ($currentUser['custom_fields'] as $field) {
            if (isset($field['code']) && strtolower($field['code']) === 'email' && count($field['values'])) {
                foreach ($field['values'] as $item) {
                    if ($item['value'] === $email) return true;
                }
            }
        }

        return false;
    }

    public function getUser($email)
    {
        $currentUser =$this->api->contact->apiList([
            'query' => $email,
            'limit_rows' => 1
        ]);
        if (count($currentUser)) {
            $currentUser=$currentUser[0];
            $exist = $this->existUser($email, $currentUser);
            if ($exist) {
                return $currentUser;
            }
        }
    }
}
