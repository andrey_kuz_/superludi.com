<?php
/**
 * Created by PhpStorm.
 * User: aku
 * Date: 14.07.18
 * Time: 11:35
 */

namespace App\Services;

use omgdef\unisender\UniSenderWrapper;
use Illuminate\Support\Facades\Log;

class UniSenderService
{
    private $api_key ;
    private $list_id;
    private $params = [];

    public function __construct()
    {
        $this->api_key = config('unisender.api_key');
        $this->list_id = config('unisender.list_id');
        $this->params = config('unisender.params');
    }

    public function subscribe($email) {

        $obj = new UniSenderWrapper();
        $obj->apiKey = $this->api_key;
        $list_id = $this->list_id;
        $fields['email'] = $email;

        $response = $obj->subscribe($list_id, $fields, $this->params);

        if ($response['result']) {
            Log::channel('unisender')->info('subscribe-success:'.print_r($response, true));
            return true;
        } else {
            Log::channel('unisender')->info('subscribe-error:'.print_r($response, true));
            return false;
        }
    }
}
