<?php
/**
 * Created by PhpStorm.
 * User: aku
 * Date: 14.07.18
 * Time: 11:35
 */

namespace App\Services;

use Illuminate\Support\Facades\Log;
use GetResponse;

class GetResponseService
{
    private $list_id;

    public function __construct()
    {
        $this->list_id = config('getresponse.lists.default.id');
    }

    public function subscribe($email) {

        $fields['email'] = $email;
        $fields['campaign']['campaignId'] = $this->list_id;
        $response = GetResponse::addContact($fields);

        if (isset($response->code)){
            Log::channel('getresponse')->info('subscribe-error:'.print_r($response, true));
            return $response;
        }
        Log::channel('getresponse')->info('subscribe-success:'.print_r($response, true));
        return true;
    }
}
