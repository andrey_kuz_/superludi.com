<?php

namespace App\Services;

use App\PaymentStatus;
use Cloudipsp\Configuration;
use Cloudipsp\Checkout;
use App\Subscription;
use App\SubscriptionType;
use App\Payment as UserPayment;
use Cloudipsp\Payment;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class SubscriptionService
{
    private $merchant_id ;
    private $secretKey;
    private $prefix;
    private $callbackUrl;
    private $responseUrl;
    private $presaleCallbackUrl;
    private $presaleResponseUrl;

    public function __construct()
    {
        $this->merchant_id = config('fondy.merchant_id');
        $this->secretKey = config('fondy.secret_key');
        $this->prefix = config('fondy.order_prefix');
        $this->callbackUrl = route('checkoutCallback');
        $this->responseUrl = route('checkoutResponse');

        $this->presaleCallbackUrl = route('presale.checkoutCallback');
        $this->presaleResponseUrl = route('presale.checkoutResponse');
    }

    public function subscribe(SubscriptionType $subscription_type, $user_id)
    {
        $this->setMerchantData();
        $data = $this->generateData($subscription_type->id, $user_id);
        $signature = $this->generateSignature($data);
        $data['signature'] = $signature;
        Log::channel('fondy')->info('subscribe: '.print_r($data, true));
        $this->checkout($data);
    }

    public function setMerchantData()
    {
        Configuration::setMerchantId($this->merchant_id);
        Configuration::setSecretKey($this->secretKey);
    }

    public function generateSignature(Array $params){
        $params['merchant_id'] = $this->merchant_id;
        $params = array_filter($params,'strlen');
        ksort($params);
        $params = array_values($params);
        array_unshift( $params , $this->secretKey);
        $params = join('|',$params);
        return(sha1($params));
    }

    public function generateData($subscription_type_id, $user_id, $subscription = null)
    {
        if ($subscription) {
            $subscription_type = SubscriptionType::find($subscription->type_id);
            $price = $subscription_type->sale_price ?? $subscription_type->price;
        }
        if (!$subscription) {
            $subscription_type = SubscriptionType::find($subscription_type_id);
            $subscription = $this->createSubscription($subscription_type, $user_id);
            $price = $subscription_type->price;
        }

        $amount = $price * 100;
        $payment = $this->createPayment($subscription);
        $payment_id = $this->prefix . $payment->id;

        $data = [
            'order_id' => $payment_id,
            'order_desc' => $subscription_type->order_desc,
            'currency' => 'USD',
            'amount' => $amount,
            'default_payment_system' => 'card',
            'server_callback_url' => $this->callbackUrl,
            'response_url' => $this->responseUrl
        ];
        return $data;
    }

    public function checkout($data)
    {
        $checkout_url = Checkout::url($data);
        $checkout_data = $checkout_url->getData();
        if ($checkout_data['response_status'] === 'success') {
            Log::channel('fondy')->info('checkout: '.print_r($checkout_data, true));
            $checkout_url->toCheckout();
        }
    }

    public function createSubscription($subscription_type, $user_id, $custom_data=[])
    {
        $subscription = new Subscription();
        $subscription->user_id = $user_id;
        $subscription->type_id = $subscription_type->id;
        if (count($custom_data)) {
            $custom_data = json_encode($custom_data);
            $subscription->custom_data = $custom_data;
        }
        $subscription->save();

        return $subscription;
    }

    public function isSuccess($request)
    {
        $payment_id = $request['order_id'];
        $payment_id = str_replace($this->prefix, '', $payment_id);//test

        $payment = UserPayment::find($payment_id);
        $subscription = $payment->paymentable;

        $order_time = Carbon::parse($request['order_time']);
        $count_days = $subscription->subscription_type->period;
        if ($order_time->lte($subscription->subscription_type->active_at)) {
            $expired_date = Carbon::parse($subscription->subscription_type->active_at);
            $expired_date = $expired_date->addDays($count_days)->toDateTimeString();
        } else {
            $now = Carbon::now();
            if ($now->gte($subscription->expiring_at)){
                $expired_date = $order_time->addDays($count_days)->toDateTimeString();
            } else {
                $expired_date = Carbon::parse($subscription->expiring_at);
                $expired_date = $expired_date->addDays($count_days)->toDateTimeString();
            }
        }

        if ($request['order_status'] === 'approved' && $request['response_status'] === 'success') {

            $subscription->expiring_at = $expired_date;
            $subscription->is_active = 1;
            $subscription->save();

            $amo_lead = $subscription->user->amo_lead;
            $amo_lead->is_synced = 0;
            $amo_lead->save();

            $payment->status_id = PaymentStatus::SUCCESS;
            $payment->paid_at = $order_time->toDateTimeString();
            $payment->save();

            Log::channel('fondy')->info('isSuccess success: ' . print_r($subscription, true));

            return true;

        } else {
            $subscription->is_active = 0;
            $subscription->save();

            $payment->status_id = PaymentStatus::FAILED;
            $payment->paid_at = $order_time->toDateTimeString();
            $payment->save();

            Log::channel('fondy')->info('isSuccess error: ' . print_r($subscription, true));

            return false;
        }
    }

    public function recurring(Subscription $subscription)
    {
        $this->setMerchantData();
        $data = $this->generateData($subscription->type_id, $subscription->user_id, $subscription);
        $signature = $this->generateSignature($data);
        $data['signature'] = $signature;
        Log::channel('fondy')->info('subscribe: '.print_r($data, true));
        $this->checkout($data);
    }

    public function createPayment($subscription)
    {
        $payment = $subscription->payment()->save(
            new UserPayment([
                'user_id' => $subscription->user_id,
                'status_id' => PaymentStatus::PENDING
            ]));

        return $payment;
    }

    public function presaleSubscribe(SubscriptionType $subscription_type, $user_id, $subscription, $custom_data)
    {
        $this->setMerchantData();
        $data = $this->createPresaleData($subscription_type->id, $user_id, $subscription, $custom_data);
        $signature = $this->generateSignature($data);
        $data['signature'] = $signature;
        Log::channel('fondy')->info('presale subscribe: '.print_r($data, true));
        $this->checkout($data);
    }

    public function createPresaleData($subscription_type_id, $user_id, $subscription, $custom_data)
    {
        $subscription_type = SubscriptionType::find($subscription_type_id);
        $amount = $subscription_type->price * 100;
        if (!$subscription) {
            $subscription = $this->createSubscription($subscription_type, $user_id, $custom_data);
        }
        $payment = $this->createPayment($subscription);
        $payment_id = $this->prefix . $payment->id;

        $data = [
            'order_id' => $payment_id,
            'order_desc' => $subscription_type->order_desc,
            'currency' => 'USD',
            'amount' => $amount,
            'default_payment_system' => 'card',
            'server_callback_url' => $this->presaleCallbackUrl,
            'response_url' => $this->presaleResponseUrl
        ];
        return $data;
    }


//    public function recurring(Subscription $subscription)
//    {
//        $payment = $this->createPayment($subscription);
//
//        $this->setMerchantData();
//        $data = $this->generateRecurringData($subscription);
//
//        $order = Payment::recurring($data);
//        $approved = $order->isApproved();
//
//        $recurring_order = $order->getData();
//        $order_time = Carbon::parse($recurring_order['order_time']);
//
//        $count_days = $subscription->subscription_type->period;
//        $now = Carbon::now();
//
//        if ($now->gte($subscription->expiring_at)){
//            $expired_date = $order_time->addDays($count_days)->toDateTimeString();
//        } else {
//            $expired_date = Carbon::parse($subscription->expiring_at);
//            $expired_date = $expired_date->addDays($count_days)->toDateTimeString();
//        }
//
//        if($approved) {
//
//            $subscription->expiring_at = $expired_date;
//            $subscription->is_active = 1;
//            $subscription->rectoken = $recurring_order['rectoken'];
//            $subscription->save();
//
//            $amo_lead = $subscription->user->amo_lead;
//            $amo_lead->is_synced = 0;
//            $amo_lead->save();
//
//            $payment->status_id = PaymentStatus::SUCCESS;
//            $payment->paid_at = $order_time->toDateTimeString();
//            $payment->save();
//
//            Log::channel('fondy')->info('recurring(data): '.print_r($data, true));
//            Log::channel('fondy')->info('recurring(recurring_order): '.print_r($recurring_order, true));
//
//            return true;
//
//        } else {
//            $subscription->is_active = 0;
//            $subscription->save();
//
//            $payment->status_id = PaymentStatus::FAILED;
//            $payment->paid_at = $order_time->toDateTimeString();
//            $payment->save();
//
//            return false;
//        }
//    }
//
//    public function generateRecurringData($subscription)
//    {
//        $amount  = $subscription->subscription_type->price * 100;
//        $recurringData = [
//            'currency' => 'USD',
//            'amount' => $amount,
//            'rectoken' => $subscription->rectoken
//        ];
//        return $recurringData;
//    }



}
