<?php
namespace App;
use App\Abstracts\Models\TranslatableModel;
use App\Traits\Models\Filterable;


class BlogCategory extends TranslatableModel
{
    use Filterable;

    protected $table = 'blog_category';

    protected $translationForeignKey = 'category_id';

    public $fillable = ['slug'];

    public $translatedAttributes = ['title', 'description', 'keywords'];

    protected $filterable_by_kw = ['slug', 'title'];

    public function posts()
    {
        return $this->hasMany(BlogPost::class, 'category_id', 'id');
    }
}

