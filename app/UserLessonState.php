<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLessonState extends Model
{
    protected $table = 'user_lesson_state';
    protected $fillable = ['user_id', 'course_lesson_id', 'time_start'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function course_lesson()
    {
        return $this->belongsTo(CourseLesson::class);
    }

}
