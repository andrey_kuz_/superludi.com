<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Traits\Models\Filterable;

class Subscription extends Model
{
    protected $table = 'subscription';

    use Filterable {
        scopeFiltered as scopeFilteredTrait;
    }
    public $fillable = ['user_id', 'type_id', 'is_active', 'expiring_at', 'rectoken', 'created_at', 'updated_at', 'custom_data'];

    protected $filterable_by_kw = ['id'];

    protected $translationForeignKey = 'user_id';

    protected $casts = [
        'is_active' => 'boolean',
        'custom_data' => 'array'
    ];

    public function subscription_type()
    {
        return $this->belongsTo(SubscriptionType::class, 'type_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function payment()
    {
        return $this->morphMany(Payment::class, 'paymentable');
    }

    public function scopeActive(Builder $query)
    {
        $query->where('is_active', 1);
    }

    public function getCoursesAttribute()
    {
        return $this->subscription_type->courses;
    }

    public function scopeFiltered(Builder $query, $filters)
    {
        if(isset($filters['user_id'])) {
            $searchid = $filters['user_id'];
            $query->orWhereHas('user', function($query) use ($searchid)
            {
                $query->where('id', $searchid);
            });
        }
        if(isset($filters['expiring_at-start'])){
            $query->where('expiring_at','>', $filters['expiring_at-start']);
        }
        if(isset($filters['expiring_at-end'])){
            $query->where('expiring_at','<', $filters['expiring_at-end']);
        }

        $this->scopeFilteredTrait($query, $filters);
    }


}
