<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AmoLead extends Model
{
    protected $table = 'amo_lead';

    protected $casts = [
        'data' => 'json',
    ];

    public function leadable()
    {
        return $this->morphTo();
    }
}
