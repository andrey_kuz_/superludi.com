<?php

namespace App\Abstracts\Models;

use Illuminate\Database\Eloquent\Model;
use Dimsav\Translatable\Translatable;

abstract class TranslatableModel extends Model
{
    use Translatable;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->append('translations_array');
    }

    public function getTranslationsArrayAttribute() {

        return $this->getTranslationsArray();
    }
}
