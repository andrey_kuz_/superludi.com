<?php

namespace App\Console\Commands;

use App\Services\SubscriptionService;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class SubscriptionRecurring extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscription:recurring';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Subscription using card token';

    protected $subscriptionService = null;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::channel('commands')->info('RUN ' . $this->signature . ' at ' . Carbon::now()->format('d.m.Y H:i:s'));

        $this->subscriptionService = new SubscriptionService();

        $this->subscriptionService->recurring();
    }
}
