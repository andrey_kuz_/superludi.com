<?php

namespace App\Console\Commands;

use App\Services\AmoCrmService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class AmocrmSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'amocrm:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Syncing records with AmoCrm';

    protected $amocrmService = null;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::channel('commands')->info('RUN ' . $this->signature . ' at ' . Carbon::now()->format('d.m.Y H:i:s'));

        $this->amocrmService = new AmoCrmService();

        $this->amocrmService->syncContacts();
        $this->amocrmService->syncLeads();
    }
}
