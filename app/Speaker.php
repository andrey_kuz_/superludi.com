<?php

namespace App;

use App\Abstracts\Models\TranslatableModel;
use App\Traits\Models\Filterable;
use App\Traits\Models\Uploadable;

class Speaker extends TranslatableModel
{
    use Uploadable, Filterable;

    protected $table = 'speaker';

    public $fillable = ['avatar'];

    protected $appends = ['avatar'];

    public $translatedAttributes = ['name', 'description', 'position'];

    protected $filterable_by_kw = ['name'];

    public function courses()
    {
        return $this->hasMany(Course::class);
    }

    public function setAvatarAttribute($image)
    {
        $this->uploadedImageSet($image, 'avatar');
    }

    public function getAvatarAttribute()
    {
        return $this->uploadedImageGet('avatar');
    }

}
