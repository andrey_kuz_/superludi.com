<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailTemplateTranslation extends Model
{
    protected $table = 'mail_template_translation';

    public $timestamps = false;

    protected $fillable = ['subject', 'content'];

    public function mail() {

        return $this->belongsTo(MailTemplate::class, 'mail_id', 'id');
    }
}
