<?php

namespace App;

use App\Abstracts\Models\TranslatableModel;
use App\Traits\Models\Filterable;

class Info extends TranslatableModel
{
    use Filterable;

    protected $table = 'info';

    public $fillable = ['slug', 'created_at', 'updated_at'];

    protected $translationForeignKey = 'info_id';

    public $translatedAttributes = ['title', 'description',  'keywords', 'content'];

    protected $filterable_by_kw = ['id', 'slug'];


}
