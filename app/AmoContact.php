<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AmoContact extends Model
{
    protected $table = 'amo_contact';

    protected $casts = [
        'data' => 'json',
    ];

    public function contactable()
    {
        return $this->morphTo();
    }
}
