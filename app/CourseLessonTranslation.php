<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseLessonTranslation extends Model
{
    protected $table = 'course_lesson_translation';

    public $timestamps = false;

    protected $fillable = ['title', 'description'];

    public function lesson() {

        return $this->belongsTo(CourseLesson::class, 'lesson_id', 'id');
    }
}
