<?php

namespace App;
use App\Abstracts\Models\TranslatableModel;
use Illuminate\Database\Query\Builder;
use App\Traits\Models\Filterable;

class BlogTag extends TranslatableModel
{
    use Filterable;

    protected $table = 'blog_tag';

    public $fillable = ['slug', 'title', 'created_at', 'updated_at'];

    protected $translationForeignKey = 'tag_id';

    public $translatedAttributes = ['title', 'description', 'keywords'];

    protected $filterable_by_kw = ['id', 'slug'];

    public function posts()
    {
        return $this->belongsToMany(BlogPost::class, 'blog_post_tag','tag_id', 'post_id');
    }

    public function getPostsIdsAttribute()
    {
        return $this->posts->pluck('id');
    }
}
