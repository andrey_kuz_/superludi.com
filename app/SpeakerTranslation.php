<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpeakerTranslation extends Model
{
    protected $table = 'speaker_translation';

    public $timestamps = false;

    protected $fillable = ['name', 'description', 'position'];

    public function speaker()
    {
        return $this->belongsTo(Speaker::class, 'speaker_id', 'id');
    }
}
