<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Models\Filterable;
use App\Traits\Models\Uploadable;
use Illuminate\Database\Eloquent\Builder;
use SplFileInfo;


class CourseLessonMaterial extends Model
{
    use Uploadable;
    use Filterable {
        scopeFiltered as scopeFilteredTrait;
    }

    protected $table = 'course_lesson_material';

    public $fillable = ['file', 'course_id', 'course_lesson_id', 'title'];

    protected $appends = ['file', 'size', 'extension'];

    protected $filterable_by_kw = ['title'];

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function course_lesson()
    {
        return $this->belongsTo(CourseLesson::class);
    }

    public function setFileAttribute($file)
    {
        $this->uploadedFileSet($file, 'file');
    }

    public function getFileAttribute()
    {
        return $this->uploadedFileGet('file');
    }

    public function getSizeAttribute()
    {
        $file_path = $this->file['path'];
        try{
            $file_size = filesize($file_path);
            $size = number_format($file_size / 1048576, 2);
            return $size;
        } catch (\Exception $e) {
           return null;
        }
    }
    
    public function getExtensionAttribute()
    {
        $file_info = new SplFileInfo($this->file['src']);
        return $file_info->getExtension();
    }

    public function scopeFiltered(Builder $query, $filters)
    {
        if (isset($filters['course_id'])){
            $query->where('course_id', $filters['course_id']);
        }

        if (isset($filters['course_lesson_id'])){
            $query->where('course_lesson_id', $filters['course_lesson_id']);
        }

        $this->scopeFilteredTrait($query, $filters);
    }
}
