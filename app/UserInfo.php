<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Models\Uploadable;

class UserInfo extends Model
{
    use Uploadable;

    protected $table = 'user_info';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['full_name', 'city', 'avatar', 'phone'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['security_answer'];

    protected $appends = ['avatar'];

    public function user() {

        return $this->belongsTo(User::class);
    }

    public function setAvatarAttribute($image)
    {
        $this->uploadedImageSet($image, 'avatar');
    }

    public function getAvatarAttribute()
    {
        return $this->uploadedImageGet('avatar');
    }
}
