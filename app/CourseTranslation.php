<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseTranslation extends Model
{
    protected $table = 'course_translation';

    public $timestamps = false;

    protected $fillable = ['title', 'description', 'keywords'];

    public function course() {

        return $this->belongsTo(Course::class, 'course_id', 'id');
    }
}
