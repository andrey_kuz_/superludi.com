<?php

namespace App\Providers;

use App\AmoContact;
use App\AmoLead;
use App\User;
use App\UserInfo;
use App\BlogPost;
use App\SocialCount;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        User::created(function($user) {

            $user->amo_contact()->save(new AmoContact());
            $user->amo_lead()->save(new AmoLead());
        });

        User::updated(function($user) {

            $user->amo_contact->is_synced = 0;
            $user->amo_contact->save();
        });

        UserInfo::updated(function($user_info) {

            $user_info->user->amo_contact->is_synced = 0;
            $user_info->user->amo_contact->save();
        });

        BlogPost::created(function ($post){
            $post->social_count()->save(new SocialCount());
        });
    }
}
