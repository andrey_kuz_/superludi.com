<?php

namespace App;


use App\Abstracts\Models\TranslatableModel;
use App\Traits\Models\Filterable;

class PaymentStatus extends TranslatableModel
{
    use Filterable;

    const PENDING = 1;
    const SUCCESS = 2;
    const FAILED = 3;
    const CANCELLED = 4;

    protected $table = 'payment_status';

    public $fillable = ['id'];

    protected $translationForeignKey = 'payment_status_id';

    public $translatedAttributes = ['title'];

    protected $filterable_by_kw = ['name'];
}
