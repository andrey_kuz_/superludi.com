<?php

namespace App\Http\Controllers\Callback;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\SubscriptionService;
use Illuminate\Support\Facades\Log;

class SubscriptionController extends Controller
{
    public function checkoutCallback(Request $request)
    {
        $subscribeService = new SubscriptionService();
        $subscribeService->isSuccess($request);
        Log::channel('fondy')->info('checkoutCallback: '.print_r($subscribeService, true));
    }

    public function checkoutResponse(Request $request)
    {
        $subscribeService = new SubscriptionService();
        $response = $subscribeService->isSuccess($request);

        if ($response) {
            Log::channel('fondy')->info('checkoutResponse(success): '.print_r($response, true));
            return redirect()->route('thank_payment');
        } else {
            Log::channel('fondy')->info('checkoutResponse(error): '.print_r($response, true));
            return redirect()->route('error_payment');
        }
    }
}
