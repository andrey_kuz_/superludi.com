<?php

namespace App\Http\Controllers\Web\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Web\WebController;
use Illuminate\Support\Facades\Session;
use Socialite;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Crypt;


class AuthController extends WebController
{

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        $fb_user = Socialite::driver($provider)->user();
        $user = User::where('fb_id', $fb_user->id)->first();

        if (!$fb_user->email && !$user) {
            Session::put('fb_user', $fb_user);
            return redirect()->route('enter_email');
        }

        $user_by_email = User::where('email', $fb_user->email)->whereNull('fb_id')->first();

        if ($user_by_email) {
            Session::put('fb_user', $fb_user);
            return redirect()->route('enter_password');
        }

        if (!$user) {
            $user = User::create([
                'email' => $fb_user->email,
                'fb_id' => $fb_user->id
            ]);
            $user->assignRole('customer');
            $user->info()->updateOrcreate(['full_name' => $fb_user->name]);

            $image = $this->saveFBAvatar($fb_user->avatar_original);
            $user->info->avatar = $image;
            $user->info->save();

            $user->save();
        }

        return $this->loginByFB($user);
    }

    public function loginByFB($user)
    {
        try {

            $token = $this->auth->fromUser($user);
            Cookie::queue('token', $token);

            return response()->redirectToRoute('home', ['refresh_token' => 1]);

        } catch (\Exception $e) {

            return response()->json($e->getMessage(), 500);
        }
    }

    private function saveFBAvatar($avatar)
    {
        // TODO: look for the laravel-style method

        $file = file_get_contents($avatar);
        $file_info = finfo_open();
        $mime_type = finfo_buffer($file_info, $file, FILEINFO_MIME_TYPE);
        $f = base64_encode($file);

        $image['data'] = 'data:' . $mime_type . ';base64,' . $f;
        $image['key'] = 'avatar';
        $image['multiple'] = false;

        return $image;
    }

}
