<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Web\WebController;

class AppController extends WebController
{
    public function index()
    {
        return view('admin.app');
    }
}
