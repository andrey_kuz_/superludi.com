<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Tymon\JWTAuth\JWTAuth;

class WebController extends Controller
{
    use SEOToolsTrait;

    function __construct(JWTAuth $auth)
    {
        parent::__construct($auth);

        $this->seo()->metatags()->setTitleDefault(trans('seo.general.title_append'));

        $this->seo()->setTitle(trans('seo.general.title'));
        $this->seo()->setDescription(trans('seo.general.description'));
        $this->seo()->metatags()->setKeywords(trans('seo.general.keywords'));

        $this->seo()->opengraph()->addImage(asset('images/og-banner.jpg'));
    }

    public function setSeoFields($title, $description, $keywords)
    {
        if ($title) {
            $this->seo()->setTitle($title);
        }

        if ($description) {
            $this->seo()->setDescription($description);
        }

        if ($keywords) {
            $this->seo()->metatags()->setKeywords($keywords);
        }
    }
}
