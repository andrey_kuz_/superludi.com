<?php

namespace App\Http\Controllers\Web\Front;

use App\Http\Controllers\Web\WebController;
use Illuminate\Http\Request;

use App\BlogPost;
use App\Season;
use App\SubscriptionType;
use App\Course;
use Illuminate\Support\Facades\Cookie;


class HomeController extends WebController
{

    public function index(Request $request)
    {
        $active_subscription = SubscriptionType::active()
            ->where('name', '<>', 'presale')
            ->minPrice()
            ->with('courses')
            ->first();

        $courses = Course::latest()->limit(6)->get()->reverse();
        $count_published_courses = Course::published()->count();
        $latest_season = Season::query()->latest()->with('courses')->first();
        $latest_season['courses'] = $latest_season->courses()->published()->get();
        $posts = BlogPost::with('social_count')->published()->get()->sortByDesc('social_count.view_count');

        $this->setSeoFields(
            trans('seo.home.title'),
            trans('seo.home.description'),
            trans('seo.home.keywords')
        );

        if ($request->has('buy_subscription')) {
            Cookie::queue('buy_subscription', $request->input('buy_subscription'));
        }

        try {
            $user = $this->auth->toUser();
        }
        catch (\Exception $e) {
            $user = null;
        }

        if ($user && $request->cookies->has('buy_subscription')) {

            return response()
                ->redirectToRoute('subscription.checkout', $request->cookies->get('buy_subscription'))
                ->withCookie(Cookie::forget('buy_subscription'));
        }

        return view('front.home',
            compact('courses',
                'latest_season',
                'posts',
                'active_subscription',
                'count_published_courses'
            )
        );
    }
}
