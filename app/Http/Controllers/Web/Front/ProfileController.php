<?php

namespace App\Http\Controllers\Web\Front;


use App\Http\Controllers\Web\WebController;
use App\Course;
use App\UserLessonState;
use Carbon\Carbon;


class ProfileController extends WebController
{
    public function index()
    {
        $user = $this->auth->toUser();
        $active_subscriptions = $user->active_subscriptions;
        $viewed_courses = $user->viewed_courses;
        $recommended_courses_ids = $user->recommended_courses;
        $recommended_courses = Course::find($recommended_courses_ids);

        foreach($active_subscriptions as $item) {
            $item->expiring_at = Carbon::parse($item->expiring_at);
        }

        $this->setSeoFields(trans('seo.profile.title'), trans('seo.profile.description'), trans('seo.profile.keywords'));

        return view('front.profile',
            compact('active_subscriptions',
                'viewed_courses',
                'recommended_courses')
        );
    }
}
