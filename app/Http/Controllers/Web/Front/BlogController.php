<?php

namespace App\Http\Controllers\Web\Front;

use App\Http\Controllers\Web\WebController;

use App\BlogCategory;
use App\BlogPost;
use App\BlogTag;
use App\Comment;
use App\SocialCount;
use App\UserInfo;
use Illuminate\Support\Facades\View;
use Tymon\JWTAuth\JWTAuth;

class BlogController extends WebController
{
    public function __construct(JWTAuth $auth)
    {
        $categories = BlogCategory::all();
        $tags = BlogTag::all();

        View::composer('front.*', function ($view) use (&$categories, &$tags) {

            $view->with(compact('categories', 'tags'));
        });

        parent::__construct($auth);
    }


    public function index()
    {
        $tag_count = config('blog.main_show_count_tag');
        $query_tag = BlogTag::query();
        $tags = $query_tag->inRandomOrder()->limit($tag_count)->get();
        $posts = BlogPost::published()->get();

        $this->setSeoFields(trans('seo.blog.title'), trans('seo.blog.description'), trans('seo.blog.keywords'));

        return view('front.blog', compact('tags', 'posts'));
    }

    public function category($category_slug)
    {
        $category_count = config('blog.show_count_category');
        $tag_count = config('blog.main_show_count_tag');

        $query = BlogCategory::query();

        $category = $query
            ->where('slug', $category_slug)
            ->firstOrFail();
        $posts = $category
            ->posts()
            ->published()
            ->paginate($category_count);
        $query_tag = BlogTag::query();

        $posts_count = $category
            ->posts()
            ->published()
            ->count();
        $posts_count = $posts_count - $category_count;
        $tags = $query_tag
            ->inRandomOrder()
            ->limit($tag_count)
            ->get();

        $this->setSeoFields($category->title, $category->description, $category->keywords);

        return view('front.blog-category')->with(['posts' => $posts, 'category' => $category, 'tags' => $tags, 'posts_count' => $posts_count]);
    }


    public function post($category_slug, $post_slug)
    {
        $query = BlogCategory::query();
        $category = $query->where('slug', $category_slug)->firstOrFail();
        $post = $category->posts()->where('slug', $post_slug)->firstOrFail();
        $all_posts = BlogPost::with('social_count')->published()->get()->sortByDesc('social_count.view_count');

        $post_view_count = BlogPost::where('slug', $post_slug)->first();
        if (!isset($_COOKIE['idpage' . $post_view_count->id])) {
            $post_view_count->social_count->view_count += 1;
            setcookie('idpage' . $post_view_count->id, $post_view_count->id, time() + 60 * 60 * 24 * 30);
            $post_view_count->social_count->save();
        }

        $comments = $post->comments()->where('parent_id', 0)->published()->with('childrens')->limit(BlogPost::VISIBLE_COMMENTS)->get();
        $commentable = $post;

        $count_all_comments = $post->comments()->published()->count();
        $count_visible_comments = $this->countComments($comments->count(), $comments);
        $count_available_comments = $count_all_comments - $count_visible_comments;

        $this->setSeoFields($post->title, $post->description, $post->keywords);

        return view('front.blog-post',compact('post', 'comments', 'all_posts', 'commentable', 'count_available_comments'));
    }

    public function tag($tag_slug)
    {
        $tag_count = config('blog.show_count_tag');

        $query = BlogTag::query();

        $tag_current = $query
            ->where('slug', $tag_slug)
            ->firstOrFail();

        $posts = $tag_current->posts()
            ->published()
            ->paginate($tag_count);

        $posts_count = $tag_current
            ->posts()
            ->published()
            ->count();

        $this->setSeoFields($tag_current->title, $tag_current->description, $tag_current->keywords);

        return view('front.blog')->with(['posts' => $posts, 'tag_current' => $tag_current, 'posts_count' => $posts_count]);
    }

    // TODO: refactoring

    public static function getPublicUrl($id)
    {
        $public_url = config('app.url');
        $public_url.= '/ru/blog/'.BlogPost::find($id)->category->slug.'/'.BlogPost::find($id)->slug ;
        return $public_url;
    }

    private function countComments($count, $comments)
    {
        foreach ($comments as $comment) {
            $count += $comment->childrens->count();
            $count = $this->countComments($count, $comment->childrens);
        }
        return $count;
    }
}
