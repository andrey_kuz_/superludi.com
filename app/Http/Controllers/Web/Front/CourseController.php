<?php

namespace App\Http\Controllers\Web\Front;

use App\Comment;
use App\CourseLesson;
use App\Http\Controllers\Web\WebController;
use function Clue\StreamFilter\fun;
use Illuminate\Http\Request;

use App\BlogPost;
use App\Season;
use App\Course;
use App\UserInfo;

class CourseController extends WebController
{
    public function index(Request $request)
    {
        $seasons = Season::query()->orderBy('created_at', 'desc')->get();

        $this->setSeoFields(trans('seo.courses.title'), trans('seo.courses.description'), trans('seo.courses.keywords'));

        return view('front.course-list', compact('seasons'));
    }

    public function course($slug)
    {
        $course = Course::where('slug', $slug)->firstOrFail();
        $seasons = Season::with('courses')->get();
        $more_courses = $course->season->courses()->published()->with('speaker')->get();
        $posts = BlogPost::with('social_count')->published()->get()->sortByDesc('social_count.view_count');

        $this->setSeoFields($course->title, $course->description, $course->keywords);

        return view('front.course-page',
            compact('course',
                'seasons',
                'more_courses',
                'posts'
            )
        );
    }

}
