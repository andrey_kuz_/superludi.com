<?php

namespace App\Http\Controllers\Web\Front;

use App\Http\Controllers\Web\WebController;
use App\Info;
use Illuminate\Http\Request;

class InfoController extends WebController
{
    public function index(Request $request, string $slug)
    {
        $info = Info::where('slug', $slug)->firstOrFail();

        $this->setSeoFields($info->title, $info->description, $info->keywords);

        return view('front.info',compact('info'));
    }
}
