<?php

namespace App\Http\Controllers\Web\Front;

use App\Http\Controllers\Web\WebController;
use App\Subscription;
use App\SubscriptionType;
use App\Services\SubscriptionService;
use Illuminate\Support\Facades\Cookie;

class SubscriptionController extends WebController
{
    public function checkout(SubscriptionType $subscription_type)
    {
        $user = $this->auth->toUser();
        $subscription = Subscription::where('user_id', $user->id)
                                    ->where('type_id', $subscription_type->id)
                                    ->where('is_active', 1)
                                    ->first();
        if ($subscription) {
            return redirect()->route('exist_subscription');
        }
        $subscribeService = new SubscriptionService();
        $subscribeService->subscribe($subscription_type, $user->id);
    }

    public function recurring(Subscription $subscription)
    {
        if ($subscription) {
            $subscribeService = new SubscriptionService();
            $subscribeService->recurring($subscription);
        }

//        if ($subscription) {
//            $subscribeService = new SubscriptionService();
//            $response = $subscribeService->recurring($subscription);
//
//            if ($response) {
//                return redirect()->route('thank_payment');
//            } else {
//                return redirect()->route('error_payment');
//            }
//        }
    }
}
