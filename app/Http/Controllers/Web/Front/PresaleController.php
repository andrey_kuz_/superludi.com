<?php

namespace App\Http\Controllers\Web\Front;

use App\Http\Controllers\Controller;
use App\Http\Requests\Front\PresaleRequest;
use App\Services\SubscriptionService;
use App\SubscriptionType;
use App\User;
use Illuminate\Http\Request;

class PresaleController extends Controller
{
    public function index(Request $request)
    {
        return view('front.presale');
    }

    public function createSubscription(PresaleRequest $request)
    {
        $custom_data = [];
        if($request->promo) {
            $custom_data['promo'] = $request->promo;
        }
        $user = User::where('email', $request->email)->first();

        if (!$user) {
            $user = User::create([
                'email' => $request->email,
                'password' => str_random(12)
            ]);
        }

        if (!$user->hasRole('customer')) {
            $user->assignRole(['customer', 'presale']);
        } else {
            $user->assignRole('presale');
        }

        $user->info->phone = $request->phone;
        $user->info->full_name = $request->name;
        $user->info->save();

        $subscription_type = SubscriptionType::where('name', 'presale')->first();

        $subscription = $user->subscriptions()->whereHas('subscription_type', function ($q){
            $q->where('name', 'presale')->where('is_active', 0);
        })->first();

        if ($subscription && $request->promo) {
            $subscription->custom_data = json_encode($custom_data);
            $subscription->save();
        }
        $subscribeService = new SubscriptionService();
        $subscribeService->presaleSubscribe($subscription_type, $user->id, $subscription, $custom_data);
    }
}
