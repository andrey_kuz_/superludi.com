<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Auth\FBRegisterRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\Auth\ForgotPasswordRequest;
use App\Mail\ForgotPassword;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;


class AuthController extends ApiController {

    public function user(Request $request)
    {

        $user = $this->auth->toUser();

        return response()->json($user);
    }

    public function login(Request $request)
    {
        $redirect_to = null;
        if ($request->cookies->has('buy_subscription')) {
            $redirect_to = route('subscription.checkout', Crypt::decryptString($request->cookies->get('buy_subscription')));
        }
        elseif ((int)$request->input('just_registered', false)) {
            $redirect_to = route('courses');
        }

        $credentials = $request->only('email', 'password');

        $token = $this->auth->attempt($credentials);

        if (!$token) {
            throw new UnauthorizedHttpException('jwt-auth', trans('error.exceptions.auth.invalid_data'));
        }

        /**
         * @var $user User
         */
        $user = $this->auth->toUser($token);

        if ($request->has('fb_id')) {
            $user->fb_id = $request->fb_id;
            $user->save();
        }

        if ($user->hasRole('presale')) {
            $user->removeRole('presale');
        }

        $permissions = [
            'roles'     => $user->roles()->pluck('name'),
            'rights'    => $user->permissions()->pluck('name')
        ];

        return response()
            ->json(compact('token', 'permissions', 'redirect_to'))
            ->withCookie(Cookie::make('token', Crypt::encrypt($token, false)))
            ->withCookie(Cookie::forget('buy_subscription'));
    }


    public function logout()
    {
        try{
            if ($this->auth->getToken()) {
                $this->auth->invalidate($this->auth->getToken());
            }
        }
        catch(\Exception $e) {
            if ($e instanceof TokenExpiredException) {
                $token = $this->auth->getToken();
                $refresh_token = $this->auth->refresh($token);
                $this->auth->invalidate($refresh_token);
            }
        }

        return response()
            ->json(['success' => true], 200)
            ->withCookie(Cookie::forget('token'));
    }


    public function refresh()
    {
        $token = $this->auth->refresh($this->auth->getToken());

        return response()
            ->json(compact('token'))
            ->withCookie(Cookie::make('token', Crypt::encrypt($token, false)));
    }

    public function register(RegisterRequest $request)
    {
        $user = User::create([
            'email' => $request->email,
            'password' => $request->password,
        ]);

        if ($request->utm_tags) {
            $utm_tags = json_encode($request->utm_tags);
            $user->utm_tags = $utm_tags;
            $user->save();
        }

        $user->assignRole('customer');

        $user->info->full_name = $request->name;
        $user->info->phone = $request->phone;
        $user->info->save();

        return response()->json(['success' => true], 200);
    }

    public function registerByEmail(FBRegisterRequest $request)
    {
        $user = User::create([
            'email' => $request->email,
            'fb_id' => $request->fb_id
        ]);
        $user->assignRole('customer');

        $image = $this->saveFBAvatar($request->avatar);

        $user->info->full_name = $request->name;
        $user->info->avatar = $image;
        $user->info->save();

        return response()->json(['user' => $user], 200);
    }

    public function loginByFB(Request $request)
    {
        try {

            $redirect_to = null;
            if ($request->cookies->has('buy_subscription')) {
                $redirect_to = route('subscription.checkout', Crypt::decryptString($request->cookies->get('buy_subscription')));
            }
            elseif ((int)$request->input('just_registered', false)) {
                $redirect_to = route('courses');
            }

            $fb_user = User::where('fb_id', $request->fb_id)->first();
            $token = $this->auth->fromUser($fb_user);

        } catch (\Exception $e) {

            return response()->json([
                'errors' => [
                    'token' => ['failed_to_create_token']
                ]
            ], 500);
        }

        return response()
            ->json(compact('token', 'redirect_to'))
            ->withCookie(Cookie::make('token', Crypt::encrypt($token, false)))
            ->withCookie(Cookie::forget('buy_subscription'));

    }

    private function saveFBAvatar($avatar)
    {
        // TODO: look for the laravel-style method

        $file = file_get_contents($avatar);
        $file_info = finfo_open();
        $mime_type = finfo_buffer($file_info, $file, FILEINFO_MIME_TYPE);
        $f = base64_encode($file);

        $image['data'] = 'data:' . $mime_type . ';base64,' . $f;
        $image['key'] = 'avatar';
        $image['multiple'] = false;

        return $image;
    }

    public function forgotPassword(ForgotPasswordRequest $request)
    {
        $user = User::where('email', $request->email)->first();
        $password = str_random(12);
        $user->password = $password;
        if($user->save()) {
            Mail::to($request->email)->send(new ForgotPassword($password));
        }
        return response()->json(['success' => true, 'message' => trans('message.forgotPassword.success')], 200);
    }
}
