<?php

namespace App\Http\Controllers\Api\Admin;

use App\BlogPost;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Admin\AdminRequest as Request;

class BlogPostController extends ApiController
{
    public function index(Request $request)
    {
        $query = BlogPost::query();

        return $this->getListResponse($query, $request);
    }

    public function show(Request $request, BlogPost $blog_post)
    {
        return $this->getItemResponse($blog_post, $request);
    }

    public function store(Request $request)
    {
        $blog_post = new BlogPost();

        $blog_post->fill($request->except('translations_array'));

        $blog_post->save();

        $blog_post->fill($request->input('translations_array'));

        $blog_post->save();

        return $this->getItemResponse($blog_post, $request);
    }

    public function update(Request $request, BlogPost $blog_post)
    {
        $blog_post->update($request->except('translations_array'));

        $blog_post->save();

        $blog_post->update($request->input('translations_array'));

        $blog_post->save();

        return $this->getItemResponse($blog_post, $request);
    }

    public function destroy(Request $request, BlogPost $blog_post)
    {
        $blog_post->tags()->detach($blog_post->tags_ids);
        $blog_post->delete();

        return $this->getItemResponse($blog_post, $request);
    }
}
