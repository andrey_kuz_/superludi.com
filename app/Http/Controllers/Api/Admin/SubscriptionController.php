<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Admin\AdminRequest as Request;
use App\Subscription;
use App\SubscriptionType;
use Carbon\Carbon;

class SubscriptionController extends ApiController
{
    public function index(Request $request)
    {
        $query = Subscription::query();

        return $this->getListResponse($query, $request);
    }

    public function show(Request $request, Subscription $subscription)
    {
        return $this->getItemResponse($subscription, $request);
    }

    public function store(Request $request)
    {

        $subscription = new Subscription();

        $subscription->fill($request->except(['translations_array']));

        $subscription->save();

        /*$subscription_type->fill($request->input('translations_array'));

        $subscription_type->save();*/

        return $this->getItemResponse($subscription, $request);
    }

    public function update(Request $request, Subscription $subscription)
    {

        $subscription->update($request->except(['translations_array']));

        $subscription->save();

        /*$subscription->update($request->input('translations_array'));

        $subscription->save();*/

        return $this->getItemResponse($subscription, $request);
    }

    public function destroy(Request $request, Subscription $subscription)
    {
        $subscription->delete();
        return $this->getItemResponse($subscription, $request);
    }
}
