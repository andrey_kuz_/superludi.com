<?php

namespace App\Http\Controllers\Api\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;

use Spatie\Permission\Models\Role;

class RoleController extends ApiController
{
    public function index(Request $request)
    {
        $perPage = 10;
        $query = Role::query();
        return response()->json($query->paginate($perPage, ['*'], 'page'));
    }
}
