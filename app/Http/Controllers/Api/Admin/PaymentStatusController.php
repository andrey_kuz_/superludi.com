<?php

namespace App\Http\Controllers\Api\Admin;

use App\PaymentStatus;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Admin\AdminRequest as Request;

class PaymentStatusController extends ApiController
{
    public function index(Request $request)
    {
        $query = PaymentStatus::query();

        return $this->getListResponse($query, $request);
    }

    public function show(Request $request, PaymentStatus $payment_status)
    {
        return $this->getItemResponse($payment_status, $request);
    }

    public function update(Request $request, PaymentStatus $payment_status)
    {
        $payment_status->update($request->except('translations_array'));

        $payment_status->save();

        $payment_status->update($request->input('translations_array'));

        $payment_status->save();

        return $this->getItemResponse($payment_status, $request);
    }
}
