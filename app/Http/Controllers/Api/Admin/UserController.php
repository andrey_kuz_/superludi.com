<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Requests\Admin\User\UpdateRequest;
use App\User;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Admin\AdminRequest as Request;
use Spatie\Permission\Models\Role;

class UserController extends ApiController
{
    public function index(Request $request)
    {
        $query = User::query();

        return $this->getListResponse($query, $request);
    }

    public function show(Request $request, User $user)
    {
        $user->info;

        return $this->getItemResponse($user, $request);
    }

    public function store(Request $request)
    {
        $user = new User();

        $user->fill($request->only(['email', 'password']));

        $user->save();

        return $this->getItemResponse($user, $request);
    }

    public function update(UpdateRequest $request, User $user)
    {
        $user->fill($request->only(['email', 'password']));

        $user->save();

        if ($request->has('roles_ids')) {

            $user->syncRoles($request->input('roles_ids'));
        }

        $user->info->fill($request->input('info'));

        $user->info->save();

        return $this->getItemResponse($user, $request);
    }

    public function destroy(Request $request, User $user)
    {
        $user->delete();

        return $this->getItemResponse($user, $request);
    }

}
