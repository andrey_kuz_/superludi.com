<?php

namespace App\Http\Controllers\Api\Admin;

use App\BlogCategory;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Admin\AdminRequest as Request;

class BlogCategoryController extends ApiController
{
    public function index(Request $request)
    {
        $query = BlogCategory::query();

        return $this->getListResponse($query, $request);
    }

    public function show(Request $request, BlogCategory $blog_category)
    {
        return $this->getItemResponse($blog_category, $request);
    }

    public function store(Request $request)
    {
        $blog_category = new BlogCategory();

        $blog_category->fill($request->except('translations_array'));

        $blog_category->save();

        $blog_category->fill($request->input('translations_array'));

        $blog_category->save();

        return $this->getItemResponse($blog_category, $request);
    }

    public function update(Request $request, BlogCategory $blog_category)
    {
        $blog_category->update($request->except('translations_array'));

        $blog_category->save();

        $blog_category->update($request->input('translations_array'));

        $blog_category->save();

        return $this->getItemResponse($blog_category, $request);
    }

    public function destroy(Request $request, BlogCategory $blog_category)
    {
        $blog_category->delete();

        return $this->getItemResponse($blog_category, $request);
    }
}
