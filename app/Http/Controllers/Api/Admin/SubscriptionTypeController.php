<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Admin\AdminRequest as Request;
use App\SubscriptionType;
use Carbon\Carbon;

class SubscriptionTypeController extends ApiController
{
    public function index(Request $request)
    {
        $query = SubscriptionType::query();

        return $this->getListResponse($query, $request);
    }

    public function show(Request $request, SubscriptionType $subscription_type)
    {
        return $this->getItemResponse($subscription_type, $request);
    }

    public function store(Request $request)
    {

        $subscription_type = new SubscriptionType();

        $subscription_type->fill($request->except(['translations_array']));

        $subscription_type->save();

        $subscription_type->fill($request->input('translations_array'));

        $subscription_type->save();

        return $this->getItemResponse($subscription_type, $request);
    }

    public function update(Request $request, SubscriptionType $subscription_type)
    {

        $subscription_type->update($request->except(['translations_array']));

        $subscription_type->save();

        $subscription_type->update($request->input('translations_array'));

        $subscription_type->save();

        return $this->getItemResponse($subscription_type, $request);
    }

    public function destroy(Request $request, SubscriptionType $subscription_type)
    {
        $subscription_type->seasons()->detach($subscription_type->seasons_ids);
        $subscription_type->delete();

        return $this->getItemResponse($subscription_type, $request);
    }
}
