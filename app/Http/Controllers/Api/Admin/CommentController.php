<?php

namespace App\Http\Controllers\Api\Admin;

use App\Comment;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Admin\AdminRequest as Request;

class CommentController extends ApiController
{
    public function index(Request $request)
    {
        $query = Comment::query();
        return $this->getListResponse($query, $request);
    }

    public function show(Request $request, Comment $comment)
    {

        return $this->getItemResponse($comment, $request);
    }

    public function update(Request $request, Comment $comment)
    {
        $comment->update($request->except('translations_array'));

        $comment->save();

        return $this->getItemResponse($comment, $request);
    }

    public function destroy(Request $request, Comment $comment)
    {
        $comment->delete();

        return $this->getItemResponse($comment, $request);
    }
}
