<?php

namespace App\Http\Controllers\Api\Admin;

use App\CourseLessonMaterial;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Admin\AdminRequest as Request;

class CourseLessonMaterialController extends ApiController
{
    public function index(Request $request)
    {
        $query = CourseLessonMaterial::query();

        return $this->getListResponse($query, $request);
    }

    public function show(Request $request, CourseLessonMaterial $course_lesson_material)
    {
        return $this->getItemResponse($course_lesson_material, $request);
    }

    public function store(Request $request)
    {
        $course_lesson_material = new CourseLessonMaterial();

        $course_lesson_material->fill($request->all());

        $course_lesson_material->save();

        return $this->getItemResponse($course_lesson_material, $request);
    }

    public function update(Request $request, CourseLessonMaterial $course_lesson_material)
    {
        $course_lesson_material->update($request->all());

        $course_lesson_material->save();

        return $this->getItemResponse($course_lesson_material, $request);
    }

    public function destroy(Request $request, CourseLessonMaterial $course_lesson_material)
    {
        $course_lesson_material->delete();

        return $this->getItemResponse($course_lesson_material, $request);
    }
}
