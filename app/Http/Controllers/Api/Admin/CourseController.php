<?php

namespace App\Http\Controllers\Api\Admin;

use App\Course;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Admin\AdminRequest as Request;

class CourseController extends ApiController
{
    public function index(Request $request)
    {
        $query = Course::query();

        return $this->getListResponse($query, $request);
    }

    public function show(Request $request, Course $course)
    {
        return $this->getItemResponse($course, $request);
    }

    public function store(Request $request)
    {
        $course = new Course();

        $course->fill($request->except('translations_array'));

        $course->save();

        $course->fill($request->input('translations_array'));

        $course->save();

        return $this->getItemResponse($course, $request);
    }

    public function update(Request $request, Course $course)
    {
        $course->update($request->except('translations_array'));

        $course->save();

        $course->update($request->input('translations_array'));

        $course->save();

        return $this->getItemResponse($course, $request);
    }

    public function destroy(Request $request, Course $course)
    {
        $course->delete();

        return $this->getItemResponse($course, $request);
    }
}
