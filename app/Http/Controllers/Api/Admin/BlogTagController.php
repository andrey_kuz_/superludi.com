<?php

namespace App\Http\Controllers\Api\Admin;

use App\BlogTag;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Admin\AdminRequest as Request;

class BlogTagController extends ApiController
{
    public function index(Request $request)
    {
        $query = BlogTag::query();

        return $this->getListResponse($query, $request);
    }

    public function show(Request $request, BlogTag $blog_tag)
    {

        return $this->getItemResponse($blog_tag, $request);
    }

    public function store(Request $request)
    {
        $blog_tag = new BlogTag();

        $blog_tag->fill($request->except('translations_array'));

        $blog_tag->save();

        $blog_tag->fill($request->input('translations_array'));

        $blog_tag->save();

        return $this->getItemResponse($blog_tag, $request);
    }

    public function update(Request $request, BlogTag $blog_tag)
    {
        $blog_tag->update($request->except('translations_array'));

        $blog_tag->save();

        $blog_tag->update($request->input('translations_array'));

        $blog_tag->save();

        return $this->getItemResponse($blog_tag, $request);
    }

    public function destroy(Request $request, BlogTag $blog_tag)
    {
        $blog_tag->delete();

        return $this->getItemResponse($blog_tag, $request);
    }
}
