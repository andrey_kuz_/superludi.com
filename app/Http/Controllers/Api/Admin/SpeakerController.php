<?php

namespace App\Http\Controllers\Api\Admin;

use App\Speaker;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Admin\AdminRequest as Request;

class SpeakerController extends ApiController
{
    public function index(Request $request)
    {
        $query = Speaker::query();

        return $this->getListResponse($query, $request);
    }

    public function show(Request $request, Speaker $speaker)
    {
        return $this->getItemResponse($speaker, $request);
    }

    public function store(Request $request)
    {
        $speaker = new Speaker();

        $speaker->fill($request->except('translations_array'));

        $speaker->save();

        $speaker->fill($request->input('translations_array'));

        $speaker->save();

        return $this->getItemResponse($speaker, $request);
    }

    public function update(Request $request, Speaker $speaker)
    {
        $speaker->update($request->except('translations_array'));

        $speaker->save();

        $speaker->update($request->input('translations_array'));

        $speaker->save();

        return $this->getItemResponse($speaker, $request);
    }

    public function destroy(Request $request, Speaker $speaker)
    {
        $speaker->delete();

        return $this->getItemResponse($speaker, $request);
    }
}
