<?php

namespace App\Http\Controllers\Api\Admin;

use App\Info;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Admin\AdminRequest as Request;

class InfoController extends ApiController
{
    public function index(Request $request)
    {
        $query = Info::query();

        return $this->getListResponse($query, $request);
    }

    public function show(Request $request, Info $info)
    {
        return $this->getItemResponse($info, $request);
    }


    public function update(Request $request, Info $info)
    {
        $info->update($request->except('translations_array'));

        $info->save();

        $info->update($request->input('translations_array'));

        $info->save();

        return $this->getItemResponse($info, $request);
    }

    public function destroy(Request $request, Info $info)
    {
        $info->delete();

        return $this->getItemResponse($info, $request);
    }
}
