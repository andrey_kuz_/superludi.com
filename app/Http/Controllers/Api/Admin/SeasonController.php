<?php

namespace App\Http\Controllers\Api\Admin;

use App\Season;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Admin\AdminRequest as Request;

class SeasonController extends ApiController
{
    public function index(Request $request)
    {
        $query = Season::query();

        return $this->getListResponse($query, $request);
    }

    public function show(Request $request, Season $season)
    {
        return $this->getItemResponse($season, $request);
    }

    public function store(Request $request)
    {
        $season = new Season();

        $season->fill($request->except('translations_array'));

        $season->save();

        $season->fill($request->input('translations_array'));

        $season->save();

        return $this->getItemResponse($season, $request);
    }

    public function update(Request $request, Season $season)
    {
        $season->update($request->except('translations_array'));

        $season->save();

        $season->update($request->input('translations_array'));

        $season->save();

        return $this->getItemResponse($season, $request);
    }

    public function destroy(Request $request, Season $season)
    {
        $season->delete();

        return $this->getItemResponse($season, $request);
    }


}
