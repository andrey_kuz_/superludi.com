<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Admin\AdminRequest as Request;

use App\MailTemplate;

class MailTemplateController extends ApiController
{
    public function index(Request $request)
    {
        $query = MailTemplate::query();

        return $this->getListResponse($query, $request);
    }

    public function show(Request $request, MailTemplate $mail_template)
    {
        return $this->getItemResponse($mail_template, $request);
    }

    public function update(Request $request, MailTemplate $mail_template)
    {
        $mail_template->update($request->except('translations_array'));

        $mail_template->save();

        $mail_template->update($request->input('translations_array'));

        $mail_template->save();

        return $this->getItemResponse($mail_template, $request);
    }

}
