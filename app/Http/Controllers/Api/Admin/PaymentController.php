<?php

namespace App\Http\Controllers\Api\Admin;

use App\Payment;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Admin\AdminRequest as Request;

class PaymentController extends ApiController
{
    public function index(Request $request)
    {
        $query = Payment::query();

        return $this->getListResponse($query, $request);
    }

    public function show(Request $request, Payment $payment)
    {
        return $this->getItemResponse($payment, $request);
    }
}
