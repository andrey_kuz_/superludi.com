<?php

namespace App\Http\Controllers\Api\Admin;

use App\CourseLesson;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Admin\AdminRequest as Request;

class CourseLessonController extends ApiController
{
    public function index(Request $request)
    {
        $query = CourseLesson::query();

        return $this->getListResponse($query, $request);
    }

    public function show(Request $request, CourseLesson $course_lesson)
    {
        return $this->getItemResponse($course_lesson, $request);
    }

    public function store(Request $request)
    {
        $course_lesson = new CourseLesson();

        $course_lesson->fill($request->except('translations_array'));

        $course_lesson->save();

        $course_lesson->fill($request->input('translations_array'));

        $course_lesson->save();

        return $this->getItemResponse($course_lesson, $request);
    }

    public function update(Request $request, CourseLesson $course_lesson)
    {
        $course_lesson->update($request->except('translations_array'));

        $course_lesson->save();

        $course_lesson->update($request->input('translations_array'));

        $course_lesson->save();

        return $this->getItemResponse($course_lesson, $request);
    }

    public function destroy(Request $request, CourseLesson $course_lesson)
    {
        $course_lesson->delete();

        return $this->getItemResponse($course_lesson, $request);
    }
}
