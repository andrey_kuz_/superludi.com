<?php

namespace App\Http\Controllers\Api\Front;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Front\Mailinglist\SubscribeRequest;
use App\Services\GetResponseService;


class MailinglistController extends ApiController
{
    public function subscribe(SubscribeRequest $request)
    {
        $getresponse = new GetResponseService();
        $response = $getresponse->subscribe($request->email);

        if (isset($response->code)){

            switch ($response->code) {
                case 1008:
                    $message = trans('mail.exist');
                    break;
                default:
                    $message = trans('mail.error');
            }
        } else {
            $message = trans('mail.thanks');
        }

        return response()->json($message);
    }

}
