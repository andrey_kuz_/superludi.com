<?php

namespace App\Http\Controllers\Api\Front;

use App\BlogTag;
use App\CourseLesson;
use App\Http\Controllers\Api\ApiController;
use App\BlogPost;
use App\Comment;
use Illuminate\Http\Request;
use App\Http\Requests\Front\Comment\AddRequest;


class CommentController extends ApiController
{
    public function addComment($class, $id, AddRequest $request)
    {
        $user = $this->auth->toUser()->id;
        $errors['errors'][] = trans('message.comment.add_error');

        if ($class =='blog_post') {
            $post = BlogPost::find($id);
            $c = new Comment();
            $c->content = $request->input('content');
            if($request->input('parent_id')) {
                $c->parent_id = $request->input('parent_id');
            } else {
                $c->parent_id = 0;
            }
            $c->user_id = $user;
            if($post->comments()->save($c)) {
                return response()->json($c,200);
            } else {

                return response()->json($errors,400);
            }
        } elseif ($class =='course_lesson') {
            $course = CourseLesson::find($id);
            $c = new Comment();
            $c->content = $request->input('content');
            if ($request->input('parent_id')) {
                $c->parent_id = $request->input('parent_id');
            } else {
                $c->parent_id = 0;
            }
            $c->user_id = $user;
            if ($course->comments()->save($c)) {
                return response()->json($c,200);
            } else {
                return  response()->json($errors,400);
            }
        }
    }

    public function showMore(Request $request)
    {
        $token = $this->auth->getToken();
        $user = $this->auth->toUser($token);
        $model = 'App\\' . studly_case(strtolower(str_singular($request->type)));
        $commentable = $model::find($request->id);

        if ($commentable->comments) {
            $comments = $commentable->comments()->where('id', $request->comment_id)->published()->with('childrens')->get();
            $replace = true;
            $depth = 0;
        }

        $child = false;
        if (!empty($comments->first())) {
            $child = true;
        }

        $html = view('front.comment-item')
            ->with(compact('comments', 'user', 'commentable', 'replace', 'depth'))
            ->render();

        return response()->json(['comments' => $html, 'has_child' => $child]);
    }

    public function showOther(Request $request)
    {
        $token = $this->auth->getToken();
        $user = $this->auth->toUser($token);
        $model = 'App\\' . studly_case(strtolower(str_singular($request->type)));
        $commentable = $model::find($request->id);

        if ($commentable->comments) {
            $comments = $commentable->comments()->where('parent_id', 0)->published()->with('childrens')->offset($request->offset)->limit($commentable::VISIBLE_COMMENTS)->get();
            $depth = 0;
        }

        $count_all_comments = $commentable->comments()->published()->count();
        $parent_visible_comments = $commentable->comments()->where('parent_id', 0)->published()->with('childrens')->limit($request->offset)->get();
        $count_visible_comments = $this->countComments($parent_visible_comments->count(), $parent_visible_comments);
        $count_current_comments = $this->countComments($comments->count(), $comments);
        $count_available_comments = $count_all_comments - ($count_visible_comments + $count_current_comments);

        $html = view('front.comment-item')
            ->with(compact('comments', 'user', 'commentable', 'depth'))
            ->render();

        return response()->json([
            'comments' => $html,
            'count' => $count_available_comments
        ]);
    }

    private function countComments($count, $comments)
    {
        foreach ($comments as $comment) {
            $count += $comment->childrens->count();
            $count = $this->countComments($count, $comment->childrens);
        }
        return $count;
    }

}
