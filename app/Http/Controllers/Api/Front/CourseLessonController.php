<?php

namespace App\Http\Controllers\Api\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;
use App\CourseLesson;
use App\UserLessonState;

class CourseLessonController extends ApiController
{
    public function show(CourseLesson $course_lesson)
    {
        $token = $this->auth->getToken();
        $user = $this->auth->toUser($token);

        $comments = $course_lesson->comments()->where('parent_id', 0)->published()->with('childrens')->limit(CourseLesson::VISIBLE_COMMENTS)->get();
        $commentable = $course_lesson;

        $count_all_comments = $course_lesson->comments()->published()->count();
        $count_visible_comments = $this->countComments($comments->count(), $comments);
        $count_available_comments = $count_all_comments - $count_visible_comments;

        $html = view('front.comments')
            ->with(compact('comments', 'user', 'commentable', 'count_available_comments'))
            ->render();
        if ($course_lesson->hasAccess($user)) {
            return response()->json([
                'lesson' => $course_lesson,
                'course_materials' => $course_lesson->course->materials,
                'lesson_materials' => $course_lesson->materials,
                'comments' => $html
            ]);
        } else {
            return response()->json('access_false');
        }
    }

    public function showState(CourseLesson $course_lesson)
    {
        $user = $this->auth->toUser();
        $user_lesson_state = UserLessonState::where('user_id', $user->id)->where('course_lesson_id', $course_lesson->id)->first();

        if ($user_lesson_state) {
            $start_time = $user_lesson_state->time_start;
        } else {
            $start_time = 0;
        }

        return response()->json($start_time);
    }

    public function storeState(Request $request, CourseLesson $course_lesson)
    {
        $user = $this->auth->toUser();
        $user_lesson_state = UserLessonState::where('user_id', $user->id)->where('course_lesson_id', $course_lesson->id)->first();

        if (!$user_lesson_state) {
            UserLessonState::create([
                'user_id' => $user->id,
                'course_lesson_id' => $course_lesson->id,
                'time_start' => $request->time
            ]);
        } else {
            $user_lesson_state->update([
                'time_start' => $request->time
            ]);
        }

        return response()->json(['status' => 'success'], 200);
    }

    private function countComments($count, $comments)
    {

        foreach ($comments as $comment) {
            $count += $comment->childrens->count();
            $count = $this->countComments($count, $comment->childrens);
        }

        return $count;
    }
}
