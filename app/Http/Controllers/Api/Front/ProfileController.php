<?php

namespace App\Http\Controllers\Api\Front;

use App\Http\Requests\Front\Profile\UpdateCityRequest;
use App\Http\Requests\Front\Profile\UpdateEmailRequest;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Front\Profile\UpdateNameRequest;
use App\Http\Requests\Front\Profile\UpdatePasswordRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;


class ProfileController extends ApiController
{
    public function updateEmail(UpdateEmailRequest $request)
    {
        $user = $this->auth->toUser();
        $user->update(['email' => $request->email]);
        return response()->json($user->email);
    }

    public function updatePassword(UpdatePasswordRequest $request)
    {
        $user = $this->auth->toUser();
        $user->update(['password' => $request->password]);
        return response()->json($user->password);
    }

    public function updateName(UpdateNameRequest $request)
    {
        $user = $this->auth->toUser();
        $user->info->full_name = $request->full_name;
        $user->info->save();

        return response()->json($user->info->full_name);
    }

    public function updateCity(UpdateCityRequest $request)
    {
        $user = $this->auth->toUser();
        $user->info->city = $request->city;
        $user->info->save();

        return response()->json($user->info->city);
    }

    public function updateAvatar(Request $request)
    {
        $user = $this->auth->toUser();
        $files = $request->file('avatar');

        $file = $files[0];
        $path = $file->getRealPath();
        $file_name = $file->getClientOriginalName();
        $mime_type = $file->getMimeType();

        $f = File::get($path);
        $f = base64_encode($f);

        $image['data'] = 'data:' . $mime_type . ';base64,' . $f;
        $image['title'] = $file_name;
        $image['key'] = 'avatar';
        $image['multiple'] = false;

        $user->info->avatar = $image;
        $user->info->save();

        return response()->json($user->info->avatar['src']);
    }


}
