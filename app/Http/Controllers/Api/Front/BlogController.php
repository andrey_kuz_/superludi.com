<?php

namespace App\Http\Controllers\Api\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;
use App\BlogPost;

class BlogController extends ApiController
{
    public function showMore(Request $request)
    {
        $classes = ['small', 'large', 'horizontal tiny', 'normal', 'large centered', 'normal', 'small'];
        $offset = $request->count;
        $limit = config('blog.main_show_count_post');

        $posts = BlogPost::published()->offset($offset)->limit($limit)->get();
        $count_all_posts = BlogPost::published()->count();
        $count_visible_posts = $offset  + $posts->count();

        $count_available_posts = $count_all_posts - $count_visible_posts;

        $html = view('front.parts.blog.ajax_post_list')
            ->with(compact('posts', 'classes'))
            ->render();

        return response()->json([
            'posts' => $html,
            'count' => $count_available_posts
        ]);
    }
}
