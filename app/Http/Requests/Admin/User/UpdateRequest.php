<?php

namespace App\Http\Requests\Admin\User;

use App\Http\Requests\Admin\AdminRequest;

class UpdateRequest extends AdminRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:user,email, ' . $this->route('user')->id,
            'password' => 'nullable|same:password_confirm',
        ];
    }

    public function  messages()
    {
        return [
            'email.required' => trans('error.user.email.required'),
            'email.email' => trans('error.user.email.email'),
            'email.unique' => trans('error.user.email.unique'),
            'password.required' => trans('error.user.password.required'),
            'password.same' => trans('error.user.password.same')
        ];
    }
}
