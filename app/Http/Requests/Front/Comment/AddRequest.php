<?php

namespace App\Http\Requests\Front\Comment;

use Illuminate\Foundation\Http\FormRequest;

class AddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'content' => 'required',
            'parent_id' => 'exists:comments,id'
        ];
    }

    public function messages()
    {
        return [
            'content.required' => trans('error.comment.content.required'),
            'parent_id.exists' => trans('error.comment.parent_id.exists')
        ];
    }
}
