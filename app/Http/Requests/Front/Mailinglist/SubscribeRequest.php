<?php

namespace App\Http\Requests\Front\Mailinglist;

use Illuminate\Foundation\Http\FormRequest;

class SubscribeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'user_agree_subscribe' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'email.required' => trans('error.mailinglist.email.required'),
            'email.email' => trans('error.mailinglist.email.email'),
            'user_agree_subscribe.required' => trans('error.mailinglist.user_agree.required')
        ];
    }
}
