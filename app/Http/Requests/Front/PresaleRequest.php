<?php

namespace App\Http\Requests\Front;

use App\User;
use Illuminate\Foundation\Http\FormRequest;

class PresaleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//        dd($this->messages());
        return [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => [
                'required',
                function ($attribute, $value, $fail) {
                    $str = preg_replace("/[^0-9]/", '', $value);
                    if (strlen($str) < 12) {
                        $fail(trans('error.user.phone.invalid'));
                    }
                },
            ],
        ];
    }

    public function messages()
    {
        return [
            'name.required' => trans('error.user.name.required'),
            'email.required' => trans('error.user.email.required'),
            'email.email' => trans('error.user.email.email'),
            'phone.required' => trans('error.user.phone.required'),
        ];
    }
}
