<?php

namespace App\Http\Requests\Front\Profile;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required|min:8|same:password_confirm',
        ];
    }

    public function messages()
    {
        return [
            'password.required' => trans('error.profile.password.required'),
            'password.min' => trans('error.profile.password.min'),
            'password.same' => trans('error.profile.password.same'),
        ];
    }
}
