<?php

namespace App\Http\Requests\Front\Profile;

use Illuminate\Foundation\Http\FormRequest;

class UpdateEmailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user_id = $this->user()->id;

        return [
            'email' => 'required|email|same:email_confirm|unique:user,email, ' . $user_id
        ];
    }

    public function messages()
    {
        return [
            'email.required' => trans('error.profile.email.required'),
            'email.email' => trans('error.profile.email.email'),
            'email.unique' => trans('error.profile.email.unique'),
            'email.same' => trans('error.profile.email.same'),
        ];
    }
}
