<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class ForgotPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|exists:user,email'
        ];
    }

    public function messages()
    {
        return [
            'email.required' => trans('error.user.email.required'),
            'email.email' => trans('error.user.email.email'),
            'email.exists' => trans('error.user.email.exists')
        ];
    }
}