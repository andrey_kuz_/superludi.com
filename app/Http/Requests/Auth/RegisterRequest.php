<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:user,email',
            'password' => 'required|min:8',
            'phone' => [
                'required',
                function ($attribute, $value, $fail) {
                    $str = preg_replace("/[^0-9]/", '', $value);
                    if (strlen($str) < 12) {
                        $fail(trans('error.user.phone.invalid'));
                    }
                },
            ],
        ];
    }

    public function messages()
    {
        return [
            'name.required' => trans('error.user.name.required'),
            'email.required' => trans('error.user.email.required'),
            'email.email' => trans('error.user.email.email'),
            'email.unique' => trans('error.user.email.unique'),
            'password.required' => trans('error.user.password.required'),
//            'password.same' => trans('error.user.password.same'),
            'password.min' => trans('error.user.password.min'),
            'phone.required' => trans('error.user.phone.required'),
        ];
    }
}
