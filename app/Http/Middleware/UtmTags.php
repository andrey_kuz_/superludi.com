<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Session;

class UtmTags
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        $utm_tags = [];
        if ($request->has('utm_source')) {
            $utm_tags['utm_source'] = $request->utm_source;
        }
        if ($request->has('utm_medium')) {
            $utm_tags['utm_medium'] = $request->utm_medium;
        }
        if ($request->has('utm_campaign')) {
            $utm_tags['utm_campaign'] = $request->utm_campaign;
        }
        if ($request->has('utm_term')) {
            $utm_tags['utm_term'] = $request->utm_term;
        }
        if ($request->has('utm_content')) {
            $utm_tags['utm_content'] = $request->utm_content;
        }
        if ($utm_tags) {
            $utm_tags = json_encode($utm_tags);
            Session::put('utm_tags', $utm_tags);
        }

        return $next($request);
    }
}
