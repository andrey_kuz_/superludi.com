<?php

namespace App\Http\Middleware;

use App\SubscriptionType;
use Illuminate\Support\Facades\View;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\JWTAuth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Cookie;

class ViewsData
{
    /**
     * The authentication factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(JWTAuth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        $user = null;
        $token = null;

        try{
            $token = $this->auth->getToken();
            if ($token) {
                $user = $this->auth->toUser($token);
            }
        } catch(\Exception $e) {
            $user = null;
            if ($e instanceof TokenExpiredException) {
                try {
                    $refresh_token = $this->auth->refresh($this->auth->getToken());
                    Cookie::queue('token', $refresh_token);
                    View::composer('front.*', function ($view) use (&$refresh_token) {
                        $view->with(compact('refresh_token'));
                    });
                    $user = $this->auth->toUser($refresh_token);
                }
                catch (\Exception $e) {}
            }
        }

        if ($token && $request->input('refresh_token', 0)) {
            View::composer('front.*', function ($view) use (&$token) {
                $view->with([
                    'refresh_token' => $token
                ]);
            });
        }

        $active_subscription = SubscriptionType::with('courses')
            ->where('name', '<>', 'presale')
            ->active()
            ->minPrice()
            ->first();

        View::composer('front.*', function ($view) use (&$user, &$active_subscription) {

            $view->with(compact('user', 'active_subscription'));
        });

        return $next($request);
    }
}
