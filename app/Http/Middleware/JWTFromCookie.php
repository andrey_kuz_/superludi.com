<?php

namespace App\Http\Middleware;

class JWTFromCookie
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        if (!$request->has('token')) {
            $token = $request->cookie('token', false);
            $request->request->add(['token' => $token]);
        }
        return $next($request);
    }
}
