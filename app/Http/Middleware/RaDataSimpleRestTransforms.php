<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\TransformsRequest;

class RaDataSimpleRestTransforms extends TransformsRequest {

    protected $except = [
        
    ];

    protected function transform($key, $value)
    {
        if (in_array($key, $this->except, true)) {
            return $value;
        }

        if (is_string($value)) {
            $rv = json_decode($value, true);
            if (!is_null($rv)) {
                return $rv;
            }
            else {
                return $value;
            }
        }
        else {
            return $value;
        }
    }
}