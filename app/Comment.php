<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Models\Filterable;
use Illuminate\Database\Eloquent\Builder;


class Comment extends Model
{
    use Filterable {
        scopeFiltered as scopeFilteredTrait;
    }


    public $fillable = ['id','parent_id', 'user_id', 'content', 'is_published', 'created_at', 'updated_at', 'commentable_id', 'commentable_type'];

    protected $filterable_by_kw = ['id', 'user_id', 'content', 'is_published'];

    protected $casts = [
        'is_published' => 'boolean',
    ];
    /**
     * Get all of the owning commentable models.
     */
    public function commentable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopePublished($query)
    {
        return $query->where('is_published', 1);
    }


    public function scopeFiltered(Builder $query, $filters)
    {
        if (isset($filters['is_published'])){
            $query->where('is_published', $filters['is_published']);
        }
        if (isset($filters['type'])){
            $query->where('commentable_type', $filters['type']);
        }
        if (isset($filters['object_id'])){
            $query->where('commentable_id', $filters['object_id']);
        }

        $this->scopeFilteredTrait($query, $filters);
    }


    public function childrens()
    {
        return $this->hasMany(self::class, 'parent_id')->published()->with('childrens');
    }

    public function hasChildrens()
    {
        $result = $this->childrens()->count();
        if($result){
            return true;
        }
       return false;

    }

}