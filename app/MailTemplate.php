<?php

namespace App;

use App\Abstracts\Models\TranslatableModel;
use App\Traits\Models\Filterable;

class MailTemplate extends TranslatableModel
{
    use Filterable;

    protected $table = 'mail_template';

    public $fillable = ['id'];

    protected $translationForeignKey = 'mail_id';

    public $translatedAttributes = ['subject', 'content'];

    protected $filterable_by_kw = ['name'];


}
