<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class InfoTranslation extends Model
{
    protected $table = 'info_translation';

    public $timestamps = false;

    protected $fillable = ['title', 'description',  'keywords', 'content'];

    public function post() {
        return $this->belongsTo(Info::class, 'info_id', 'id');
    }
}
