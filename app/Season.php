<?php

namespace App;

use App\Abstracts\Models\TranslatableModel;
use App\Traits\Models\Filterable;
use Illuminate\Database\Eloquent\Builder;

class Season extends TranslatableModel
{
    use Filterable {
        scopeFiltered as scopeFilteredTrait;
    }

    protected $table = 'season';

    public $translatedAttributes = ['title', 'title_short'];

    protected $filterable_by_kw = ['title', 'title_short'];

    public $fillable = ['id'];

    public function courses()
    {
        return $this->hasMany(Course::class);
    }

    public function subscription_types()
    {
        return $this->belongsToMany(SubscriptionType::class, 'subscription_type_season', 'season_id', 'subscription_type_id');
    }

    public function getFirstCourseAttribute()
    {
        return $this->courses()->published()->first();
    }

    public function scopeFiltered(Builder $query, $filters)
    {
        if (isset($filters['subscription_type_id'])){
            $subscription_type_id = $filters['subscription_type_id'];
            $query->whereHas('subscription_types', function($q) use($subscription_type_id) {
                $q->where('subscription_type_id', $subscription_type_id);
            });
        }

        $this->scopeFilteredTrait($query, $filters);
    }

}
