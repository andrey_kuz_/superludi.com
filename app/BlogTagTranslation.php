<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogTagTranslation extends Model
{
    protected $table = 'blog_tag_translation';

    public $timestamps = false;

    protected $fillable = ['title', 'description', 'keywords'];

    public function tag() {

        return $this->belongsTo(BlogTag::class, 'tag_id', 'id');
    }
}
