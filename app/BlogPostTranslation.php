<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class BlogPostTranslation extends Model
{
    protected $table = 'blog_post_translation';

    public $timestamps = false;

    protected $fillable = ['title', 'author_name', 'description', 'content', 'keywords'];

    public function post() {

        return $this->belongsTo(BlogPost::class, 'post_id', 'id');
    }
}