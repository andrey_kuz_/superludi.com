<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Container\Container;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\View;
use Tymon\JWTAuth\JWTAuth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Cookie;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    protected $auth;

    function __construct(Container $container, JWTAuth $auth)
    {
        $this->auth = $auth;

        parent::__construct($container);
    }

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        $user = null;
        try{
            $token = Crypt::decryptString(Cookie::get('token'));
            if ($token) {
                $user = $this->auth->toUser($token);
            }
        } catch(\Exception $e) {
            $user = null;
        }

        View::composer('errors::*', function ($view) use (&$user) {

            $view->with(compact('user'));
        });

        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into a response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        $response = parent::unauthenticated($request, $exception);

        return $response->withCookie(Cookie::forget('token'));
    }
}
