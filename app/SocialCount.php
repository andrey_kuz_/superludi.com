<?php

namespace App;

use App\Http\Controllers\Web\BlogController;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Models\Filterable;
use Illuminate\Database\Eloquent\Builder;

class SocialCount extends Model
{
    protected $table = 'social_count';

    use Filterable {
        scopeFiltered as scopeFilteredTrait;
    }


    public $fillable = ['view_count', 'share_count', 'social_count_table_id', 'social_count_table_type', 'created_at', 'updated_at'];

    protected $filterable_by_kw = ['id'];

    /**
     * Get all of the owning commentable models.
     */
    public function socialcountable()
    {
        return $this->morphTo();
    }

    public function scopeFiltered(Builder $query, $filters)
    {
        if (isset($filters['is_published'])){
            $query->where('is_published', $filters['is_published']);
        }
        if (isset($filters['type'])){
            $query->where('commentable_type', $filters['type']);
        }

        $this->scopeFilteredTrait($query, $filters);
    }

    // TODO: totaly wrong =(

    public function RefreshCounts($id)
    {
        $social_count = SocialCount::find($id);
        $get_url = BlogController::getPublicUrl($social_count->socialcountable_id);
        //send for update social_count and update
    }

}
