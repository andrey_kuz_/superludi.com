<?php

namespace App;

use App\Abstracts\Models\TranslatableModel;
use App\Traits\Models\Filterable;
use Illuminate\Database\Eloquent\Builder;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class SubscriptionType extends TranslatableModel
{
    use Filterable {
        scopeFiltered as scopeFilteredTrait;
    }

    protected $table = 'subscription_type';

    public $fillable = ['name', 'period', 'price', 'sale_price', 'start_at', 'end_at', 'active_at', 'seasons_ids'];

    protected $appends = ['seasons_ids'];

    protected $translationForeignKey = 'subscription_type_id';

    public $translatedAttributes = ['title', 'order_desc'];

    protected $filterable_by_kw = ['name'];

    public function seasons()
    {
        return $this->belongsToMany(Season::class, 'subscription_type_season', 'subscription_type_id', 'season_id');
    }

    public function courses()
    {
        return $this->hasManyThrough(Course::class, SubscriptionTypeSeason::class,'subscription_type_id', 'season_id', 'id', 'season_id');
    }

    public function scopeActive(Builder $query)
    {
        $now = Carbon::now()->toDateTimeString();
        $query->where('start_at', '<', $now)->where('end_at', '>', $now);
    }

    public function scopeMinPrice(Builder $query)
    {
        $query->where('price', $query->min('price'));
    }

    public function getSeasonsIdsAttribute()
    {
        return $this->seasons->pluck('id')->toArray();
    }

    public function setSeasonsIdsAttribute($seasons_ids)
    {
        if ($this->exists) {
            $old_seasons_ids = $this->getSeasonsIdsAttribute();
            $this->seasons()->detach(array_diff($old_seasons_ids, $seasons_ids));
            $this->seasons()->attach(array_diff($seasons_ids, $old_seasons_ids));
        }
    }

    public function getActiveCoursesAttribute()
    {
        return $this->courses()->published()->whereHas('lessons')->get();
    }

    public function scopeFiltered(Builder $query, $filters)
    {
        if (isset($filters['season_id'])){
            $season_id = $filters['season_id'];
            $query->whereHas('seasons', function($q) use($season_id) {
                $q->where('season_id', $season_id);
            });
        }

        $this->scopeFilteredTrait($query, $filters);
    }

}
