<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogCategoryTranslation extends Model
{
    protected $table = 'blog_category_translation';

    public $timestamps = false;

    protected $fillable = ['title', 'description', 'keywords'];

    public function category() {

        return $this->belongsTo(BlogCategory::class, 'category_id', 'id');
    }
}
