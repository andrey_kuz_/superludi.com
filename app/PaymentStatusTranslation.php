<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentStatusTranslation extends Model
{
    protected $table = 'payment_status_translation';

    public $timestamps = false;

    protected $fillable = ['title'];

    public function payment_status() {

        return $this->belongsTo(PaymentStatus::class, 'payment_status_id', 'id');
    }
}
