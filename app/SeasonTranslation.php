<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeasonTranslation extends Model
{
    protected $table = 'season_translation';

    public $timestamps = false;

    protected $fillable = ['title', 'title_short'];

    public function season()
    {
        return $this->belongsTo(Season::class, 'season_id', 'id');
    }

}
