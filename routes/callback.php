<?php

/*
|--------------------------------------------------------------------------
| Callback Routes
|--------------------------------------------------------------------------
*/

Route::group([
    'namespace'     => 'Callback',
    'middleware'    => [ 'callback' ]
], function() {

    Route::post('/checkout_callback', 'SubscriptionController@checkoutCallback')->name('checkoutCallback');
    Route::post('/checkout_response', 'SubscriptionController@checkoutResponse')->name('checkoutResponse');

    Route::group([
        'prefix'  => 'presale',
    ], function() {
        Route::post('/checkout_callback', 'PresaleController@checkoutCallback')->name('presale.checkoutCallback');
        Route::post('/checkout_response', 'PresaleController@checkoutResponse')->name('presale.checkoutResponse');
    });

});