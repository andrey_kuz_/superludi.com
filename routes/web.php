<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'namespace'     => 'Web\Admin',
    'middleware'    => [ 'web' ]
], function() {

    Route::get('/admin', 'AppController@index');
});

Route::group([
    'namespace'     => 'Web\Auth',
    'middleware'    => [ 'web' ]
], function() {

    Route::get('/login/{provider}', 'AuthController@redirectToProvider')->name('login_by_provider');
    Route::get('/callback/{provider}', 'AuthController@handleProviderCallback');
});

Route::group([
    'prefix'        => LaravelLocalization::setLocale(),
    'namespace'     => 'Web\Front',
    'middleware'    => [ 'web', 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
], function() {

    Route::get( '/',  'HomeController@index')->name('home');

    Route::get( '/login',  'HomeController@index')->name('login');
    Route::get( '/register',  'HomeController@index')->name('register');
    Route::get( '/enter_email',  'HomeController@index')->name('enter_email');
    Route::get( '/enter_password',  'HomeController@index')->name('enter_password');
    Route::get( '/thank_payment',  'HomeController@index')->name('thank_payment');
    Route::get( '/error_payment',  'HomeController@index')->name('error_payment');
    Route::get( '/exist_subscription',  'HomeController@index')->name('exist_subscription');
    Route::get( '/forgot_password',  'HomeController@index')->name('forgot_password');

    Route::get('/blog',  'BlogController@index')->name('blog-main');
    Route::get('/blog/tag/{slug}',  'BlogController@tag')->name('blog-tag');
    Route::get('/blog/{category_slug}',  'BlogController@category')->name('blog-category');
    Route::get('/blog/{category_slug}/{post_slug}',  'BlogController@post')->name('blog-post');

    Route::get('/info/{slug}', 'InfoController@index')->name('info');

    Route::get( '/courses/{slug}',  'CourseController@course')->name('course');

    Route::group([
        'middleware'    => [ 'auth' ]
    ], function() {

        Route::get('/profile', 'ProfileController@index')->name('profile');
        Route::get('/checkout/{subscription_type}', 'SubscriptionController@checkout')->name('subscription.checkout');
        Route::get('/recurring/{subscription}', 'SubscriptionController@recurring')->name('subscription.recurring');

        Route::get( '/courses',  'CourseController@index')->name('courses');
    });

});

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web']], function () {

    Route::group([
        'middleware'    => [ 'auth' ]
    ], function() {

        \UniSharp\LaravelFilemanager\Lfm::routes();

    });
});

Route::group([
    'namespace'     => 'Web\Front',
    'prefix'        => LaravelLocalization::setLocale(),
], function() {

    Route::get('/meetup_2', 'PresaleController@index');
    Route::get('/meetup_2/thanks', 'PresaleController@index')->name('presale.thanks');
    Route::get('/meetup_2/error', 'PresaleController@index')->name('presale.error');

    Route::post('/meetup_2/checkout', 'PresaleController@createSubscription')->name('presale.checkout');
});


