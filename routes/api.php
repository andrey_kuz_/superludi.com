<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
    'prefix' => 'auth',
    'namespace' => 'Api\Auth',
    'middleware' => ['api']
], function() {

    Route::post('/login', 'AuthController@login')->name('api.login');
    Route::post('/register', 'AuthController@register')->name('api.register');
    Route::post('/login_by_fb', 'AuthController@loginByFB')->name('api.login_by_fb');
    Route::post('/register_by_email', 'AuthController@registerByEmail')->name('api.register_by_email');
    Route::post( '/forgot_password',  'AuthController@forgotPassword')->name('api.forgot_password');
    Route::post('/logout', 'AuthController@logout')->name('api.logout');
    Route::get('/refresh', 'AuthController@refresh');

    Route::group([
        'middleware' => 'jwt.auth'
    ], function() {

        Route::get('/user',  'AuthController@user');
    });
});

Route::group([
    'prefix' => 'front',
    'namespace' => 'Api\Front',
    'middleware' => ['api']
], function() {

    Route::post('/mailinglist/subscribe', 'MailinglistController@subscribe')->name('api.subscribe');
    Route::get( '/course_lesson/{course_lesson}',  'CourseLessonController@show');
    Route::post( '/blog/show_more',  'BlogController@showMore');
    Route::post('/comment/show_more',  'CommentController@showMore');
    Route::post('/comment/show_other',  'CommentController@showOther');

    Route::group([
        'middleware' => 'jwt.auth'
    ], function() {

        Route::get( '/course_lesson/{course_lesson}/state',  'CourseLessonController@showState');
        Route::post( '/course_lesson/{course_lesson}/state',  'CourseLessonController@storeState');

        Route::post('/profile/update_email', 'ProfileController@updateEmail')->name('api.profile.update_email');
        Route::post('/profile/update_password', 'ProfileController@updatePassword')->name('api.profile.update_password');
        Route::post('/profile/update_name', 'ProfileController@updateName');
        Route::post('/profile/update_city', 'ProfileController@updateCity');
        Route::post('/profile/update_avatar', 'ProfileController@updateAvatar');

        Route::post('/comment/{class}/{id}',  'CommentController@addComment');

    });
});

Route::group([
    'prefix' => 'admin',
    'namespace' => 'Api\Admin',
    'middleware' => ['api', 'jwt.auth']
], function() {

    Route::apiResource('/user', 'UserController');

    Route::apiResource('/role', 'RoleController');

    Route::apiResource('/course', 'CourseController');

    Route::apiResource('/course_lesson', 'CourseLessonController');

    Route::apiResource('/course_lesson_material', 'CourseLessonMaterialController');

    Route::apiResource('/blog_tag', 'BlogTagController');

    Route::apiResource('/blog_post', 'BlogPostController');

    Route::apiResource('/blog_category', 'BlogCategoryController');

    Route::apiResource('/mail_template', 'MailTemplateController');

    Route::apiResource('/payment_status', 'PaymentStatusController');

    Route::apiResource('/payment', 'PaymentController');

    Route::apiResource('/season', 'SeasonController');

    Route::apiResource('/speaker', 'SpeakerController');

    Route::apiResource('/info', 'InfoController');

    Route::apiResource('/comment', 'CommentController');

    Route::apiResource('/subscription_type', 'SubscriptionTypeController');
    Route::apiResource('/subscription', 'SubscriptionController');

});
