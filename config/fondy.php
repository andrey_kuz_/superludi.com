<?php
/**
 * Created by PhpStorm.
 * User: an
 * Date: 28.01.19
 * Time: 10:49
 */

return [
    'merchant_id' => env('FONDY_MERCHANT_ID', '1396424'),
    'secret_key' => env('FONDY_SECRET_KEY', 'test'),
    'order_prefix' => env('FONDY_ORDER_PREFIX', '')
];