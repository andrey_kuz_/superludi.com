<?php

use Monolog\Handler\StreamHandler;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Log Channel
    |--------------------------------------------------------------------------
    |
    | This option defines the default log channel that gets used when writing
    | messages to the logs. The name specified in this option should match
    | one of the channels defined in the "channels" configuration array.
    |
    */

    'default' => env('LOG_CHANNEL', 'stack'),

    /*
    |--------------------------------------------------------------------------
    | Log Channels
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log channels for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Drivers: "single", "daily", "slack", "syslog",
    |                    "errorlog", "monolog",
    |                    "custom", "stack"
    |
    */

    'channels' => [
        'stack' => [
            'driver' => 'stack',
            'channels' => ['daily'],
        ],

        'single' => [
            'driver' => 'single',
            'path' => storage_path('logs/laravel.log'),
            'level' => 'debug',
            'days' => 30,
        ],

        'daily' => [
            'driver' => 'daily',
            'path' => storage_path('logs/laravel.log'),
            'level' => 'debug',
            'days' => 30,
        ],

        'slack' => [
            'driver' => 'daily',
            'url' => env('LOG_SLACK_WEBHOOK_URL'),
            'username' => 'Laravel Log',
            'emoji' => ':boom:',
            'level' => 'critical',
            'days' => 30,
        ],

        'stderr' => [
            'driver' => 'daily',
            'handler' => StreamHandler::class,
            'with' => [
                'stream' => 'php://stderr',
            ],
            'days' => 30,
        ],

        'syslog' => [
            'driver' => 'daily',
            'level' => 'debug',
            'days' => 30,
        ],

        'errorlog' => [
            'driver' => 'daily',
            'level' => 'debug',
            'days' => 30,
        ],

        'commands' => [
            'driver' => 'daily',
            'path' => storage_path('logs/commands/exec.log'),
            'level' => 'debug',
            'days' => 365,
        ],
        'amocrm' => [
            'driver' => 'daily',
            'path' => storage_path('logs/api/amocrm.log'),
            'level' => 'debug',
            'days' => 30,
        ],
        'unisender' => [
            'driver' => 'daily',
            'path' => storage_path('logs/api/unisender.log'),
            'level' => 'debug',
            'days' => 30,
        ],
        'fondy' => [
            'driver' => 'daily',
            'path' => storage_path('logs/api/fondy.log'),
            'level' => 'debug',
            'days' => 30,
        ],
    ],

];
