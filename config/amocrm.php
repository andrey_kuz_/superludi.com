<?php

/*
 * This file is part of Laravel AmoCrm.
 *
 * (c) dotzero <mail@dotzero.ru>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Авторизация в системе amoCRM
    |--------------------------------------------------------------------------
    |
    | Эти параметры необходимы для авторизации в системе amoCRM.
    | - Поддомен компании. Приставка в домене перед .amocrm.ru;
    | - Логин пользователя. В качестве логина в системе используется e-mail;
    | - Ключ пользователя, который можно получить на странице редактирования
    |   профиля пользователя.
    |
    */
    'domain' => env('AMO_DOMAIN', ''),
    'login' => env('AMO_LOGIN', ''),
    'hash' => env('AMO_HASH', ''),
    'fields' => [
        'email' => env('AMO_FIELD_EMAIL', 187647),
        'phone' => env('AMO_FIELD_PHONE', 187645),
        'source_transaction' => env('AMO_FIELD_SOURCE', 579542),
        'type_transaction' => env('AMO_FIELD_TYPE', 579552),
        'product' => env('AMO_FIELD_PRODUCT', 579548),
        'stream' => env('AMO_FIELD_SEASON', 580018),
        'advertising_type' => env('AMO_FIELD_ADVERTISING', 579556),
        'question' => env('QUESTION', 583930)
    ],
    'statuses' => [
        'primary' => env('AMO_STATUS_PRIMARY', 18057247),
        'success' => env('AMO_STATUS_SUCCESS', 142)
    ],
    'analytics' => [
        'utm_source' => env('AMO_UTM_SOURCE', 517403),
        'utm_medium' => env('AMO_UTM_MEDIUM', 517409),
        'utm_campaign' => env('AMO_UTM_CAMPAIGN', 517413),
        'utm_term' => env('AMO_UTM_TERM', 517415),
        'utm_content' => env('AMO_UTM_CONTENT', 517431),
    ]

];