<?php

/*
 * This file is part of Laravel AmoCrm.
 *
 * (c) dotzero <mail@dotzero.ru>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Настройки Блога
    |--------------------------------------------------------------------------
    |
    | Эти параметры отвечают за колличество выведенной информации.
    |
    */

    'show_count_tag' => env("BLOG_SHOW_COUNT_TAG", 7),
    'show_count_category' => env("BLOG_SHOW_COUNT_CATEGORY", 7),
    'main_show_count_tag' => env("BLOG_MAIN_SHOW_COUNT_TAG", 10),
    'main_show_count_post' => env("BLOG_MAIN_SHOW_COUNT_POST", 7),


];