<?php
/**
 * Created by PhpStorm.
 * User: an
 * Date: 28.01.19
 * Time: 11:01
 */
return [
    'api_key' => env('UNISENDER_API_KEY', ''),
    'list_id' => env('UNISENDER_LIST_ID', ''),
    'params' => [
        'double_optin' => env('UNISENDER_DOUBLE_OPTIN', '3'),
    ],
];
