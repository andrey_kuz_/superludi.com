<?php
return [
    /*
     * Api Information
     */
    'apiKey' => env('GETRESPONSE_APIKEY', 'null'),
    'apiUrl' => env('GETRESPONSE_API_URL', 'https://api.getresponse.com/v3'),
    'timeout' => 8,
    'enterpriseDomain' => null,
    'appId' => null,

    'lists' => [
        'default' => [
            'id' => env('GETRESPONSE_LIST_ID', 'null'),
        ],
    ],
];
